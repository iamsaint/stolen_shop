<div class="mag-catalog-block view-middle" ng-controller="SearchCtrl">
    <div class="mag-catalog-item ng-scope" ng-repeat="i in items">
        <div class="image">
            <a href="javascript:void(0)" ng-mousedown="openCard(i)">
                <img ng-src="{{i.file}}" ng-if="i.file" ng-class="{vertical:i.category_data.imagetype==1}" class="ng-scope">
            </a>
        </div>
        <div class="name ng-binding" ng-bind="i.name"></div>
        <div class="clear"></div>
        <div class="cena-add-basket-wrap">
            <div class="cena fll">
                <span class="ng-binding" ng-bind="i.price + 'p.'"></span>
            </div>
            <a class="flr add-basket" href="javascript:void(0)" ng-mousedown="openCard(i)"><span class="dotted-underline">в корзину</span></a>
        </div>
    </div>
    <?$this->renderPartial('../site/frame')?>
</div>

<? \Yii::app()->clientScript->registerScriptFile('SearchCtrl.js', \CClientScript::POS_END); ?>
