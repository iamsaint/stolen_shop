<h1 style="line-height: 60px;">Даные заказчика</h1>
<div style="display: inline-block;">
<div style="float:left;">
<table>
    <tr>
        <td style="padding:7px 7px 7px 17px;text-align: left;">ФИО</td>
        <td style="padding:7px 7px 7px 17px;text-align: left;"><?=$model->morderdata[0]->fio?></td>
    </tr>
    <tr>
        <td style="padding:7px 7px 7px 17px;text-align: left;">Email</td>
        <td style="padding:7px 7px 7px 17px;text-align: left;"><?=$model->morderdata[0]->email?></td>
    </tr>
    <tr>
        <td style="padding:7px 7px 7px 17px;text-align: left;">Телефон</td>
        <td style="padding:7px 7px 7px 17px;text-align: left;"><?=$model->morderdata[0]->phone?></td>
    </tr>
    <tr>
        <td style="padding:7px 7px 7px 17px;text-align: left;">Почтовый индекс</td>
        <td style="padding:7px 7px 7px 17px;text-align: left;"><?=$model->morderdata[0]->zip?></td>
    </tr>
    <tr>
        <td style="padding:7px 7px 7px 17px;text-align: left;">Город</td>
        <td style="padding:7px 7px 7px 17px;text-align: left;"><?=$model->morderdata[0]->city?></td>
    </tr>
    <tr>
        <td style="padding:7px 7px 7px 17px;text-align: left;">Адрес</td>
        <td style="padding:7px 7px 7px 17px;text-align: left;"><?=$model->morderdata[0]->address?></td>
    </tr>
    <tr>
        <td style="padding:7px 7px 7px 17px;text-align: left;">Транспортная компания</td>
        <td style="padding:7px 7px 7px 17px;text-align: left;"><?=$model->morderdata[0]->bdelivery->name?></td>
    </tr>
</table>
</div>
    <div style=" width:470px;display: inline-block;margin-left:20px;padding:0 20px 24px 20px;"><p style="width:100%;font-weight: 600;line-height: 54px;">Комментарий</p>
        <p style="line-height:24px;"><?=$model->morderdata[0]->comment?></p>
    </div>

</div>


<h3 style="line-height: 60px;">Товары в заказе</h3>
<table style="border-collapse: collapse;border:1px solid #dadada; width:100%">
    <thead>
    <th style="padding:7px 7px 7px 17px;text-align: left;">№п/п</th>
    <th style="padding:7px 7px 7px 17px;text-align: left;"></th>
    <th style="padding:7px 7px 7px 17px;text-align: left;">Название</th>
    <th style="padding:7px 7px 7px 17px;text-align: left;">Параметры</th>
    <th style="padding:7px 7px 7px 17px;text-align: left;">Количество</th>
    <th style="padding:7px 7px 7px 17px;text-align: left;">Цена</th>
    <th style="padding:7px 7px 7px 17px;text-align: left;">Стоимость</th>
    </thead>
    <tbody>
    <?foreach($model->morderitems as $k=>$item):?>
        <tr style="border:1px solid #dadada;">
            <td style="padding:7px 7px 7px 17px;text-align: left;"><?=$k+1?></td>
            <td style="padding:7px 7px 7px 17px;text-align: left;">
                <?if(is_null($item->scu)):?>
                    <img src="<?='http://'.$_SERVER["HTTP_HOST"].Yii::app()->easyImage->thumbSrcOf('.'. $item->icomp->bitem->images[0]->file, array('resize' => array('width' => 64, 'height' => 64)))?>">
                <?else:?>
                    <?
                    $scuImageFile = isset($item->bscu->images[0]->file)? $item->bscu->images[0]->file : $item->bscu->item->images[0]->file;
                    ?>
                    <img src="<?='http://'.$_SERVER["HTTP_HOST"].Yii::app()->easyImage->thumbSrcOf('.'. $scuImageFile, array('resize' => array('width' => 64, 'height' => 64)))?>">
                <?endif?>
            </td>
            <td style="padding:7px 7px 7px 17px;text-align: left;"><?=$item->icomp->bitem->name?:$item->bscu->item->name?></td>
            <td style="padding:7px 7px 7px 17px;text-align: left;">
                <?if($articul = $item->icomp->articul?:$item->bscu->articul):?>
                    <strong>Артикул</strong> <?=$articul?><br>
                <?endif?>
                <?if($code = $item->icomp->code?:$item->bscu->code):?>
                    <strong>Код товара</strong> <?=$code?><br>
                <?endif?>
                <?if(is_null($item->scu)):?>
                    <strong>Размер</strong> <?=$item->icomp->bcomplectation->bsize->name?><br>
                    <strong>Материал</strong> <?=$item->icomp->bcomplectation->bmaterial->name?><br>
                    <strong>Производитель</strong> <?=$item->icomp->bcomplectation->bmanufacturer->name?><br>
                    <strong>Категория</strong> <?=$item->icomp->bitem->bcategory->name?><br>
                <?else:?>
                    <?foreach($item->bscu->item->popt as $s):?>
                        <?if($s->bcatalog->basket==0) continue;?>
                        <strong><?=$s->bcatalog->name?></strong> <?=$s->bcatalogitem->name?><br>
                    <?endforeach?>
                    <?foreach($item->bscu->scuitems as $s):?>
                        <?if($s->bcatalog->basket==0) continue;?>
                        <strong><?=$s->bcatalog->name?></strong> <?=$s->bcatalogitem->name?><br>
                    <?endforeach?>
                    <strong>Категория</strong> <?=$item->bscu->item->bcategory->name?><br>
                <?endif?>
            </td>
            <td style="padding:7px 7px 7px 17px;text-align: left;"><?=$item->count?></td>
            <? $price = (is_null($item->scu)?$item->icomp->bcomplectation->price:(intval($item->bscu->price)>0?$item->bscu->price:$item->bscu->item->price)) ?>
            <td style="padding:7px 7px 7px 17px;text-align: left;">
                <?=$price?>
            </td>
            <td style="padding:7px 7px 7px 17px;text-align: left;"><?=$price*$item->count ?> руб.</td>
        </tr>
        <? $total += $price*$item->count?>
    <?endforeach?>
    </tbody>
</table>
<p style="float:right;text-align: right;font-weight: 700;font-size:17px;line-height:50px;">Итого: <?=$total?> руб.</p>
