<h1>Даные заказчика</h1>
<div class="order-top">
<div class="fll order-info">
<table class="items">
    <tr>
        <td>Номер заказа</td>
        <td><?=$model->order_num?></td>
    </tr>
    <tr>
        <td>ФИО</td>
        <td><?=$model->morderdata[0]->fio?></td>
    </tr>
    <tr>
        <td>Email</td>
        <td><?=$model->morderdata[0]->email?></td>
    </tr>
    <tr>
        <td>Телефон</td>
        <td><?=$model->morderdata[0]->phone?></td>
    </tr>
    <tr>
        <td>Почтовый индекс</td>
        <td><?=$model->morderdata[0]->zip?></td>
    </tr>
    <tr>
        <td>Город</td>
        <td><?=$model->morderdata[0]->city?></td>
    </tr>
    <tr>
        <td>Адрес</td>
        <td><?=$model->morderdata[0]->address?></td>
    </tr>
    <tr>
        <td>Транспортная компания</td>
        <td><?=$model->morderdata[0]->bdelivery->name?></td>
    </tr>
</table>
</div>
    <div class="mail-comment"><p class="mail-comment-header">Комментарий</p>
        <p class="mail-comment-body"><?=$model->morderdata[0]->comment?></p>
    </div>

</div>


<h3>Товары в заказе</h3>
<table class="items">
    <thead>
    <th>№п/п</th>
    <th></th>
    <th>Название</th>
    <th>Параметры</th>
    <th>Количество</th>
    <th>Цена</th>
    <th>Стоимость</th>
    </thead>
    <tbody>
    <?foreach($model->morderitems as $k=>$item):?>
        <tr>
            <td><?=$k+1?></td>
            <td>
                <?if(is_null($item->scu)):?>
                    <img src="<?='http://'.$_SERVER["HTTP_HOST"].Yii::app()->easyImage->thumbSrcOf('.'. $item->icomp->bitem->images[0]->file, array('resize' => array('width' => 64, 'height' => 64)))?>">
                <?else:?>
                    <?
                    $scuImageFile = isset($item->bscu->images[0]->file)? $item->bscu->images[0]->file : $item->bscu->item->images[0]->file;
                    ?>
                    <img src="<?='http://'.$_SERVER["HTTP_HOST"].Yii::app()->easyImage->thumbSrcOf('.'. $scuImageFile, array('resize' => array('width' => 64, 'height' => 64)))?>">
                <?endif?>
            </td>
            <td><?=$item->icomp->bitem->name?:$item->bscu->item->name?></td>
            <td>
                <?if($articul = $item->icomp->articul?:$item->bscu->articul):?>
                    <strong>Артикул</strong> <?=$articul?><br>
                <?endif?>
                <?if($code = $item->icomp->code?:$item->bscu->code):?>
                    <strong>Код товара</strong> <?=$code?><br>
                <?endif?>
                <?if(is_null($item->scu)):?>
                    <strong>Размер</strong> <?=$item->icomp->bcomplectation->bsize->name?><br>
                    <strong>Материал</strong> <?=$item->icomp->bcomplectation->bmaterial->name?><br>
                    <strong>Производитель</strong> <?=$item->icomp->bcomplectation->bmanufacturer->name?><br>
                    <strong>Категория</strong> <?=$item->icomp->bitem->bcategory->name?><br>
                <?else:?>
                    <?foreach($item->bscu->item->popt as $s):?>
                        <?if($s->bcatalog->basket==0) continue;?>
                        <strong><?=$s->bcatalog->name?></strong> <?=$s->bcatalogitem->name?><br>
                    <?endforeach?>
                    <?foreach($item->bscu->scuitems as $s):?>
                        <?if($s->bcatalog->basket==0) continue;?>
                        <strong><?=$s->bcatalog->name?></strong> <?=$s->bcatalogitem->name?><br>
                    <?endforeach?>
                    <strong>Категория</strong> <?=$item->bscu->item->bcategory->name?><br>
                <?endif?>
            </td>
            <td><?=$item->count?></td>
            <? $price = (is_null($item->scu)?$item->icomp->bcomplectation->price:(intval($item->bscu->price)>0?$item->bscu->price:$item->bscu->item->price)) ?>
            <td>
                <?=$price?>
            </td>
            <td><?=$price*$item->count ?> руб.</td>
        </tr>
        <? $total += $price*$item->count?>
    <?endforeach?>
    </tbody>
</table>
<p class="mail-sum">Итого: <?=$total?> руб.</p>
