<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?= LANGUAGE_ID ?>" lang="<?= LANGUAGE_ID ?>" ng-app="myApp">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">
    <link rel="shortcut icon" type="image/x-icon" href="<?= SITE_DIR ?>/favicon.ico"/>
    <style>.ng-hide {
            display: none
        }</style>
    <title></title> <? Yii::app()->clientScript->registerScriptFile('angular.js', CClientScript::POS_END)->registerScriptFile('ngStorage.js', CClientScript::POS_END)->registerScriptFile('app.js', CClientScript::POS_END) ?>
</head>
<body class="ng-cloack ng-hide framedBody" ng-show="loaded" ng-init="loaded=true" ng-click="dropListClose($event)">

<div class="wrapper">
    <div style="display: none">
    <?php $this->widget('app.widgets.Basket')?>
    </div>
    <?= $content ?>
</div>
</body>
<script> var PHPCONFIG = <?= CJavaScript::encode($this->jsParams) ?></script>
<link rel="stylesheet" type="text/css" href="/styles/styles.css"/>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800,300&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
</html>