<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?= LANGUAGE_ID ?>" lang="<?= LANGUAGE_ID ?>" ng-app="myApp">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <link rel="shortcut icon" type="image/x-icon" href="/styles/img/favicon.ico"/>
    <style>.ng-hide {
            display: none
        }</style>
    <title><?=$this->title ?></title>
    <? Yii::app()->clientScript->registerScriptFile('angular.js', CClientScript::POS_END)->registerScriptFile('ngStorage.js', CClientScript::POS_END)->registerScriptFile('app.js', CClientScript::POS_END)
        ->registerScriptFile('ScrollCtrl.js',CClientScript::POS_END) ?>
</head>
<body class="ng-cloack ng-hide" ng-show="loaded" ng-init="loaded=true">
<div id="scroll-to-top" ng-click="smoothScrollTop()()" ng-controller="ScrollCtrl"></div>
<div class="wrapper">
    <header class="header">
        <div class="header-divider"><p class="hot-line-title flr">Бесплатный звонок по всей России</p></div>
        <a class="fll logo" href="/" target="_self">
            <img src="/styles/img/logo_header.png"/>
        </a>
        <div class="search-block fll">
            <form action="/search/" method="get" accept-charset="utf-8">
                <input type="text" class="top-search-input fll"  name="q" value="<?=Yii::app()->request->getParam('q','')?>" placeholder="Фраза для поиска">
                <button type="" class="search fll"></button>
            </form>
        </div>
        <div class="center-block fll">
            <?php $this->widget('app.widgets.Basket')?>
        </div>
        <div class="right-block flr">
            <span class="phone flr">8 800 100-50-34 </span>
            <div class="clear"></div>
            <a href="<?= Yii::app()->params['baseHost']?>/contacts/" title="" class="head-route head-btn flr">КАК ДОБРАТЬСЯ</a>
        </div>
    </header>
    <section class="main-menu-wrapper">
        <ul class="main-menu">
            <li class="fll  main-menu-item"><a href="<?= Yii::app()->params['baseHost']?>/about/" class=" main-menu-item-a"><span class="w100">О комплексе</span></a></li>
            <li class="fll  main-menu-item"><a href="<?= Yii::app()->params['baseHost']?>/partners/" class=" main-menu-item-a"><span class="w100">Арендаторам</span></a></li>
            <li class="fll  main-menu-item"><a href="<?= Yii::app()->params['baseHost']?>/news/" class=" main-menu-item-a"><span class="w100">Новости</span></a></li>
            <li class="fll  main-menu-item"><a href="<?= Yii::app()->params['baseHost']?>/services/" class=" main-menu-item-a active"><span class="w100">Услуги и сервисы</span></a></li>
            <li class="fll  main-menu-item"><a href="<?= Yii::app()->params['baseHost']?>/contacts/" class=" main-menu-item-a"><span class="w100">Контакты</span></a></li>
            <li class="flr  main-menu-item"><a href="<?= Yii::app()->params['baseHost']?>/personal/" class=" main-menu-item-key-a main-menu-item-a"><span class="w100">Личный кабинет</span></a></li>
        </ul>
    </section>
    <main class="content">

        <div class="wrap">
            <? $this->widget('zii.widgets.CBreadcrumbs', array('separator' => '<span class="arrow"></span>', 'links' => $this->breadcrumbs,)); ?>
            <?= $content ?>
        </div>
    </main>
</div>


<footer class="footer">

    <div class="footer-content">
        <div class="footer-adress-block">
            <div class="footer-adress-logo">

                <div class="footer-logo-text">
                    ТЕКСТИЛЬ ПРОФИ
                </div>
                <div class="footer-logo-subtext">ИВАНОВО</div>
            </div>
            <div class="footer-adress">
                ЗАО «ТекстильПрофи - Иваново» <br><br>
                8 800 100-50-34 <br>
                153005, г. Иваново, ул. Сосновая, д.1
            </div>
            <div class="yandex-map"><a href="<?= Yii::app()->params['baseHost']?>/contacts/" title="">Посмотреть на Яндекс.Картах</a></div>
        </div>
        <div class="footer-section-list">
            <ul class="section-list">
                <div class="coloumn">
                    <h4>ТЕКСТИЛЬ ПРОФИ</h4>
                    <li class=""><a href="<?= Yii::app()->params['baseHost']?>/about/" class="">О комплексе</a></li>
                    <li class=""><a href="<?= Yii::app()->params['baseHost']?>/partners/" class="">Партнеры</a></li>
                    <li class=""><a href="<?= Yii::app()->params['baseHost']?>/news/" class="">Новости</a></li>
                    <li class=""><a href="<?= Yii::app()->params['baseHost']?>/services/" class="">Услуги и сервисы</a></li>
                    <li class=""><a href="<?= Yii::app()->params['baseHost']?>/contacts/" class="">Контакты</a></li>
                    <li class=""><a href="<?= Yii::app()->params['baseHost']?>/personan/" class="">Личный кабинет</a></li>
                </div>
                <div class="coloumn">
                    <h4>ОПТОВЫМ КЛИЕНТАМ</h4>
                    <li class=""><a href="<?= Yii::app()->params['baseHost']?>/optovym-pokupatelyam/tkani/" class="">Ткани</a></li>
                    <li class=""><a href="<?= Yii::app()->params['baseHost']?>/optovym-pokupatelyam/odeyala-i-podushki/" class="">Одеяла и подушки</a></li>
                    <li class=""><a href="<?= Yii::app()->params['baseHost']?>/optovym-pokupatelyam/kpb/" class="">КПБ</a></li>
                    <li class=""><a href="<?= Yii::app()->params['baseHost']?>/optovym-pokupatelyam/pokryvala-pledy/" class="">Покрывала, пледы</a></li>
                    <li class=""><a href="<?= Yii::app()->params['baseHost']?>/optovym-pokupatelyam/spetsodezhda-i-obuv/" class="">Спецодежда и обувь</a></li>
                    <li class=""><a href="<?= Yii::app()->params['baseHost']?>/optovym-pokupatelyam/detskaya-odezhda/" class="">Детская одежда</a></li>
                    <li class=""><a href="<?= Yii::app()->params['baseHost']?>/optovym-pokupatelyam/makhrovye-izdeliya/" class="">Махровые изделия</a></li>
                    <li class=""><a href="<?= Yii::app()->params['baseHost']?>/optovym-pokupatelyam/meditsinskaya-odezhda/" class="">Медицинская одежда</a></li>
                    <li class=""><a href="<?= Yii::app()->params['baseHost']?>/optovym-pokupatelyam/uniforma/" class="">Униформа</a></li>
                    <li class=""><a href="<?= Yii::app()->params['baseHost']?>/optovym-pokupatelyam/odezhda-dlya-otdykha-i-turizma/" class="">Одежда для отдыха и туризма</a></li>
                    <li class=""><a href="<?= Yii::app()->params['baseHost']?>/optovym-pokupatelyam/trikotazhnye-izdeliya/" class="">Трикотажные изделия</a></li>
                    <li class=""><a href="<?= Yii::app()->params['baseHost']?>/optovym-pokupatelyam/odezhda-i-obuv/" class="">Одежда и обувь</a></li>
                    <li class=""><a href="<?= Yii::app()->params['baseHost']?>/optovym-pokupatelyam/chulochno-nosochnye-izdeliya/" class="">Чулочно-носочные изделия</a></li>
                    <li class=""><a href="<?= Yii::app()->params['baseHost']?>/optovym-pokupatelyam/lnyanye-izdeliya/" class="">Льняные изделия</a></li>
                    <li class=""><a href="<?= Yii::app()->params['baseHost']?>/optovym-pokupatelyam/gobelenovye-izdeliya/" class="">Гобеленовые изделия</a></li>
                </div>
                <div class="coloumn">
                    <h4>РОЗНИЧНЫМ КЛИЕНТАМ</h4>
                    <li class=""><a href="<?= Yii::app()->params['baseHost']?>/roznichnym-pokupaletyam/tekstil-dlya-kukhni/" class="">Текстиль для кухни</a></li>
                    <li class=""><a href="<?= Yii::app()->params['baseHost']?>/roznichnym-pokupaletyam/tekstil-dlya-spalni/" class="">Текстиль для спальни</a></li>
                    <li class=""><a href="<?= Yii::app()->params['baseHost']?>/roznichnym-pokupaletyam/tekstil-dlya-vannoy/" class="">Текстиль для ванной</a></li>
                    <li class=""><a href="<?= Yii::app()->params['baseHost']?>/roznichnym-pokupaletyam/tekstil-dlya-malyshey/" class="">Текстиль для малышей</a></li>
                    <li class=""><a href="<?= Yii::app()->params['baseHost']?>/roznichnym-pokupaletyam/detskaya-odezhda/" class="">Детская одежда</a></li>
                    <li class=""><a href="<?= Yii::app()->params['baseHost']?>/roznichnym-pokupaletyam/trikotazhnye-izdeliya/" class="">Трикотажные изделия</a></li>
                    <li class=""><a href="<?= Yii::app()->params['baseHost']?>/roznichnym-pokupaletyam/tyul-portera-shtory/" class="">Тюль, портьера, шторы</a></li>
                    <li class=""><a href="<?= Yii::app()->params['baseHost']?>/roznichnym-pokupaletyam/odezhda-dlya-otdykha-i-turizma/" class="">Одежда для отдыха и туризма</a></li>
                    <li class=""><a href="<?= Yii::app()->params['baseHost']?>/roznichnym-pokupaletyam/spetsodezhda-i-spetsobuv/" class="">Спецодежда и спецобувь</a></li>
                    <li class=""><a href="<?= Yii::app()->params['baseHost']?>/roznichnym-pokupaletyam/chulochno-nosochnye/" class="">Чулочно-носочные</a></li>
                    <li class=""><a href="<?= Yii::app()->params['baseHost']?>/roznichnym-pokupaletyam/lnyanye-izdeliya/" class="">Льняные изделия</a></li>
                    <li class=""><a href="<?= Yii::app()->params['baseHost']?>/roznichnym-pokupaletyam/gobelenovye-izdeliya/" class="">Гобеленовые изделия</a></li>
                </div>
                <div class="clear"></div>
            </ul>		</div>
        <!--noindex--><a rel="nofollow" href="http://involta.pro/" title="" class="involta-link" target="_blank"></a><!--/noindex-->
        <a href="https://play.google.com/store/apps/details?id=com.texprofy" title="" class="go-g-play"></a>
        <div class="clear"></div>
        <div class="overflow-note-wrapper" onclick="closeNotify()">
            <p id="overflow-notification" onclick="event.stopPropagation()"></p>
        </div>
    </div>
</footer>
<script> var PHPCONFIG = <?= CJavaScript::encode($this->jsParams) ?></script>
</body>
<script>document.getElementById("scroll-to-top").setAttribute('style','width:'+(window.innerWidth-1226)/2+'px');</script>
<link rel="stylesheet" type="text/css" href="/styles/styles.css"/>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800,300&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
</html>