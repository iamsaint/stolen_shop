<div class="overflow-note-wrapper" onclick="closeNotify()">
    <p id="overflow-notification" onclick="event.stopPropagation()"></p>
</div>
    <div class="product-container ng-cloack" ng-controller="CartCtrl" >
        <div class="product-image">
            <div class="product-big-image"><img ng-src="{{CONF.images[img].file}}" alt=""></div>
            <ul class="preview-list" ng-show="CONF.images.length > 1">
                <li ng-repeat="i in CONF.images" ng-click="setImg($index)"><a href="" title=""><img ng-src="{{i.thumb}}"
                                                                                                    alt=""></a></li>
            </ul>
        </div>
        <div class="product-information"><h1>{{CONF.name}}</h1>
            <ul class="product-info-list">
                <li ng-show="sku.price>0">
                    <div class="text"><span>Цена</span></div>
                    <div class="text-info">{{sku.price}} руб.</div>
                </li>
                <li ng-show="sku.articul.length > 0">
                    <div class="text"><span>Артикул</span></div>
                    <div class="text-info">{{sku.articul}}</div>
                </li>
                <li ng-show="sku.code.length > 0">
                    <div class="text"><span>Код</span></div>
                    <div class="text-info">{{sku.code}}</div>
                </li>
                <li ng-show="CONF.manufacturer.length > 0">
                    <div class="text"><span>Производитель</span></div>
                    <div class="text-info">{{CONF.manufacturer}}</div>
                </li>
                <li ng-show="CONF.material.length > 0">
                    <div class="text"><span>Материал</span></div>
                    <div class="text-info">{{CONF.material}}</div>
                </li>
                <li ng-show="CONF.collection.length > 0">
                    <div class="text"><span>Коллекция</span></div>
                    <div class="text-info">{{CONF.collection}}</div>
                </li>
                <li ng-show="CONF.sizes.length > 0">
                    <div class="text"><span>Размер</span></div>
                    <div class="text-info">
                        <div class="select-block drop-container" ng-class="{open:openSizeSelect}"><span ng-mousedown="selectClick('openSizeSelect')">{{CONF.sizes[size].name}}</span>
                            <ul class="drop-list mini">
                                <li ng-repeat="s in CONF.sizes"><a ng-click="setItem('size', $index)">{{s.name}}</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li ng-show="sku.descr.length > 0">
                    <div class="text"><span>Комплектация</span></div>
                    <div class="text-info" ng-bind-html="sku.descr | unsafe"></div>
                </li>
                <li ng-show="CONF.package.length > 0">
                    <div class="text"><span>Упаковка</span></div>
                    <div class="text-info">{{CONF.package}}</div>
                </li>
            </ul>
            <div class="total-coin">
                <div class="text">Количество:</div>
                <div class="value-block"><input type="text" name="" value="1" placeholder="" priceformat
                                                ng-model="count"><a href="" title="" class="up" ng-click="incC()"></a><a
                        href="" title="" class="down" ng-click="decC()"></a></div>
                <div class="text">х</div>
                <div class="total">{{sku.price}} руб.</div>
                <div class="text">=</div>
                <div class="total">{{sku.price * count}} руб.</div>
            </div>
            <ul class="add-card-block">
                <li><a href="" title="" class="link big" ng-click="addToBasket()">добавить в корзину</a></li>
                <?if(!$framed):?>
                <li><a href="/catalog/<?=explode('/',$_SERVER["REQUEST_URI"])[2]?>" title="" class="link big green right">продолжить покупки</a></li>
                <?endif?>
            </ul>
        </div>
        <div class="clear"></div>
    </div>
<? \Yii::app()->clientScript->registerScriptFile('CartCtrl.js', \CClientScript::POS_END); ?>