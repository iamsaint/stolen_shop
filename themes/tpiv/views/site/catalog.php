<div ng-controller="CatalogCtrl" when-scrolled="loadMore()">
    <?php $this->widget('app.widgets.CategoryFilter', ['current' => $id, 'manufacturer' => $manufacturer, 'material' => $material, 'size' => $size,]); ?>
    <div class="mag-catalog-block view-middle" >
        <div class="mag-catalog-item" ng-repeat="i in items">
            <div class="image">
                <a href="javascript:void(0)" ng-mousedown="openCard($index)">
                    <img ng-src="{{i.file}}" ng-if="i.file" ng-class="{vertical:CURRENT_CATEGORY.imagetype==1}">
                </a>
            </div>
            <div class="name">{{ i.name }}</div>
            <div class="clear"></div>
            <div class="cena-add-basket-wrap">
                 <div class="cena fll">
                    <span>{{ i.price }} р.</span>
                 </div>
                  <a class="flr add-basket" href="javascript:void(0)" ng-mousedown="openCard($index)"><span class="dotted-underline">в корзину</span></a>
            </div>
            </div>
        </div>
    <?$this->renderPartial('frame')?>

    </div>
<? \Yii::app()->clientScript->registerScriptFile('CatalogCtrl.js', \CClientScript::POS_END); ?>


