<ul class="section-menu">
    <li class="panel-wrap">
        <a target="_self" href="<?= Yii::app()->params['baseHost']?>/optovym-pokupatelyam/" title="" class="panel ">Оптовым покупателям</a>
    </li>
    <li class="panel-wrap">
        <a target="_self" href="<?= Yii::app()->params['baseHost']?>/roznichnym-pokupaletyam/" title="" class="panel ">розничным покупателям</a>
    </li>
    <li class="panel-wrap">
        <a target="_self" href="/" title="" class="panel no-right active">Интернет-магазин</a>
    </li>
</ul>
<div class="labels-block">
    <div class="label"><div class="label-container">
            <div class="image"><img src="/styles/img/discount.png" alt=""></div>
            <div class="link"><a href="/site/discount/" title="">система скидок</a></div>
        </div>
    </div>
    <div class="label"><div class="label-container">
            <div class="image"><img src="/styles/img/rules.png" alt=""></div>
            <div class="link"><a href="/site/rules/" title="">правила работы</a></div>
        </div>
    </div>
    <div class="label"><div class="label-container">
            <div class="image"><img src="/styles/img/phone.png" alt=""></div>
            <div class="link"><a href="/site/contacts/" title="">наши контакты</a></div>
        </div>
    </div>
    <div class="label"><div class="label-container">
            <div class="image"><img src="/styles/img/gruz-opt.png" alt=""></div>
            <div class="link"><a href="/site/shipping/" title="">способы доставки</a></div>
        </div>
    </div>
</div>
<?php if($this->beginCache($id, array('duration'=>3600, 'dependency'=>$dependency))) { ?><div class="mag-category-block"><?foreach($model as $m):?><div class="mag-category-item"><div class="m-c-container"><div class="image"><img src="<?=$m->bimage->file?>" alt=""></div><div class="link"><a href="/catalog/<?=$m->code?>/" title=""><?=$m->name?></a></div><div class="text"><?=$m->descr?></div></div></div><?endforeach?></div><?php $this->endCache(); } ?>