<div class="overflow-wrapper" ng-class="{active: itemCardOpen}" ng-click="closeCard()">
    <p id="overflow-notification"></p>
    <div class="overflow-item" ng-class="{active: itemCardOpen}">
        <div class="frame-close-btn"
             ng-click="closeCard()"></div>
        <script language="javascript" type="text/javascript">
            function resizeIframe(obj) {
                var height = obj.contentWindow.document.body.scrollHeight;
                if(height > window.innerHeight){
                    height = window.innerHeight - 60;
                }
                obj.style.height = height + 'px';
                document.getElementsByClassName('overflow-item')[0].setAttribute('style','height:'+height+'px')

            }
        </script>

        <iframe ng-if="itemCardOpen" ng-src="{{'/catalog/'+CURRENT_CATEGORY.code+'/'+itemCardCode+'?framed=true'}}" width="100%" onload='javascript:resizeIframe(this);' style="border: 0px;float:left"></iframe>
    </div>
</div>