<h1>реквизиты и контакты</h1>
<div class="contacts-wrapper">
<div class="w50 fll">
    <div class="contacts-box">
        <div class="discount-head tleft">Контактная информация</div>
        <p class="contacts-place">Г.Иваново, ул.Сосновая д.1 «Текстиль Профи Иваново»</p>
        <p class="contacts-phone">(4932) 920-176; 8-800-100-50-34</p>
        <p class="contacts-mail"><a href="mailto:tp_zakaz@mail.ru">tp_zakaz@mail.ru</a></p>
    </div>
    <div class="contacts-box mt36">
        <div class="discount-head tleft">Реквизиты для оплаты<span class="contacts-add">(по предварительно выставленному счету)</span></div>
        <ul class="s-list">
            <li>ИП Глезина Татьяна Ивановна Свидетельство серия 37 №001674430 от 28.05.2014</li>
            <li>ИНН 373000152717 ОГРНИП 314370214800028 выдано ИМНС РФ по г.Иваново</li>
        </ul>
    </div>
    <div class="contacts-box">
        <div class="contacts-white-head tleft mt36">Контактная информация</span></div>
        <ul class="s-list">
            <li>Если плательщиком по счету является третье лицо, то в графе "Назначение платежа" указывать оплата по счету №, дата, за .....(ФИО получателя по счету).</li>
            <li>При оплате с карты через Сбербанк ОнЛайн необходимо выбирать "оплата организации" и получателем указывать ИП Глезина Татьяна Ивановна</li>
        </ul>
    </div>
</div>
<div class="w50 fll">
    <div class="contacts-box">
        <div class="contacts-head contacts-head-sber"></div>
        <ul class="s-list">
            <li>р/с 40802810417000002700 Отделение №8639 Сбербанка России г. Иваново</li>
            <li>Бик   042406608 Кор.счет банка  - 30101810000000000608</li>
        </ul>
    </div>
    <div class="contacts-box">
        <div class="contacts-head contacts-head-prom"></div>
        <ul class="s-list">
            <li>Р/с 40802810102000004674 Ярославский филиал ОАО «Промсвязьбанк» г. Ярославль</li>
            <li>БИК   047888760  К/с  30101810300000000760  в ГРКЦ ГУ Банка России по Ярославской области</li>
        </ul>
    </div>

</div>
</div>