<h1>система скидок</h1>
<div class="discount-wrapper">
<div class="w25 fll side-left-discount">
    <div class="discount-box">
    <div class="discount-head">На сборный заказ</div>
        <div class="discount-list-blue">
            <ul class="assortment_org_list">
                <li>от 50 000руб.    — 4%</li>
                <li>от 100 000руб.  — 6%</li>
                <li>от 150 000руб. —  8%</li>
            </ul>
        </div>
    </div>
</div>
<div class="w75 fll">
    <div class="discount-box">
    <div class="discount-head">Скидки от 30 000руб<span class="contacts-add">(1 производитель)</span></div>
        <div class="discount-list-green">
            <ul class="assortment_org_list">
                <li>5% - Diamond, Мона Лиза, Валенсия, Красная Талка, NSD, Софттекс, Текс Дизайн, Натали, Рехина, Батист, Зайка, Ивашка, Шуйские ситцы, Доброе утро, Баракат, Медицина, Стиль Форм, Софттекс, ВиоТекс, Маркиза, Дарья;</li>
                <li>7% - Арт постель, Чарующая ночь;</li>
                <li>10% - Василиса, Голд Текс (от 5000руб.), Марианна, Эдельвейс;</li>
                <li>15% - Носки (от 10000руб.).</li>
            </ul>
        </div>
    </div>
</div>
</div>