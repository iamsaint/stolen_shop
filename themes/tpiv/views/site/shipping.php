<h1> доставка</h1>
<div class="shipping-bg">
<div class="shipping-row">
    <img class="fll" src="/styles/img/shipping-1.jpg">
    <div class="shipping-row-info flr">
        <div class="shipping-info-head">ЖелДорЭкспедиция</div>
        <div class="shipping-info-info">Клиентам интернет-магазина предоставляется скидка 10% на доставку по всем направлениям, за исключением минимальной стоимости доставки. </div>
    </div>
</div>
<div class="shipping-row">
    <img class="fll" src="/styles/img/shipping-2.jpg">
    <div class="shipping-row-info flr">
        <div class="shipping-info-head">Автотрейдинг</div>
        <div class="shipping-info-info">Клиентам интернет-магазина предоставляется скидка 10% на доставку по всем направлениям, за исключением минимальной стоимости доставки. При отправке на Москву обязательно сразу указывать ф-л получения, т.к. перемещение между филиалами по г.Москва платное.</div>
    </div>
</div>
<div class="shipping-row">
    <img class="fll" src="/styles/img/shipping-3.jpg">
    <div class="shipping-row-info flr">
        <div class="shipping-info-head">Почта России</div>
        <div class="shipping-info-info"> (отправляется при 100% предоплате заказа и примерной стоимости доставки.) Наложенным платежом товар не отправляем.</div>
    </div>
</div>
<div class="discount-wrapper">
<img class="shipping-img fll"  src="/styles/img/shipping-4.jpg">
<img class="shipping-img fll" src="/styles/img/shipping-5.jpg">
<img class="shipping-img fll" src="/styles/img/shipping-6.jpg">
<img class="shipping-img no-right fll" src="/styles/img/shipping-7.jpg">
</div>
<div class="shipping-row mt24">
    <p class="tcenter pt24"><strong>Если нужной транспортной компании нет в списке</strong></p>
    <p class="tcenter">Вы можете самостоятельно заказать забор груза с нашего склада, преварительно согласовав дату с нашим менеджером</p>
</div>
</div>