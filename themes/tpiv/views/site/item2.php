<div class="overflow-note-wrapper" onclick="closeNotify()">
    <p id="overflow-notification" onclick="event.stopPropagation()"></p>
</div>
    <div class="product-container ng-cloack" ng-controller="CartCtrl">
        <div class="product-image">
            <div class="product-big-image"><img ng-src="{{CURRENT_SKU.images[img].file}}" ng-if="CURRENT_SKU.images[img].file" alt=""></div>
            <ul class="preview-list" ng-show="CURRENT_SKU.images.length > 1">
                <li ng-repeat="i in CURRENT_SKU.images" ng-click="setImg($index)">
                    <a href="" title="">
                        <img ng-src="{{i.thumb}}" alt="" ng-if="i.thumb">
                    </a>
                </li>
            </ul>
        </div>
        <div class="product-information"><h1>{{CONF.name}}</h1>
            <ul class="product-info-list">
                <li ng-show="CURRENT_SKU.price.length > 0">
                    <div class="text"><span>Цена</span></div>
                    <div class="text-info">{{CURRENT_SKU.price}} руб.</div>
                </li>
                <li ng-show="CURRENT_SKU.articul.length > 0">
                    <div class="text"><span>Артикул</span></div>
                    <div class="text-info">{{CURRENT_SKU.articul}}</div>
                </li>
                <li ng-show="CURRENT_SKU.code.length > 0">
                    <div class="text"><span>Код</span></div>
                    <div class="text-info">{{CURRENT_SKU.code}}</div>
                </li>
                <li ng-repeat="i in CONF.product_options" ng-show="CONF.popt[i.key].length > 0">
                    <div class="text"><span>{{i.value}}</span></div>
                    <div class="text-info">{{CONF.popt[i.key]}}</div>
                </li>
                <li ng-repeat="i in CONF.options">
                    <div class="text"><span>{{i.value}}</span></div>
                    <div class="text-info">
                        <div class="select-block drop-container" ng-class="{open:openFilter==i.key}"><span ng-mousedown="selectClick(i.key)">{{getOptionVal(i.key)}}</span>
                            <ul class="drop-list mini">
                                <li ng-repeat="s in getOptions(i.key)"><a
                                        ng-click="setItem(i.key, s.key)">{{s.name}}</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li ng-show="CONF.descr.length > 0 || CURRENT_SKU.descr.length > 0">
                    <div class="text"><span>Описание</span></div>
                    <div class="text-info">{{CONF.descr}}<br ng-show="CONF.descr.length > 0">{{CURRENT_SKU.descr}}</div>
                </li>
            </ul>
            <div class="total-coin">
                <div class="text">Количество:</div>
                <div class="value-block"><input type="text" name="" value="1" placeholder="" priceformat
                                                ng-model="count"><a href="" title="" class="up" ng-click="incC()"></a><a
                        href="" title="" class="down" ng-click="decC()"></a></div>
                <div class="text">х</div>
                <div class="total">{{CURRENT_SKU.price}} руб.</div>
                <div class="text">=</div>
                <div class="total">{{CURRENT_SKU.price * count}} руб.</div>
            </div>
            <ul class="add-card-block" ng-hide="count < 1">
                <li><a href="" title="" class="link big" ng-click="addToBasket()">добавить в корзину</a></li>
                <?if(!$framed):?>
                <li><a href="/catalog/<?=explode('/',$_SERVER["REQUEST_URI"])[2]?>" title="" class="link big green right">продолжить покупки</a></li>
                <?endif?>
            </ul>
        </div>
        <div class="clear"></div>
    </div>
    <? \Yii::app()->clientScript->registerScriptFile('CartCtrl2.js', \CClientScript::POS_END); ?>