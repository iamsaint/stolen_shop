<div class="page full" ng-show="step==1">
    <h1 ng-if="CONF.items.length == 0 || CONF.items == null">
        Корзина пуста
    </h1>
    <ul class="basket-step-list" ng-show="CONF.items.length > 0">
        <li class="active">Список товаров</li>
        <li>Оформление товара</li>
        <li>Подтверждение информации</li>
        <li>Завершение заказа</li>
    </ul>
    <div class="basket-table-block">
        <table class="basket-table" ng-if="CONF.items.length > 0">
            <thead>
            <tr>
                <th class="th1">Товар</th>
                <th class="th2"></th>
                <th class="th3">Цена</th>
                <th class="th1">Количество</th>
                <th class="th3">Сумма</th>
                <th class="th1">Удалить</th>
            </tr>
            </thead>
            <tbody>
            <tr ng-repeat="i in CONF.items">
                <td>
                    <div class="b-t-image"><img ng-src="{{i.image}}" ng-if="i.image" alt=""></div>
                </td>
                <td class="align-left"><a href="{{i.link}}" title="">{{i.name}}</a>
                    <ul class="b-t-info">
                        <li ng-repeat="s in i.scu" ng-if="i.scu"><strong>{{s.title}}:</strong> {{s.name}}</li>

                        <li ng-if="!i.scu"><strong>Размер:</strong> {{i.size}}</li>
                        <li ng-if="!i.scu"><strong>Производитель:</strong> {{i.manufacturer}}</li>
                        <li ng-if="!i.scu"><strong>Ткань:</strong> {{i.material}}</li>
                        <li ng-if="!i.scu"><strong>Категория:</strong> {{i.category}}</li>
                    </ul>
                </td>
                <td>
                    <div class="t-coin">{{i.price}} руб.</div>
                </td>
                <td>
                    <div class="value-block"><input type="text" name="" value="{{i.count}}" placeholder="" readonly><a
                            href="" title="" class="up" ng-click="incC(i)"></a><a href="" title="" class="down"
                                                                                  ng-click="decC(i)"></a></div>
                </td>
                <td>
                    <div class="t-coin"><strong>{{i.count * i.price}} руб.</strong></div>
                </td>
                <td><a href="" title="" class="delete" ng-click="rem(i)"></a></td>
            </tr>
            </tbody>
        </table>
    </div>
    <ul class="step-list">
        <li><a href="/" title="" class="link big green left">продолжить покупки</a></li>
        <li><a href="javascript:void(0)" ng-click="step=2" title="" ng-show="CONF.items.length > 0" class="link big right">Продолжить оформление заказа</a></li>
        <li>
            <div class="itogo" ng-show="CONF.items.length > 0">Итого: <span>{{getTotal()}} pуб.</span></div>
        </li>
    </ul>
</div>