<div class="page full" ng-show="step==4">
    <ul class="basket-step-list">
        <li>Список товаров</li>
        <li>Оформление товара</li>
        <li>Подтверждение информации</li>
        <li class="active">Завершение заказа</li>
    </ul>
    <div class="complite-block">
        <div class="title">Заказ успешно завершен!</div>
        <div class="text">В ближайшее время с вами свяжется наш менеджер, что бы подтвердить покупку. <br>Так же мы
            можете ознакомиться со следующими услугами нашего комплекса
        </div>
        <ul class="complite-block-list">
            <li><a target="_self" href="/" title="">Перейти в магазин</a></li>
            <li><a target="_self" href="/" title="">Посмотреть все товары магазина</a></li>
            <li><a target="_self" href="<?= Yii::app()->params['baseHost']?>/roznichnym-pokupaletyam/avtobusnye-shop-tury/" title="">Узнать что такое «Шоп-Тур»</a></li>
        </ul>
    </div>
</div>