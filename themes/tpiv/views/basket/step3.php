<div class="page full" ng-show="step==3">
    <ul class="basket-step-list">
        <li><a href="javascript:void(0)" ng-click="step=1" title="">Список товаров</a></li>
        <li><a href="javascript:void(0)" ng-click="step=2" title="">Оформление товара</a></li>
        <li class="active">Подтверждение информации</li>
        <li>Завершение заказа</li>
    </ul>
    <div class="basket-table-block">
        <table class="basket-table">
            <thead>
            <tr>
                <th class="th1">Товар</th>
                <th class="th2"></th>
                <th class="th3">Цена</th>
                <th class="th1">Количество</th>
                <th class="th3">Сумма</th>
            </tr>
            </thead>
            <tbody>
            <tr ng-repeat="i in CONF.items">
                <td>
                    <div class="b-t-image"><img ng-src="{{i.image}}" ng-if="i.image" alt=""></div>
                </td>
                <td class="align-left"><h3>{{i.name}}</h3>
                    <ul class="b-t-info">
                        <li ng-repeat="s in i.scu" ng-if="i.scu"><strong>{{s.title}}:</strong> {{s.name}}</li>
                        <li><strong>Категория:</strong> {{i.category}}</li>

                        <li ng-if="!i.scu"><strong>Размер:</strong> {{i.size}}</li>
                        <li ng-if="!i.scu"><strong>Производитель:</strong> {{i.manufacturer}}</li>
                        <li ng-if="!i.scu"><strong>Ткань:</strong> {{i.material}}</li>
                        <li ng-if="!i.scu"><strong>Категория:</strong> {{i.category}}</li>
                    </ul>
                </td>
                <td>
                    <div class="t-coin">{{i.price}} руб.</div>
                </td>
                <td>
                    <div class="t-coin">{{i.count}}</div>
                </td>
                <td>
                    <div class="t-coin"><strong>{{i.count * i.price}} руб.</strong></div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="total-info">
        <ul class="total-info-list">
            <li><span>ФИО</span>{{fio}}</li>
            <li><span>Индекс</span>{{zip}}</li>
            <li><span>Транспортная компания</span>{{CONF.delivery[delivery]}}</li>
            <li><span>Контактный телефон</span>{{phone}}</li>
            <li><span>Город</span>{{city}}</li>
            <li><span>Комментарий</span>{{comment}}</li>
            <li><span>Электронная почта</span>{{email}}</li>
            <li><span>Адрес доставки</span>{{address}}</li>
        </ul>
        <div class="itogo">Итого: <span>{{getTotal()}} pуб.</span></div>
        <div class="clear"></div>
    </div>
    <div class="info-user-block">
        <ul class="step-list">
            <li><a href="javascript:void(0)" ng-click="step=2" title="" class="link big green left">назад</a></li>
            <li><a href="javascript:void(0)" ng-click="saveOrder()" title="" class="link big right">Продолжить
                    оформление заказа</a></li>
        </ul>
    </div>
</div>