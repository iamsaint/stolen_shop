function BasketCtrl($scope, $http, $rootScope) {
    $scope.CONF = PHPCONFIG;
    $scope.step = 1;

    $scope.setItem = function(i){
        $scope.delivery = null;
        var keys = Object.keys($scope.CONF.delivery);
        for(var j = 0; j < keys.length; j++) {
            if(i==$scope.CONF.delivery[keys[j]]){
                $scope.delivery = keys[j];
                break;
            }
        }
        $scope.selectClick();
    }

    $scope.getItem = function(){
        var itm = $scope.CONF.delivery[$scope.delivery];
        return itm==undefined?'Не выбрана':$scope.CONF.delivery[$scope.delivery];
    }

    $scope.selectClick = function() {
        $scope.openFilter =  !$scope.openFilter;
    }

    $scope.incC = function (i) {
        if(i.scu){
            $http.post('/basket/addscu', {scu: i.id, count: 1}).success(function (res) {
                i.count = parseInt(i.count) + 1;
                $rootScope.$broadcast('widgets::basket::plusOne', {
                    count: 1,
                    price: parseInt(i.price)
                });
            });
        } else {
            $http.post('/basket/add', {id: i.id, count: 1}).success(function (res) {
                i.count = parseInt(i.count) + 1;
                $rootScope.$broadcast('widgets::basket::plusOne', {
                    count: 1,
                    price: parseInt(i.price)
                });
            });
        }
    }

    $scope.decC = function (i) {
        if (parseInt(i.count) - 1 > 0) {
            if(i.scu) {
                $http.post('/basket/delscu', {scu: i.id, count: 1}).success(function (res) {
                    i.count = parseInt(i.count) - 1;
                    $rootScope.$broadcast('widgets::basket::minusOne', {
                        count: 1,
                        price: parseInt(i.price)
                    });
                });
            } else {
                $http.post('/basket/del', {id: i.id, count: 1}).success(function (res) {
                    i.count = parseInt(i.count) - 1;
                    $rootScope.$broadcast('widgets::basket::minusOne', {
                        count: 1,
                        price: parseInt(i.price)
                    });
                });
            }
        } else
            $scope.rem(i, true);
    }

    $scope.rem = function (i, s) {
        if (!s && !confirm('Вы действительно хотите удалить товар из корзины?')) return;
        if(i.scu) {
            $http.post('/basket/remscu', {scu: i.id}).success(function (res) {
                if (res.status == 1) {
                    $rootScope.$broadcast('widgets::basket::minusOne', {
                        count: i.count,
                        price: parseInt(i.price)
                    });
                    $scope.CONF.items.splice($scope.CONF.items.indexOf(i), 1);
                } else
                    alert('Ошибка при удалении товара');
            });
        } else {
            $http.post('/basket/rem', {id: i.id}).success(function (res) {
                if (res.status == 1) {
                    $rootScope.$broadcast('widgets::basket::minusOne', {
                        count: i.count,
                        price: parseInt(i.price)
                    });
                    $scope.CONF.items.splice($scope.CONF.items.indexOf(i), 1);
                } else
                    alert('Ошибка при удалении товара');
            });
        }
    };

    $scope.getTotal = function () {
        var price = 0;
        for (var i = 0, j = Object($scope.CONF.items).length; i < j; i++)
            price += parseInt($scope.CONF.items[i].count) * $scope.CONF.items[i].price;
        return price;
    }

    $scope.valideteForm = function () {
        $scope.formFields = JSON.parse(angular.toJson($scope.cform));
        $scope.error = [];
        var res = true;

        angular.forEach($scope.formFields, function (v, k) {

            $scope.error[k] = $scope.error && !$scope.cform[k].$valid;

            res = res && $scope.cform[k].$valid;
        });

        if (res) $scope.step = 3;
    }

    $scope.saveOrder = function () {
        var data = {};
        angular.forEach($scope.formFields, function(v, k) {
           data[k] = $scope[k];
        });

        $http.post('/basket/saveorder', {
            data: data
        }).success(function () {
            $rootScope.$broadcast('widgets::basket::flush');
            $scope.step = 4;
        });
    }
}