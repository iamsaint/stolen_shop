'use strict';

function CartCtrl($scope, $http, $rootScope) {
    $scope.CONF = PHPCONFIG;
    $scope.package = 0;
    $scope.size = 0;
    $scope.count = 1;
    $scope.img = 0;
    $scope.setScu = function() {
        $scope.sku = $scope.CONF.sku[$scope.CONF.sizes[$scope.size].id];
        $http.get('/site/geticomp?item='+$scope.CONF.id+'&comp='+$scope.sku.comp).success(function(res){
            $scope.icomp = res.id;
        });
    }

    $scope.$watch('count', function() {
        if(typeof $scope.count=='string');
        $scope.count = parseInt($scope.count);
        if(isNaN($scope.count) || parseInt($scope.count) < 1)
            $scope.count = 1;
    })

    $scope.incC = function() {
        $scope.count = parseInt($scope.count)+1;
    }

    $scope.decC = function() {
        if(parseInt($scope.count) - 1 > 0)
            $scope.count = parseInt($scope.count)-1;
    }

    $scope.setImg = function(i)
    {
        $scope.img = i;
    }


    $scope.selectClick = function(item) {
        var tmp = $scope[item];
        $scope.openPackSelect = false;
        $scope.openSizeSelect = false;
        $scope[item] = !tmp;
    }
    /*$scope.$on('select::close', function(e,p) {
        if((p.getAttribute('class')!='select-block drop-container open')&&(true)) {
            document.getElementsByClassName('select-block drop-container open')[0].setAttribute('class','select-block drop-container');
        }
    })*/
    $scope.setItem = function(id, val) {
        $scope[id] = val;
        $scope.selectClick();
        $scope.setScu();
    }

    $scope.addToBasket = function() {
        $http.post('/basket/add', {
            'id' : $scope.icomp,
            'count' : $scope.count
        }).success(function(result){
            $rootScope.$broadcast('widgets::basket::plusOne', {
                count: $scope.count,
                price: $scope.sku.price
            });
            notify(result.message);
        });
    }

    $scope.setScu();
}