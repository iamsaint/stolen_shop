'use strict';
function notify(msg){
    document.getElementById('overflow-notification').innerHTML = msg+'<span class="notify-close" onclick="closeNotify()">Закрыть</span>';
    document.getElementsByClassName('overflow-note-wrapper')[0].setAttribute('class','overflow-note-wrapper active');
    document.getElementById('overflow-notification').setAttribute('class','active');
}
function closeNotify(){
    document.getElementsByClassName('overflow-note-wrapper')[0].setAttribute('class','overflow-note-wrapper');
    document.getElementById('overflow-notification').setAttribute('class','');
}
var app = angular.module('myApp', ['ngStorage']);

app.config( ['$locationProvider', function ($locationProvider) {
        $locationProvider.html5Mode(true).hashPrefix('!');
    }]);

app.directive('whenScrolled', function() {
    var ua = navigator.userAgent.toLowerCase();
    var isOpera = (ua.indexOf('opera')  > -1);
    var isIE = (!isOpera && ua.indexOf('msie') > -1);

    function getDocumentHeight() {
        return Math.max(document.compatMode != 'CSS1Compat' ? document.body.scrollHeight : document.documentElement.scrollHeight, getViewportHeight());
    }

    function getViewportHeight() {
        return ((document.compatMode || isIE) && !isOpera) ? (document.compatMode == 'CSS1Compat') ? document.documentElement.clientHeight : document.body.clientHeight : (document.parentWindow || document.defaultView).innerHeight;
    }

    return function(scope, elm, attr) {

        var win = angular.element(document),
            itm = document.getElementById('smartfilter'),
            top = itm.getAttribute('data-top'),
            parent = itm.parentNode,
            scrollToTopBtn = document.getElementById('scroll-to-top');

        win.bind('scroll', function(e) {
            if ((getDocumentHeight() - (window.pageYOffset || document.documentElement.scrollTop) -getViewportHeight() -400) <= 0) {
                scope.$apply(attr.whenScrolled);
            }
            if((window.pageYOffset || document.documentElement.scrollTop) > 400){
                scrollToTopBtn.setAttribute('class','active');
            }else{
                scrollToTopBtn.removeAttribute('class');
            }
            if((window.pageYOffset || document.documentElement.scrollTop) > top) {
                itm.classList.add("sticker");
                parent.setAttribute('style', 'height :' + (top / 2) + 'px;')
            } else {
                itm.classList.remove("sticker");
                parent.setAttribute('style', '')
            }
        });
    };
});

app.directive('priceformat', function($sce) {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function (inputValue) {
                if (inputValue == undefined) return ''
                var transformedInput = inputValue.replace(/[^0-9\;]/g, '');
                if (transformedInput != inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }
                return transformedInput;
            });
        }
    };
});


app.filter('unsafe', function($sce) {
    return function(val) {
        return $sce.trustAsHtml(val);
    };
});

app.filter('firstProp', function($sce) {
    return function(val) {
        for (var i in val) {
            return val[i];
            break;
        }
    }
});

app.filter('filterMultiple',['$filter',function ($filter) {
    return function (items, keyObj) {
        var filterObj = {
            data:items,
            filteredData:[],
            applyFilter : function(obj,key){
                var fData = [];
                if (this.filteredData.length == 0)
                    this.filteredData = this.data;
                if (obj){
                    var fObj = {};
                    if (!angular.isArray(obj)){
                        fObj[key] = obj;
                        fData = fData.concat($filter('filter')(this.filteredData,fObj));
                    } else if (angular.isArray(obj)){
                        if (obj.length > 0){
                            for (var i=0;i<obj.length;i++){
                                if (angular.isDefined(obj[i])){
                                    fObj[key] = obj[i];
                                    fData = fData.concat($filter('filter')(this.filteredData,fObj));
                                }
                            }

                        }
                    }
                    if (fData.length > 0){
                        this.filteredData = fData;
                    }
                }
            }
        };
        if (keyObj){
            angular.forEach(keyObj,function(obj,key){
                filterObj.applyFilter(obj,key);
            });
        }
        return filterObj.filteredData;
    }
}]);
/*app.run(function($rootScope){
    $rootScope.dropListClose = function(e)
    {
       $rootScope.$broadcast('select::close', e.target);
}})*/