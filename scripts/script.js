$(document).ready(function() {

	// вкладки схем
	animate_tab(0, 1);

	$(document).on('click','.drop-container',function(){
		var obj = $(this);
		if (obj.hasClass('open')) {
			$('.drop-container').removeClass('open');
		} else {
			$('.drop-container').removeClass('open');
			obj.addClass('open');
		};
		
		
	});

	
	$('.plan-nav li').click(function () {
	  var paneCurrent = $(this),
	    index = paneCurrent.index(),
	    time = 300;
	  $('.plan-nav li').removeClass('active').eq(index).addClass('active');
	  animate_tab(index, time);
	});

	function animate_tab (index, time) {
	  $('.plan-pane li').removeClass('active').css('position','absolute').eq(index).addClass('active');
	  var height = $('.plan-pane li.active').height();
	  $('.plan-pane').animate({
	    'minHeight': height
	  }, time, function(){
	    $('.plan-pane li').eq(index).css({
	      position: 'relative'
	    });
	  });
	}


	// рассписание в шапке
	$('.drop').click(function () {
		var obj = $(this).parent();
		var drop = obj.find('.drop-time');
		if (obj.hasClass('open')) {
			drop.fadeOut(200,function(){
				obj.removeClass('open');
			});
		} else {
			drop.fadeIn(1,function(){
				obj.addClass('open');
			});
		};
	});


	// скролл галереи
	var len = ($('.handle .item').length * 200) - 20;
	$('.dragdealer .handle').width(len);

	new Dragdealer('image-carousel', {
		// steps: 4,
		speed: 0.3,
	 	loose: true
	});


	// faq 
	$('.faq-box span').click(function () {
		var obj = $(this).parents('.faq-box');
		if (obj.hasClass('open')) {
			obj.find('.answer').slideUp(200,function(){
				obj.removeClass('open');
			});
		} else{
			obj.find('.answer').slideDown(200,function(){
				obj.addClass('open');
			});
		};
	});


	// service
	$('.service').click(function () {
		var obj = $(this);
		if (obj.hasClass('open')) {
			obj.removeClass('open');
		} else{
			obj.addClass('open');
		};
	});


	// sales-block
	$('.panel-sale .item').click(function () {
		var obj = $(this);
		var index = obj.index();
		$('.panel-sale .item').removeClass('active').eq(index).addClass('active');
		$('.sales-block .slide').removeClass('active').eq(index).addClass('active');
		return false;
	});


	// GRID

   $('.on').click(function () {
   	var height = $('.wrapper').height();
   	$('.grid-block').height(height);
   	var on = $(this);
   	var grid = $('.grid-block');
   	if (on.hasClass('check')) {
   		on.text('off').removeClass('check');
   		grid.removeClass('check');
   	} else {
   		on.text('on').addClass('check');
   		grid.addClass('check');
   	};
   });


});