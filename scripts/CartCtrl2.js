'use strict';

function CartCtrl($scope, $http, $filter, $rootScope) {
    $scope.CONF = PHPCONFIG;
    $scope.package = 0;
    $scope.size = 0;
    $scope.count = 1;
    $scope.img = 0;
    $scope.openFilter = 0;
    $scope.CURRENT_SKU = $filter('firstProp')($scope.CONF.scu);
    if($scope.CURRENT_SKU.images == undefined)
        $scope.CURRENT_SKU.images = $scope.CONF.images;

    $scope.getOptions = function(itemIndex) {
        var data = $filter('filter')($scope.CONF.options, {key : itemIndex})[0],
            filter = {};

        angular.forEach($scope.CONF.scu, function(val, key){
            angular.forEach(val.options, function(v, k){
                if(filter[k]==undefined)
                    filter[k]=[];
                if(filter[k].indexOf(v) < 0)
                    filter[k].push(v);
            });
        });

        return $filter('filterMultiple')(data.items, {key : filter[itemIndex]});
    }

    $scope.getOptionVal = function(itemIndex){

        var data = $filter('filter')($scope.CONF.options, {key : itemIndex})[0];

        var item = $filter('filter')(data.items, {key : $scope.CURRENT_SKU.options[itemIndex] })[0]

        return item==undefined?'Не выбрано':item.name;
    }

    $scope.setScu = function() {
        $scope.sku = $scope.CONF.sku[$scope.CONF.sizes[$scope.size].id];
        $http.get('/site/geticomp?item='+$scope.CONF.id+'&comp='+$scope.sku.id).success(function(res){
            $scope.icomp = res.id;
        });
    }

    $scope.$watch('count', function() {
        if(typeof $scope.count=='string');
            $scope.count = parseInt($scope.count);
        if(isNaN($scope.count) || parseInt($scope.count) < 1)
            $scope.count = 1;
    })

    $scope.incC = function() {
        $scope.count = parseInt($scope.count)+1;
    }

    $scope.decC = function() {
        if(parseInt($scope.count) - 1 > 0)
            $scope.count = parseInt($scope.count)-1;
    }

    $scope.setImg = function(i)
    {
        $scope.img = i;
    }


    $scope.selectClick = function(item) {
        $scope.openFilter = ($scope.openFilter == item)? 0 : item;
    }
   /* $scope.$on('select::close', function(e,p) {
        if((p.getAttribute('class')!='select-block drop-container open')&&(true)) {
            document.getElementsByClassName('select-block drop-container open')[0].setAttribute('class','select-block drop-container');
        }

    })*/
    $scope.setItem = function(optionId, optionValId) {

        var isExists = false, filter = {};

        angular.forEach($scope.CURRENT_SKU.options, function(v, k){
            filter[k]=v;
        });

        filter[optionId] = optionValId;

        var TMP_SCU = [];
        angular.forEach($scope.CONF.scu, function(v, k){
           TMP_SCU.push(v);
        });
        angular.forEach(filter, function(v, k){
            var obj = { options : {}};
            obj.options[k] = v;
            TMP_SCU = $filter('filter')(TMP_SCU, obj)
        });

        if(TMP_SCU.length>=1) {
            $scope.CURRENT_SKU = TMP_SCU[0];
        }
         else {
            TMP_SCU = [];
            angular.forEach($scope.CONF.scu, function(v, k){
                TMP_SCU.push(v);
            });

            angular.forEach(filter, function(v, k ){
                if(k==optionId) {
                    var obj = { options : {}};
                    obj.options[k] = v;
                    TMP_SCU = $filter('filter')(TMP_SCU, obj)
                }
            });

            if(TMP_SCU.length > 0)
                $scope.CURRENT_SKU = TMP_SCU[0];
        }

        if($scope.CURRENT_SKU.images == undefined)
            $scope.CURRENT_SKU.images = $scope.CONF.images;

        $scope.img = 0;
        $scope.openFilter = 0;
    }

    $scope.getCurrentSCUID =function(){
        var res = false;
        angular.forEach($scope.CONF.scu, function(v,k){
            if($scope.CURRENT_SKU.options== v.options)
                res = k;
        });
        return res;
    }

    $scope.addToBasket = function() {
        var scuId = $scope.getCurrentSCUID();
        if(scuId)
        $http.post('/basket/addscu', {
            'scu' : scuId,
            'count' : $scope.count
        }).success(function(result){
            $rootScope.$broadcast('widgets::basket::plusOne', {
                count: $scope.count,
                price: $scope.CURRENT_SKU.price
            });
            notify(result.message);
        });
    }
}
