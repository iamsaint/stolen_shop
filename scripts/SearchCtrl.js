function SearchCtrl($scope) {
    $scope.items = PHPCONFIG.items;
    $scope.openCard = function(i) {
        $scope.CURRENT_CATEGORY = i.category_data;
        $scope.itemCardCode = i.code;
        $scope.itemCardOpen = true;
    }

    $scope.closeCard = function() {
        $scope.itemCardOpen = false;
    }
}
