function ScrollCtrl($scope){
    $scope.smoothScrollTop = function()  {
    var timer, start, factor;
    return function () {
        var offset = window.pageYOffset,
            y = 0,
            duration = 200;
        start = Date.now();
        factor = 1;
        if( timer ) {
            clearTimeout(timer);
        }
        function step() {
            factor = 1 - (Date.now()-start)/duration;
            y = factor * offset ;
            window.scrollTo(0,y);
            timer = setTimeout(step, 20);
            if( factor <= 0 ) {
                clearTimeout(timer);
                factor = 0;
                window.scrollTo(0,0);
            }
        }
        timer = setTimeout(step, 20);
        return timer;
    };
}
}