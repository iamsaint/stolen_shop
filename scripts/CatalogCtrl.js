'use strict';
function CatalogCtrl($scope, $timeout, $http, $location, $localStorage) {
    $scope.CONF = PHPCONFIG;

    $scope.selectClick = function(item) {
        var tmp = $scope[item];
        $scope.openCatSelect = false;
        $scope.openManSelect = false;
        $scope.openMatSelect = false;
        $scope.openSizeSelect = false;
        $scope[item] = !tmp;
    }

    $scope.openFilter = -1;

    $scope.selectClick2 = function(item, id) {
        $scope.openFilter = $scope.openFilter == id? -1 : id;
    }

    $scope.getItem = function(id, val) {
        var name = 'Все';
        angular.forEach($scope.CONF[id], function(v,k){
            if(v.id==val)
               name = v.name;
        });

        return name;
    }

    $scope.setItem = function(i, val) {
        var ind = $scope.filterValues[i.id].indexOf(val);
        if(ind>=0)
            $scope.filterValues[i.id].splice(ind, 1);
        else
            $scope.filterValues[i.id].push(val);
    }

    $scope.setItem2 = function(id, val) {
        $scope['filter_'+id] = val;
        $scope.selectClick();
        setTimeout(function() {
            $scope.$apply();
            document.getElementById('smartfilter').submit();
        }, 10);
    }

    $scope.setLocation = function(i){
        window.location = i;
    }

    $scope.loadMore = function(params) {
        if($scope.CURRENT_CATEGORY == undefined){
            $timeout(function(){$scope.loadMore(params)}, 10);
            return;
        }
        if(!$scope.canload) return;
        $scope.canload = false;
        $scope.page += 1;
        if(params==undefined) {
            params = {}
            angular.forEach($scope.filters, function(v,k){
                if($scope.filterValues[v.id]!=undefined && $scope.filterValues[v.id].length > 0) {
                    if(params[v.id] == undefined) params[v.id] = [];
                    angular.forEach($scope.filterValues[v.id], function(val, key){
                        params[v.id].push(v.values[val].id)
                    })
                }
            });
        }

        var url = '/api/items/catalog/' + $scope.CURRENT_CATEGORY.id+ '/page/'+$scope.page;

        delete $localStorage.categoryFilter;
        $localStorage.categoryFilter = {};

        if(params !=undefined) {
            $location.url($location.path());
            $localStorage.categoryFilter = params;
                angular.forEach(params, function(v, k){
                if(v.length > 0) {
                    url = url + '/filters['+k+']/'+v;
                    $location.search('filters['+k+']', v);
                }
            });
        }

        $http.get(url).success(function(data){
            $scope.canload = true;
            if($scope.page == 1 || $scope.items==undefined)
                $scope.items = data;
            else
                for(var i= 0, j=data.length;i<j;i++)
                    $scope.items.push(data[i]);
        }).error(function() {
            $scope.canload = true;
        });
    }

    $scope.setCategory = function(cat){
        $scope.openFilter = -1;
        $scope.page = 0;
        $scope.canload = true;
        $http.get('/api/categories').success(function(data) {
            $scope.categories = data;
            angular.forEach(data, function(v,k){
                if(v.id==cat) {
                    if($scope.CURRENT_CATEGORY == undefined)
                        $scope.CURRENT_CATEGORY = v;
                    else {
                        $scope.CURRENT_CATEGORY = v;
                        $location.path('/catalog/'+$scope.CURRENT_CATEGORY.code);
                    }
                }
            })
            $scope.setFilters();
        });
    }

    $scope.applyFilter = function() {
        var data = {},
            filterLength = 0;

        angular.forEach($scope.filters, function(v,k){
            if(data[v.id]==undefined) data[v.id] = [];
            if($scope.filterValues[v.id]!=undefined && $scope.filterValues[v.id].length > 0) {
                angular.forEach($scope.filterValues[v.id] , function(val, key){
                    data[v.id].push(v.values[val].id);
                    filterLength++;
                });
            }
        });
        $scope.isFilterApply = filterLength > 0;
        $scope.openFilter = -1;
        $scope.page = 0;
        $scope.canload = true;

        $scope.loadMore(data);
    }

    $scope.clearFilters = function() {
        angular.forEach($scope.filterValues, function(v,k) {
            $scope.filterValues[k] = [];
        })
        $scope.applyFilter();
    }

    $scope.setFilters = function() {
        $http.get('/api/filters/category/'+$scope.CURRENT_CATEGORY.id).success(function(data) {
            $scope.filters = data;
            $scope.filterValues = {}
            var params = $location.search(),
                categoryFilter = $localStorage.categoryFilter,
                filterLength = 0;
            if(categoryFilter==undefined) categoryFilter = {};
            angular.forEach($scope.filters, function(v,k){
                if(params['filters['+ v.id +']']!=undefined && params['filters['+ v.id +']'].length > 0) {
                    angular.forEach(v.values, function(val, key){
                       if(params['filters['+ v.id +']'].indexOf(val.id)>=0) {
                           if($scope.filterValues[v.id]==undefined)
                               $scope.filterValues[v.id] = [];
                           $scope.filterValues[v.id].push(key);
                           filterLength++;
                       }
                    });
                } else if(categoryFilter[v.id]!=undefined && categoryFilter[v.id].length > 0){
                    angular.forEach(v.values, function(val, key){
                        if(categoryFilter[v.id].indexOf(val.id)>=0) {
                            if($scope.filterValues[v.id]==undefined)
                                $scope.filterValues[v.id] = [];
                            $scope.filterValues[v.id].push(key);
                            filterLength++;
                        }
                    });
                } else
                    $scope.filterValues[v.id] = [];
            });

            $scope.isFilterApply = filterLength > 0;
            $scope.page = 0;
            $scope.loadMore();
        });
    }

    $scope.openCard = function(i){
        $scope.itemCardCode = $scope.items[i].code;
        setTimeout(function(){
            $scope.itemCardOpen = true;
            $scope.$apply();
            document.body.setAttribute('style', 'overflow: hidden');
        }, 300)
    }

    $scope.closeCard = function() {
        $scope.itemCardOpen = false;
        document.getElementsByClassName('overflow-item')[0].setAttribute('style','height:0');
        document.body.setAttribute('style', '');
    }

    $scope.setCategory($scope.CONF.category);
}