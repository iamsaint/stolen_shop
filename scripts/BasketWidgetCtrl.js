function BasketWidgetCtrl($scope, $localStorage, $timeout){
    $scope.UUIDv4 = function b(a){return a?(a^Math.random()*16>>a/4).toString(16):([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g,b)}
    $scope.uuid = $scope.UUIDv4();
    delete $localStorage.track;

    $scope.$on('widgets::basket::plusOne', function(event, track) {
        $scope.setData(
            $scope.totalBasketItemsCount + track.count,
            $scope.totalBasketPrice + track.price*track.count
        );
    });
    $scope.$on('widgets::basket::flush', function(event, track) {
        $scope.setData(0, 0);
    });

    $scope.$on('widgets::basket::minusOne', function(event, track) {
        $scope.setData(
            $scope.totalBasketItemsCount - track.count,
            $scope.totalBasketPrice - track.price*track.count
        );
    });

    $scope.setData = function(cnt, pr) {
        $scope.totalBasketItemsCount = cnt;
        $scope.totalBasketPrice = pr;

        $localStorage.track = {
            count: cnt,
            price: pr,
            uuid: $scope.uuid
        };

    }

    $scope.$watch(function() {
        return angular.toJson($localStorage);
    }, function() {
        if($localStorage.track!=undefined)
        if($localStorage.track.uuid!=undefined && $localStorage.track.uuid != $scope.uuid) {
            $scope.totalBasketItemsCount = $localStorage.track.count;
            $scope.totalBasketPrice = $localStorage.track.price;
        } else {
            $timeout(function() {
                delete $localStorage.track;
            }, 500);
        }
    });
}