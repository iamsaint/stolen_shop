<?php

$config = [
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',

    'name'=>'',
    'sourceLanguage' => 'sys',
    'language' => 'ru',

    'aliases'=>[
        'app'=>'application'
    ],

	'preload'=>['log'. 'importelastica'],

	'import'=>[
		'application.models.*',
		'application.components.*',
        'application.modules.user.models.*',
        'application.modules.user.components.*',
        'ext.ECompositeUniqueValidator.*',
        'ext.YiiMailer.YiiMailer'
	],

    'modules'=>[
        'gii'=>[
            'class'=>'system.gii.GiiModule',
            'password'=>'1',
        ],
        'user'=>[
            'tableUsers'=>'users',
            'tableProfiles'=>'profiles',
            'tableProfileFields'=>'profiles_fields',
            'hash' => 'md5',
            'sendActivationMail' => true,
            'loginNotActiv' => false,
            'activeAfterRegister' => false,
            'autoLogin' => true,
            'registrationUrl' => ['/user/registration'],
            'recoveryUrl' => ['/user/recovery'],
            'loginUrl' => ['/user/login'],
            'returnUrl' => ['/admin'],
            'returnLogoutUrl' => ['/user/login'],
        ],
        'dev',
        'admin'
    ],

    'components' => [
        'importelastica'=>array(
            'class' => 'application.extensions.ElasticaLoader',
            'libPath' => 'application.vendors',
        ),
        'clientScript' => [
            'scriptMap' => [
                'angular.js' => '/bower_components/angular/angular.min.js',
                'ngStorage.js' => '/bower_components/ngstorage/ngStorage.min.js',
                'app.js' => '/scripts/app.js',
                'script.js' => '/scripts/script.js',
                'CatalogCtrl.js' => '/scripts/CatalogCtrl.js',
                'SearchCtrl.js' => '/scripts/SearchCtrl.js',
                'BasketCtrl.js' => '/scripts/BasketCtrl.js',
                'CartCtrl.js' => '/scripts/CartCtrl.js',
                'CartCtrl2.js' => '/scripts/CartCtrl2.js',
                'BasketWidgetCtrl.js' => '/scripts/BasketWidgetCtrl.js',
                'styles.css' => '/styles/styles.css',
                'ScrollCtrl.js' => '/scripts/ScrollCtrl.js',
            ]
        ],

        'db'=>[
            'class' => 'CDbConnection',
            'connectionString' => 'mysql:host=DATABASE_HOST;dbname=DATABASE_NAME',
            'emulatePrepare' => true,
            'username' => 'DATABASE_USER',
            'password' => 'DATABASE_USER_PASSWORD',
            'charset' => 'utf8',
        ],
        'urlManager'=>[
            'urlFormat'=>'path',
            'showScriptName'=>false,
            'rules'=>[
                'catalog'=>'site/index',
                'catalog/<cat:[\w\d\-]+>/<id:[\w\d\-]+>'=>'site/item',
                'catalog/<id:[\w\d\-]+>'=>'site/catalog',
            ]
        ],

        'user'=>[
            'class' => 'WebUser',
            'allowAutoLogin'=>true,
            'loginUrl' => array('/user/login'),
        ],

        'errorHandler'=>array(
            'errorAction'=>'site/error',
        ),

        'cache'=>array(
            'class'=>'CMemCache',
            'servers'=>array(
                array(
                    'host'=>'localhost',
                    'port'=>11211,
                    'weight'=>60,
                ),
            ),
        ),

        'easyImage' => [
            'class' => 'application.extensions.easyimage.EasyImage',
            'quality' => 75,
        ],
    ]

];
if(file_exists(dirname(__FILE__).'/environment/main.php')) {
    $environment = require_once(dirname(__FILE__).'/environment/main.php');
    $config = CMap::mergeArray($config, $environment);
}

return $config;
