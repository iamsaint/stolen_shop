<form name="_form" action="/catalog/<?= $currentItem->code ?>/" method="get" class="smartfilter ng-pristine ng-valid"
      id="smartfilter">
    <div class="filter-block">
        <div class="filter-cat">
            <div class="image"><img src="<?= $currentItem->bimage->file ?>" alt=""></div>
            <div class="name drop-container" ng-class="{open:openFilter==0}"><span
                    ng-mousedown="selectClick2('openFilter', 0)"><?= $currentItem->name ?></span>
                <ul class="drop-list mini">
                    <? foreach ($category as $cat): ?>
                        <li>
                            <a href="javascript:void(0)" onmousedown="window.location='/catalog/<?= $cat->code ?>/'"
                                   title=""><?= $cat->name ?></a>
                        </li>
                    <? endforeach ?>
                </ul>
             </div>

            <div class="text"><?= $currentItem->descr ?></div>
        </div>
        <ul class="filter-select-block">
            <? foreach ($catalogs as $cat): ?>
                <?
                    $flName = 'Все';
                    foreach($filters[$cat->id] as $fcat )
                        if($fcat['id']==Yii::app()->request->getParam('filter')[$cat->id])
                            $flName = $fcat['name'];
                ?>
                <li><input name="filter[<?= $cat->id ?>]" ng-model="filter_<?= $cat->id ?>" style="display: none"
                           ng-init="filter_<?= $cat->id ?>=<?= Yii::app()->request->getParam('filter', [$cat->id => 0])[$cat->id] ?>">

                <div class="filter-title"><?= $cat->name ?></div>
                <div class="select-block drop-container" ng-class="{open:openFilter==<?= $cat->id ?>}"><span
                        ng-mousedown="selectClick2('openFilter',<?= $cat->id ?>)"><?= $flName ?></span>
                    <ul class="drop-list">
                        <li><a ng-mousedown="setItem2(<?= $cat->id ?>, 0)">Все</a>
                        </li>
                            <li ng-repeat="i in CONF[<?=$cat->id?>] | orderBy: 'name'"><a ng-mousedown="setItem2(<?= $cat->id ?>, i.id)">{{i.name}}</a>
                    </ul>
                </div></li><? endforeach ?></ul>
        <div class="clear"></div>
    </div>
</form>