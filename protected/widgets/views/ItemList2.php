<div class="mag-catalog-block view-middle" id="posts"><?php foreach ($posts as $data): ?>
        <div class="mag-catalog-item">
        <div class="image">
            <a href="javascript:void(0)" onmousedown="window.location = '<?= $data['code'] ?>'">
                <?if($imagetype == 0):?>
                    <img src="<?= Yii::app()->easyImage->thumbSrcOf('.' . $data['file'], array('resize' => array('width' => 214, 'height' => 160))); ?>"></a>
                <?else:?>
                    <img class="vertical" src="<?= Yii::app()->easyImage->thumbSrcOf('.' . $data['file'], array('resize' => array('width' => 160, 'height' => 240))); ?>">
                <?endif ?>
            </a>
        </div>
        <div class="name"><?= $data['name'] ?></div>
        <div class="cena"><?= \Yii::t('catalog', 'price') ?>
            :<span><?= round($data['price']) ?> <?= \Yii::t('catalog', 'valute') ?></span>
        </div>
        <div class="add-basket"><a class="link middle" href="javascript:void(0)"
                                   onmousedown="window.location = '<?= $data['code'] ?>'"><?= \Yii::t('catalog', 'add_to_basket') ?></a>
        </div></div><?php endforeach; ?>
</div>
<?php $this->widget('ext.yiinfinite-scroll.YiinfiniteScroller', array('contentSelector' => '#posts', 'itemSelector' => 'div.mag-catalog-item', 'loadingText' => '', 'donetext' => '', 'pages' => $pages,)); ?>