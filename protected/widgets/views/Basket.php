<div class="dashed-container"><a ng-controller="BasketWidgetCtrl" href="/basket"  target="_self">
    <div class="header-basket fll">{{totalBasketItemsCount}}</div>
    <p class="basket-widget-title" >Моя корзина</p>
    <div ng-init="totalBasketItemsCount=<?=$count?>">{{totalBasketItemsCount}} <span class="basket-widget-count">товаров</span></div>
    <div class="basket-widget-price" ng-init="totalBasketPrice=<?=$price?>">На сумму: {{totalBasketPrice | currency:""}}</div>
</a>
</div>

<? \Yii::app()->clientScript->registerScriptFile('BasketWidgetCtrl.js', \CClientScript::POS_END); ?>