<div>
    <div class="smartfilter" id="smartfilter" data-top="165">
        <div class="filter-block">
            <div class="filter-cat">
                <div class="image"><img ng-src="{{CURRENT_CATEGORY.file}}" alt=""></div>
                <div class="name drop-container" ng-class="{open:openFilter==0}">
                    <span ng-mousedown="selectClick2('openFilter', 0)">{{CURRENT_CATEGORY.name}}</span>
                    <ul class="drop-list mini">
                        <li ng-repeat="i in categories">
                            <a href="javascript:void(0)" ng-mousedown="setCategory(i.id)" title="">{{i.name}}</a>
                        </li>
                    </ul>
                </div>
                <div class="text">{{CURRENT_CATEGORY.descr}}</div>
            </div>
            <ul class="filter-select-block">
                <li ng-repeat="i in filters">
                    <div class="filter-title">{{i.name}}</div>
                    <div class="select-block drop-container" ng-class="{open:openFilter==i.id}" >
                        <span ng-mousedown="selectClick2('openFilter', i.id)">{{ filterValues[i.id].length == 0? 'Все' : 'Фильтр' }}</span>
                        <div class="drop-list">
                        <ul class="drop-list-inner" style="padding-bottom: 0px">
                            <li class="drop-list-item" ng-repeat="n in i.values" ng-if="$index <= i.values.length / 3">
                                <label for="filter_cb_{{i.id}}_{{$index}}">
                                    <a ng-mousedown="setItem(i, $index)">
                                        <input id="filter_cb_{{i.id}}_{{$index}}" type="checkbox" ng-checked="filterValues[i.id].indexOf($index) >= 0">
                                        {{ n.name }}
                                    </a>
                                </label>
                            </li>
                        </ul>
                            <ul class="drop-list-inner" style="padding-bottom: 0px">
                                <li class="drop-list-item" ng-repeat="n in i.values" ng-if="($index <= i.values.length*2/3)&&($index > i.values.length / 3)">
                                    <label for="filter_cb_{{i.id}}_{{$index}}">
                                        <a ng-mousedown="setItem(i, $index)">
                                            <input id="filter_cb_{{i.id}}_{{$index}}" type="checkbox" ng-checked="filterValues[i.id].indexOf($index) >= 0">
                                            {{ n.name }}
                                        </a>
                                    </label>
                                </li>
                            </ul>
                        <ul class="drop-list-inner" style="padding-bottom: 0px">
                            <li class="drop-list-item" ng-repeat="n in i.values" ng-if="$index > i.values.length*2/3">
                                <label for="filter_cb_{{i.id}}_{{$index}}">
                                    <a ng-mousedown="setItem(i, $index)">
                                        <input id="filter_cb_{{i.id}}_{{$index}}" type="checkbox" ng-checked="filterValues[i.id].indexOf($index) >= 0">
                                        {{ n.name }}
                                    </a>
                                </label>
                            </li>

                        </ul>
                            <a  class="drop-list-item-clr" ng-click="applyFilter()">
                                Применить фильтр
                            </a>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <a class="smartfilter-clr-btn" ng-click="clearFilters()" ng-show="isFilterApply">очистить все фильтры</a>
    </div>
</div>