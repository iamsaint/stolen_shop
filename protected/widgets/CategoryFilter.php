<?php


class CategoryFilter extends CWidget {
    public $current;
    public $manufacturer;
    public $material;
    public $size;

    public function run() {

        if(Yii::app()->request->isAjaxRequest) return;
        $this->controller->jsParams['category'] = $this->current;
        $this->render('CategoryFilter');

        /*$dependency = new CDbCacheDependency('SELECT MAX(update_time) FROM categories');
        $category = $model = Categories::model()->cache(1000,$dependency)->with('bimage')->findAll();

        foreach($category as $cat)
            if($cat->id==$this->current)
                $currentItem = $cat;

        if($this->current==1) {
            $this->render('CategoryFilter', [
                'category'=>$category,
                'currentItem'=>$currentItem,
                'current'=>$this->current,
                'manufacturer'=>$this->manufacturer,
                'material'=>$this->material,
                'size'=>$this->size,
                'filters'=>$this->getFilters($this->current)
            ]);
        } else {
            $catalogs = Catalogs::model()->findAllByAttributes(['category'=>$this->current, 'filter'=>1]);

            $this->render('CategoryFilter2', [
                'category'=>$category,
                'currentItem'=>$currentItem,
                'manufacturer'=>$this->manufacturer,
                'material'=>$this->material,
                'size'=>$this->size,
                'filters'=>$this->getFilters2($catalogs),
                'catalogs'=>$catalogs
            ]);
        }*/
    }

    private function getFilters2($catalogs) {
        $result = [];
        foreach($catalogs as $cat) {
            foreach($cat->catalogitems as $itm)
            $result[$cat->id][]=['id'=>$itm->id, 'name'=>$itm->name];
        }
        $this->controller->jsParams = $result;
        return $result;
    }

    private function getFilters($cat) {
        $res = [
            'manufacturer'=>[],
            'material'=>[],
            'size'=>[],
        ];

        $criteria = new CDbCriteria;
        $criteria->condition = 'category=:cat and micat.active=:active';
        $criteria->params = [':cat'=>$cat, ':active'=>1];
        $criteria->with = ['micat'];
        $criteria->together=true;
        $criteria->select=['manufacturer', 'material', 'size'];
        $criteria->distinct = true;

        $dependency = new CDbCacheDependency('SELECT MAX(update_time) FROM items');

        $scriteria = new CDbCriteria;
        $scriteria->order = 'name ASC';

        $manufacturers = Manufacturers::model()->cache(1000, $dependency)->findAll($scriteria);
        $materials = Materials::model()->cache(1000, $dependency)->findAll($scriteria);
        $sizes = Sizes::model()->cache(1000, $dependency)->findAll($scriteria);

        foreach($manufacturers as $m) $rs['manufacturer'][$m->id]=$m;
        foreach($materials as $m) $rs['material'][$m->id]=$m;
        foreach($sizes as $m) $rs['size'][$m->id]=$m;

        $cp = Complectations::model()->cache(1000, $dependency)->findAll($criteria);

        foreach($cp as $c) {
            $resa['manufacturer'][$rs['manufacturer'][$c->manufacturer]->id] = $rs['manufacturer'][$c->manufacturer]->name;
            $resa['material'][$rs['material'][$c->material]->id] = $rs['material'][$c->material]->name;
            $resa['size'][$rs['size'][$c->size]->id] = $rs['size'][$c->size]->name;
        }
        foreach($resa as $key=>$val)
            foreach($val as $k=>$v)
                $res[$key][]=['id'=>$k, 'name'=>$v];

        $this->controller->jsParams = $res;

        return $res;
    }
}