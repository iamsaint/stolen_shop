<?php

class ItemList extends CWidget {
    public $current;
    public $manufacturer;
    public $material;
    public $size;
    public $imagetype;

    public function run() {
        if($this->current ==1) {
            $conditions  = ['and', 'items.category=:category', 'icomplectations.active=1'];
            $params = [':category'=>intval($this->current)];

            if(intval($this->manufacturer) > 0) {
                $conditions[]= 'complectations.manufacturer=:mn';
                $params[':mn']=intval($this->manufacturer);
            }
            if(intval($this->material) > 0) {
                $conditions[]= 'complectations.material=:mt';
                $params[':mt']=intval($this->material);
            }
            if(intval($this->size) > 0) {
                $conditions[]='complectations.size=:sz';
                $params[':sz']=intval($this->size);
            }

            $posts = Categoryitems::findAll($conditions, $params);
            $total = count($posts) ;

            $pages = new CPagination($total);
            $pages->pageSize = 20;

            $dataProvider=new CArrayDataProvider($posts);
            $dataProvider->setPagination($pages);

            $this->render('ItemList', array(
                'posts' => $dataProvider->getData(),
                'pages' => $pages,
            ));
        } else {

            $condition = 'category=:category';
            $params = array(':category'=>$this->current);

            $data = Yii::app()->db->createCommand()
                ->select('*')
                ->from('categoryitems2')
                ->where($condition, $params)
                ->queryAll();
            $items = [];
            foreach($data as $d) {
                $items[$d['id']]['name']=$d['name'];
                $items[$d['id']]['code']=$d['code'];
                $items[$d['id']]['price']=$d['price'];
                $items[$d['id']]['type']=$d['price'];
                $items[$d['id']]['file']=$d['file'];

                if($d['type'] == 0)
                    $items[$d['id']]['opt'][$d['catalog']] = $d['opt'];
                else
                    $items[$d['id']]['scu'][$d['catalog']] = explode(',',$d['opt']);
            }


            $posts=[];
            $filter = Yii::app()->request->getParam('filter', []);
            foreach($items as $itemId => $itm) {
                $canAdd = true;
                foreach($itm['opt'] as $key=>$val) {
                    if(isset($filter[$key]) && $filter[$key]>0 && $filter[$key] != $val) {
                        $canAdd = false;
                    }
                }

                if(!$canAdd) continue;

                foreach($itm['opt'] as $key=>$val){
                    if(isset($filter[$key]) && $filter[$key]>0 && is_array($val) && !in_array($filter[$key], $val)) {
                        $canAdd = false;
                    }
                }

                if($canAdd)
                    $posts[$itemId]=$itm;
            }

            $total = count($posts) ;
            $pages = new CPagination($total);
            $pages->pageSize = 65535;

            $dataProvider=new CArrayDataProvider($posts);
            $dataProvider->setPagination($pages);
            $this->render('ItemList2', array(
                'posts' => $dataProvider->getData(),
                'pages' => $pages,
                'imagetype' => $this->imagetype,
            ));
        }
    }
}