<?php

class Basket extends CWidget {
    public function run() {
        $items = Orders::getBasketItems();
        $price = 0;
        $count = 0;
        if(is_array($items))
        foreach($items as $item) {
            $price += $item['price'] * $item['count'];
            $count += $item['count'];
        }
        $this->render('Basket', [
           'price'=>$price,
           'count'=>$count
        ]);
    }
}