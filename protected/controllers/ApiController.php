<?php

class ApiController extends Controller {
    public function actionItems($catalog = 1, $page = 1) {
        $page = $page -1;
        $items = [];

        $catalogType = Yii::app()->db->createCommand()
            ->select('imagetype')
            ->from('categories')
            ->where('id=:id',[':id'=>$catalog])
            ->queryRow()['imagetype'];

        $condition = 'category = :category';
        $params = [':category'=>$catalog];

        $filters = Yii::app()->request->getParam('filters', []);
        if($catalog == 1) {
            if($filters[1]>0) {$condition .= ' and manufacturer in ' .$this->buildInCondition(explode(',', $filters[1]));}
            if($filters[2]>0) {$condition .= ' and material in ' .$this->buildInCondition(explode(',', $filters[2]));}
            if($filters[3]>0) {$condition .= ' and size in ' .$this->buildInCondition(explode(',', $filters[3]));}

            $items = Yii::app()->db->createCommand()
                ->select('distinct id, name, code, file, ceil(min(price)) as price')
                ->from('categoryitems')
                ->where($condition, $params)
                ->group('id')
                ->order('creation_time DESC')
                ->limit(20)
                ->offset(20*$page)
                ->queryAll();

        } else {
            $sql = 'select
                distinct
                items.id,
                items.code,
                items.name,
                items.creation_time,
                ceil(min(if(scu.price is null, items.price, scu.price))) as price,
                min(if(i2.file is null, i1.file, i2.file)) as file
                from
                items
                join (select distinct id from( SELECT
                distinct id ';

            if(count($filters)>0){
                foreach($filters as $k=>$v){
                    $sql .= ' , max(if(catalogitem='.intval($k).', value, null)) as filter_'.intval($k);
                }
            }

            $sql .='
                FROM
                (
                        select
                        distinct
                        items.id,
                        scuitems.catalogitem,
                        scuitems.value
                    from items
                    join scu ON scu.itemid = items.id
                    join scuitems on scuitems.scu = scu.id
                    WHERE
                        items.category = '.intval($catalog).'
                        and items.active = 1
                        and scu.active = 1
                    UNION
                    select
                        distinct
                        items.id,
                        item_options.catalogitem,
                        item_options.value
                    from items
                    join item_options ON item_options.itemid = items.id
                    WHERE
                        items.category = '.intval($catalog).'
                        and items.active = 1
                    ORDER BY id, catalogitem, value
                ) t ';

            if(count($filters)>0){
                $sql .= ' where 1=1 and (';
                $farray = [];
                foreach($filters as $k=>$v){
                    $farray[]='(catalogitem = '.intval($k).' and value in '.$this->buildInCondition(explode(',',$v)).')';
                }
                $sql .= implode(' or ', $farray).') group by id) t where 1=1 ';

                foreach($filters as $k=>$v){
                    $sql .= ' and filter_'.intval($k).' in '.$this->buildInCondition(explode(',',$v));

                }
            } else $sql .= ') t ';

            $sql .=') t on t.id = items.id
                left join (	select id, min(price) as price, itemid from scu group by itemid HAVING min(price) > 0) scu on scu.itemid = items.id
                left join scuimages on scuimages.scu = scu.id
                left join images i1 on scuimages.image = i1.id
                left join itemimages on itemimages.item = items.id
                left join images i2 on itemimages.image = i2.id
                where
                    items.active = 1
                group by items.id order by items.creation_time DESC ';

            $sql .= ' limit '.(20*$page).', 20';

            $command = Yii::app()->db->createCommand($sql);

            $items = $command->queryAll();

        }
        foreach($items as $k=>$v) {
            if($catalogType==1)
                $items[$k]['file'] = Yii::app()->easyImage->thumbSrcOf('.'.$items[$k]['file'], array('resize' => array('width' => 160, 'height' => 214)));
            else
                $items[$k]['file'] = Yii::app()->easyImage->thumbSrcOf('.'.$items[$k]['file'], array('resize' => array('width' => 214, 'height' => 160)));
        }

        $this->renderJSON($items);
    }

    public function actionPreview($img, $type=0) {
        if(intval($type)==1)
            echo Yii::app()->easyImage->thumbSrcOf('.'.$img, array('resize' => array('width' => 160, 'height' => 214)));
        else
            echo Yii::app()->easyImage->thumbSrcOf('.'.$img, array('resize' => array('width' => 214, 'height' => 160)));
    }

    public function actionCategories(){
        $items = Yii::app()->db->createCommand()
            ->select('categories.id, categories.name, categories.code, categories.descr, images.file, categories.imagetype')
            ->join('images', 'categories.image=images.id')
            ->from('categories')
            ->queryAll();
        $this->renderJSON($items);
    }

    public function actionFilters($category = 1) {
        $filters = [];
        if($category == 1) {
            $filters[]=['id'=>1,'name'=>'Производитель', 'values'=>$this->getKPBFilters('manufacturers') ];
            $filters[]=['id'=>2,'name'=>'Материал', 'values'=>$this->getKPBFilters('materials') ];
            $filters[]=['id'=>3,'name'=>'Размер', 'values'=>$this->getKPBFilters('sizes') ];
        } else {
            $filters= $this->getFilters($category);
        }
        $this->renderJSON($filters);
    }

    private function getFilters($category) {
        $res = [];
        $sql = '
            select
                catalogs.id, catalogs.name, catalogitems.id as cid, catalogitems.name as cname
            FROM
                catalogs
            JOIN catalogitems on catalogitems.catalog = catalogs.id
            join (select
                distinct
                items.category,
                scuitems.catalogitem,
                scuitems.value
            from items
            join scu ON scu.itemid = items.id
            join scuitems on scuitems.scu = scu.id
            WHERE
                items.category = '.intval($category).'
                and items.active = 1
                and scu.active = 1
            UNION
            select
                distinct
                items.category,
                item_options.catalogitem,
                item_options.value
            from items
            join item_options ON item_options.itemid = items.id
            WHERE
                items.category = '.intval($category).'
                and items.active = 1
            ORDER BY category, catalogitem, value
            ) t on t.catalogitem=catalogs.id and t.value = catalogitems.id
            where catalogs.filter = 1 and catalogs.category = '.intval($category).'
            ORDER BY catalogs.id ASC, catalogitems.name ASC
        ';
        $data = Yii::app()->db->createCommand($sql)->queryAll();
        foreach($data as $d) {
            if(is_null($res[$d['id']]['id']))
                $res[$d['id']] = ['name'=>$d['name'], 'id'=>$d['id'], 'values'=>[]];
            $res[$d['id']]['values'][]=['name'=>$d['cname'], 'id'=>$d['cid']];
        }
        return $res;
    }

    private function getKPBFilters($table) {
        $res = [];
        $data = Yii::app()->db->createCommand()
            ->selectDistinct($table.'.id, '.$table.'.name')
            ->from($table)
            ->join('complectations', 'complectations.'.substr($table,0, strlen($table)-1).'='.$table.'.id')
            ->join('icomplectations', 'icomplectations.complectation=complectations.id')
            ->order('name ASC')
            ->queryAll();
        foreach($data as $d)
            $res[]=['name'=>$d['name'], 'id'=>$d['id']];
        return $res;
    }

    private function  buildInCondition($arr) {
        $result = '(-1';
        foreach($arr as $a) {
            $result .= ','.intval($a);
        }
        $result .= ')';
        return $result;
    }
}