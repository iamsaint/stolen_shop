<?php

class BasketController extends Controller {
    public function actionIndex($step=1) {
        $this->jsParams['items'] = Orders::getBasketItems();
        $this->jsParams['delivery'] = DeliveryCompanies::selectList();
        $this->breadcrumbs[] = 'Корзина';
        $this->render('index', ['step'=>$step]);
    }

    public function actionSaveorder() {
        $post = $this->getNgPost();
        if(Orders::saveOrder($post['data']))
            Yii::app()->request->cookies['basket_id'] = new CHttpCookie('basket_id', uniqid());
    }

    public function actionAddscu() {
        $post = $this->getNgPost();
        if(!$scu = Scu::model()->findByPk($post['scu']))
            $this->fail('Торговое предложение не существует');
        $cookie = Yii::app()->request->cookies['basket_id']->value;
        if($cookie === null) $this->fail('Невозможно добавить товар в корзину');
        $order = Orders::model()->findByAttributes(['cookie'=>$cookie]);
        if(!$order) {
            $order = new Orders;
            $order->cookie = $cookie;
            $order->status = 0;
            if(!$order->save())
                $this->fail('Ошибка при добавлении товара в корзину');
        }

        $oitem = Orderitems::model()->findByAttributes([
            'order'=>$order->id,
            'scu'=>$scu->id
        ]);

        if(!$oitem) {
            $oitem = new Orderitems;
            $oitem->order = $order->id;
            $oitem->scu = $scu->id;
            $oitem->count = 0 ;
        }
        $oitem->count += $post['count'];

        if($oitem->save())
            $this->success('Товар добавлен в корзину');
    }

    public function actionAdd() {
        $post = $this->getNgPost();
        if(!$icomp = Icomplectations::model()->findByPk($post['id']))
            $this->fail('Комплектация не существует');

        $cookie = Yii::app()->request->cookies['basket_id']->value;
        if($cookie === null) $this->fail('Невозможно добавить товар в корзину');

        $order = Orders::model()->findByAttributes(['cookie'=>$cookie]);
        if(!$order) {
            $order = new Orders;
            $order->cookie = $cookie;
            $order->status = 0;
            if(!$order->save())
                $this->fail('Ошибка при добавлении товара в корзину');
        }

        $oitem = Orderitems::model()->findByAttributes([
            'order'=>$order->id,
            'icomplectation'=>$icomp->id
        ]);

        if(!$oitem) {
            $oitem = new Orderitems;
            $oitem->order = $order->id;
            $oitem->icomplectation = $icomp->id;
            $oitem->count = 0 ;
        }

        $oitem->count += $post['count'];

        if($oitem->save())
            $this->success('Товар добавлен в корзину');
    }

    public function actionRem() {
        $post = $this->getNgPost();
        echo json_encode([
            'status'=>Orders::removeAllItems($post['id'])
        ]);
    }
    public function actionRemscu() {
        $post = $this->getNgPost();
        echo json_encode([
            'status'=>Orders::removeAllItemsscu($post['scu'])
        ]);
    }

    public function actionDel() {
        $post = $this->getNgPost();
        echo json_encode([
            'status'=>Orders::removeItem($post['id'], $post['count'])
        ]);
    }

    public function actionDelscu() {
        $post = $this->getNgPost();
        echo json_encode([
            'status'=>Orders::removeItemscu($post['scu'], $post['count'])
        ]);
    }

    private function fail($msg){
        echo json_encode([
            'status'=>1,
            'message'=>$msg
        ]);
        Yii::app()->end();
    }
    private function success($msg){
        echo json_encode([
            'status'=>0,
            'message'=>$msg
        ]);
        Yii::app()->end();
    }

}