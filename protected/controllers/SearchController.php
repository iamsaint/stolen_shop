<?php
/**
 * Created by PhpStorm.
 * User: saint
 * Date: 16.09.14
 * Time: 10:27
 */

class SearchController extends Controller {
    public function actionIndex($q=null, $page = 1) {
        $limit = 20;
        $client = Yii::app()->elastica->getClient();
        $index = $client->getIndex('profi_shop');
        $type = $index->getType('items');

        $elasticaQueryString  = new \Elastica\Query\QueryString();
        $elasticaQueryString->setDefaultOperator('AND');
        $elasticaQueryString->setQuery($q);

        $elasticaQuery        = new \Elastica\Query();
        $elasticaQuery->setFrom(($page-1)*$limit);    // Where to start?
        $elasticaQuery->setLimit($limit);
        $elasticaQuery->setQuery($elasticaQueryString);

        $elasticaResultSet    = $index->search($elasticaQuery);

        $result = [];
        foreach($elasticaResultSet->getResults() as $set)
            $result[]=$set->getData();

        if(Yii::app()->request->isAjaxRequest)
            $this->renderJSON($result);
        else {
            $this->jsParams['items'] = $result;
            $this->render('index');
        }
    }
}