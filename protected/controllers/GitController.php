<?php


class GitController extends Controller {
    public function actionDeploy() {
        try {
            shell_exec('cd ' . $_SERVER['DOCUMENT_ROOT']);
            shell_exec('git reset --hard HEAD');
            shell_exec('git fetch');
            shell_exec('git pull ');
            shell_exec('chmod -R og-rx .git');
        } catch (Exception $e) {
            Yii::log($e, 'ERROR');
        }


    }
}