<?php

class SiteController extends Controller {
    public function actionIndex() {
        $dependency = new CDbCacheDependency('SELECT MAX(update_time) FROM categories');
        $model = Categories::model()->cache(1000, $dependency)->with('bimage')->findAll();
        $this->render('index', ['model'=>$model, $dependency]);
    }

    public function actionEsindex() {
        $model = Items::model()->findAll();
        foreach($model as $m)
            $m->save();
    }

    public function actionCatalog($id, $manufacturer=0, $material=0, $size=0) {
        $dependency = new CDbCacheDependency('SELECT MAX(update_time) FROM categories');
        $model = Categories::model()->cache(1000, $dependency)->findByAttributes(array(
            'code'=>$id
        ));
        if(!$model)
            throw new CHttpException(404, Yii::t('catalog', 'not_found'));

        $this->render('catalog', [
            'id'=>$model->id,
            'manufacturer'=>$manufacturer,
            'material'=>$material,
            'size'=>$size,
            'dependency'=>$dependency,
            'imagetype'=>$model->imagetype,
        ]);
    }

    public function actionItem($id, $framed = false) {
        if($framed)
            $this->layout = 'card';
        if(!$data = Items::getItemCard($id))
            throw new CHttpException(404, Yii::t('item', 'not_found'));
        $this->jsParams = $data;
        if($data['category']==1)
            $this->render('item', ['framed'=>$framed]);
        else
            $this->render('item2', ['framed'=>$framed]);
    }

    public function actionError()
    {
        if($error=Yii::app()->errorHandler->error)
        {
            if(Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    public function actionGeticomp($item, $comp) {
        $model = Icomplectations::model()->findByAttributes([
           'item'=>$item,
            'complectation'=>$comp
        ]);

        echo json_encode(['id'=>$model->id]);
    }

    public function actionFix(){
        $model = Items::model()->findAll();
        foreach($model as $m){
            $m->code = $m->getUniqCode($m->name);
            $m->save();
        }
    }
    public function actionRules(){
        $this->render('rules');
    }
    public function actionDiscount(){
        $this->render('discount');
    }
    public function actionContacts(){
        $this->render('contacts');
    }
    public function actionShipping(){
        $this->render('shipping');
    }
}