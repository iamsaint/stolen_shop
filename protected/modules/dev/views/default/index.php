<h1>Powered by</h1>
<center>
    <a target="_blank" href="http://www.yiiframework.com/" title="Yii framework">
        <img src="<?php echo $this->module->assetsUrl?>/images/yii.png">
    </a>
    <a target="_blank" href="http://www.mysql.com/" title="MySQL">
        <img src="<?php echo $this->module->assetsUrl?>/images/mysql.png">
    </a>
    <a target="_blank" href="http://memcached.org/" title="Memcached">
        <img src="<?php echo $this->module->assetsUrl?>/images/memcached.png">
    </a>
    <br>
    <br>
    <a target="_blank" href="http://jquery.com/" title="jQuery">
        <img src="<?php echo $this->module->assetsUrl?>/images/jquery.png">
    </a>
    <a target="_blank" href="http://angularjs.org/" title="AngularJS">
        <img src="<?php echo $this->module->assetsUrl?>/images/angularjs.png">
    </a>
    <a target="_blank" href="http://www.w3schools.com/css/css3_intro.asp" title="CSS3">
        <img src="<?php echo $this->module->assetsUrl?>/images/css3.png">
    </a>
</center>
<br>
<br>


<center>
    <h4>Team</h4>
    <a href="http://involta.pro/" target="_blank" >
        <br>
        <img src="<?php echo $this->module->assetsUrl?>/images/involta.png">
    </a>
    <br><br>

    <h4>Backend & Frontend</h4>
    <a href="http://playmc.ru/" target="_blank">Alexey Chernousov</a>
    <br><br>
    <h4>Design</h4>
    <a href="http://kulistov.com/" target="_blank">Sergey Kulistov</a>
    <br><br>
    <h4>HTML+CSS Slicer</h4>
    <a href="http://vk.com/max_buyan" target="_blank">Maxim Buyanov</a>
</center>
