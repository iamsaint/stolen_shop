<!--<?php
$this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Login");
$this->breadcrumbs=array(
	UserModule::t("Login"),
);
?>-->

<h1><?php echo UserModule::t("Login"); ?></h1>

<?php if(Yii::app()->user->hasFlash('loginMessage')): ?>

<div class="success">
	<?php echo Yii::app()->user->getFlash('loginMessage'); ?>
</div>

<?php endif; ?>
<div class="auth-form form">
<?php echo CHtml::beginForm(); ?>
	<?php echo CHtml::errorSummary($model); ?>
	
	<div class="row">
		<?php echo CHtml::activeTextField($model,'username',array('class'=>'w100 auth-input login','placeholder'=>'Логин')) ?><span class="login-span"></span>
	</div>
	
	<div class="row">
		<?php echo CHtml::activePasswordField($model,'password',array('class'=>'w100 auth-input password','placeholder'=>'Пароль')) ?><span class="password-span"></span>
	</div>
	
	<div class="row rememberMe">
		<?php echo CHtml::activeCheckBox($model,'rememberMe',array('class'=>'checkbox')); ?>
		<?php echo CHtml::activeLabelEx($model,'rememberMe',array('class'=>'checkbox-label')); ?>
	</div>

	<div class="row submit">
		<?php echo CHtml::submitButton(UserModule::t("Login"),array('class'=>'auth-form-submit w100')); ?>
	</div>
    <div class="row">
        <p class="hint">
            <?php echo CHtml::link(UserModule::t("Register"),Yii::app()->getModule('user')->registrationUrl,array('class'=>'auth-form-link fll')); ?>  <?php echo CHtml::link(UserModule::t("Lost Password?"),Yii::app()->getModule('user')->recoveryUrl,array('class'=>'auth-form-link flr')); ?>
        </p>
    </div>
	
<?php echo CHtml::endForm(); ?>
</div><!-- form -->


<?php
$form = new CForm(array(
    'elements'=>array(
        'username'=>array(
            'type'=>'text',
            'maxlength'=>32,
        ),
        'password'=>array(
            'type'=>'password',
            'maxlength'=>32,
        ),
        'rememberMe'=>array(
            'type'=>'checkbox',
        )
    ),

    'buttons'=>array(
        'login'=>array(
            'type'=>'submit',
            'label'=>'Login',
        ),
    ),
), $model);
?>