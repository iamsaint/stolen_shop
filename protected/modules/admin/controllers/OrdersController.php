<?php

class OrdersController extends AController
{
    public $modelClass = 'Orders';

    public function actionIndex($params = []) {
        $criteria = new CDbCriteria;
        $criteria->condition = 'status > :status';
        $criteria->params = [':status'=>0];
        $criteria->order = 'order_num DESC';
        $dataProvider=new CActiveDataProvider($this->modelClass,[
            'criteria'=>$criteria
        ]);
        $params = array_merge($params, ['dataProvider'=>$dataProvider]);
        $this->render('index', $params);
    }


    public function actionView($id) {
        $model = $this->loadModel($id);
        if($_POST[$this->modelClass]['status']) {
            $class = new $this->modelClass();
            if($class::setStatus($_POST[$this->modelClass]['status'], $model->id))
                $this->redirect(['index']);
            else
                $model->addError('status', 'Невозможно изменить статус заказа');
        }
        $this->render('application.views.mail.orderEmail', ['model'=>$model]);
    }

    public function actionUpdate($id, $params = []) {
        throw new CHttpException(404, 'Страница не найдена');
    }

    public function actionCreate($params = []) {
        throw new CHttpException(404, 'Страница не найдена');
    }

    public function actionDelete($params = []) {
        throw new CHttpException(404, 'Страница не найдена');
    }

}
