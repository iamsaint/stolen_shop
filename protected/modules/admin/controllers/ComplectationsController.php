<?php

class ComplectationsController extends AController
{
    public $modelClass = 'Complectations';


    public function actionClear($category){
        unset(Yii::app()->request->cookies['category_'.$category.'_filter']);
        $this->redirect($this->createUrl('index'));
    }

    public function actionIndex($params = []) {
        $category = 1;
        $model = Categories::model()->findByPk($category);
        if($model===null)
            throw new CHttpException(404, 'Категория не найдена');

        $class = new $this->modelClass();

        $cookie = json_decode((Yii::app()->request->cookies['category_'.$category.'_filter']->value?:'{}'), true);

        if($arrt = Yii::app()->request->getParam('Complectations', false)) {
            $cookie = $arrt;
            Yii::app()->request->cookies['category_'.$category.'_filter'] = new CHttpCookie('category_'.$category.'_filter', json_encode($cookie));
        }
        $model = new $class();
        $model->attributes = $cookie;
        $model->category = $category;

        $this->render('index', [
            'category'=>$category,
            'model'=>$model
        ]);
    }

    public function actionCreate($params = []){
        if(!$category = Yii::app()->request->getParam('category', false))
           throw new CHttpException(400, 'Неправильный запрос');
        $class = new $this->modelClass();
        $model = new $class;
        $model->category = $category;
        $this->CU($model, $params);
    }

    public function CU($model, $params = []) {
        if(Yii::app()->request->getPost($this->modelClass, false)) {
            $model->attributes=Yii::app()->request->getPost($this->modelClass);
            if($model->save())
                $this->redirect(['index']);
        }
        $criteria = new CDbCriteria;
        $criteria->with=['mmcategory'];
        $criteria->condition = 'mmcategory.id = :cat';
        $criteria->params = [':cat'=>$model->category];
        $criteria->order ='t.name ASC';
        $this->render('_form', ['model'=>$model, 'criteria'=>$criteria]);
    }

}
