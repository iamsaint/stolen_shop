<?php

class CatalogsController extends AController
{
    public $modelClass = 'Catalogs';

    public function actionIndex($params = []) {
        $this->modelClass = 'Categories';
        $dataProvider=new CActiveDataProvider($this->modelClass, [
            'criteria'=>[
                'condition'=>'id > 1'
            ],
            'pagination'=>[
                'pageSize'=>65535,
            ],
        ]);
        $params = array_merge($params, ['dataProvider'=>$dataProvider]);
        $this->render('index', $params);

    }

    public function actionEdit($category) {
        $dataProvider=new CActiveDataProvider($this->modelClass, [
            'criteria'=>[
                'condition'=>'category=:cat',
                'params'=>[':cat'=>$category]
            ],
            'pagination'=>[
                'pageSize'=>65535,
            ],
        ]);
        $this->render('edit', [
            'dataProvider'=>$dataProvider,
            'category'=>$category
        ]);
    }

    public function actionCreate($params = [])
    {
        $class = new $this->modelClass();
        $model = new $class();
        if(!$model->category = Yii::app()->request->getParam('category', false))
            throw new CHttpException(400,Yii::t('yii','Your request is invalid.'));
        $this->CU($model);
    }
    public function CU($model, $params = []) {
        if(Yii::app()->request->getPost($this->modelClass, false)) {
            $model->attributes = Yii::app()->request->getPost($this->modelClass);
            if($model->save()) {
                // Обновление элементов справочника
                if($items = Yii::app()->request->getPost('Catalogitems', false)){
                    foreach($items as $k=>$v){
                        $iModel = Catalogitems::model()->findByPk($k);
                        if($iModel){
                            $iModel->name = $v['name'];
                            $iModel->save();
                        }
                    }
                }
                // Добавление элементов справочника
                if($items = Yii::app()->request->getPost('Catalogitems_new', false)){
                    foreach($items as $k=>$v){
                        $iModel = new Catalogitems;
                        if($iModel){
                            $iModel->catalog=$model->id;
                            $iModel->name = $v['name'];
                            $iModel->save();
                        }
                    }
                }

                // Удаление элементов справочника
                if($items = Yii::app()->request->getPost('Catalogitems_delete', false)){
                    foreach($items as $item){
                        Catalogitems::model()->deleteByPk($item);
                    }
                }
                $this->redirect(['edit', 'category'=>$model->category]);
            }
        }
        $this->render('_form', ['model'=>$model]);
    }

    public function actionDelete($id)
    {
        $model = $this->loadModel($id);
        try {
            $model->delete();
        } catch (Exception $e){
            throw new CHttpException(500, 'Нельзя удалить используемый объект');
        }
        if(!isset($_GET['ajax']))
            $this->redirect(Yii::app()->request->getPost('returnUrl', false) ? Yii::app()->request->getPost('returnUrl') : ['admin']);
    }
}
