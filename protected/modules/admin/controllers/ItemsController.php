<?php

class ItemsController extends AController {
    public $modelClass = 'Items';

    public function actionIndex($params = []) {
        $category = 1;
        $criteria = new CDbCriteria;
        $criteria->select = 'manufacturer, material, category, collection';
        $criteria->condition = 'category = :c';
        $criteria->params = [':c'=>intval($category)];
        $criteria->distinct=true;

        $dataProvider=new CActiveDataProvider('Complectations',[
            'criteria'=>$criteria,
            'pagination'=>false,
        ]);
        $params = [
            'category'=>$category,
        ];
        $params = array_merge($params, ['dataProvider'=>$dataProvider]);
        $this->render('comps', $params);
    }


    public function actionEdit($manufacturer, $material, $category, $collection) {
        $criteria = new CDbCriteria;
        $criteria->condition = 'manufacturer = :mn and material = :mt and collection = :cl';
        $criteria->params = [':mn'=>$manufacturer, ':mt'=>$material, ':cl'=>$collection];
        if(Complectations::model()->count($criteria)==0)
            throw new CHttpException(404, 'Комплектация не существует');

        $criteria = new CDbCriteria;
        $criteria->condition = 'comps.manufacturer = :mn and comps.material = :mt and comps.collection = :cl';
        $criteria->params = [':mn'=>$manufacturer, ':mt'=>$material, ':cl'=>$collection];
        if($q = Yii::app()->request->getParam('q', false)) {
            $match = addcslashes($q, '%_');
            $criteria->addCondition("t.name like :name");
            $criteria->params[':name']="%$match%";
        }

        $item_model = new Items;

        $sort = new CSort();
        $sort->defaultOrder='creation_time DESC';
        $sort->attributes = array(
            'name'=>array('asc'=>'name', 'desc'=>'name desc'),
            'update_time'=>array('asc'=>'update_time', 'desc'=>'update_time desc'),
            'creation_time'=>array('asc'=>'creation_time', 'desc'=>'creation_time desc'),
        );
        $sort->applyOrder($criteria);
        $items = Items::model()->with('comps')->findAll($criteria);

        $dataProvider=new CActiveDataProvider('Items', [
            'sort'=>$sort
        ]);
        $dataProvider->setData($items);

        $this->render('edit', ['dataProvider'=>$dataProvider, 'manufacturer'=>$manufacturer, 'material'=>$material, 'category'=>$category, 'collection'=>$collection, 'item_model'=>$item_model]);
    }

    public function CU($model, $params = []) {
        if(
            intval(Yii::app()->request->getParam('category'))===0 ||
            intval(Yii::app()->request->getParam('material'))===0 ||
            intval(Yii::app()->request->getParam('manufacturer'))===0||
            intval(Yii::app()->request->getParam('collection'))===0
        ) throw new CHttpException(404, 'Страница не найдена');

        $tsizes = Sizes::model()->with('mmcategory')->findAll();

        $sizes = [];
        foreach($tsizes as $s) {
            if($s->mmcategory[0]->id!=Yii::app()->request->getParam('category')) continue;

            $comp = Complectations::model()->findByAttributes([
                'size'=>$s->id,
                'manufacturer'=>Yii::app()->request->getParam('manufacturer'),
                'category'=>Yii::app()->request->getParam('category'),
                'material'=>Yii::app()->request->getParam('material'),
                'collection'=>Yii::app()->request->getParam('collection'),
            ]);

            if($comp === null) continue;

            $sizes[$s->id]=[
                'name'=>$s->name,
                'ch'=>0
            ];
        }

        foreach($model->icomps as $comp) {
            if($sizes[$comp->bcomplectation->size]) {
                $sizes[$comp->bcomplectation->size]['ch']=$comp->active==1?1:0;
                $sizes[$comp->bcomplectation->size]['articul']=$comp->articul? : $comp->bcomplectation->articul;
                $sizes[$comp->bcomplectation->size]['code']=$comp->code;
                $sizes[$comp->bcomplectation->size]['discount']=$comp->discount;
            }
        }

        $params['sizes'] = $sizes;

        $params = array_merge($params, ['model'=>$model]);

        if(Yii::app()->request->getPost($this->modelClass, false))
        {
            if($model->isNewRecord) {
                $model->category = Yii::app()->request->getParam('category');
            }

            $postSizes = Yii::app()->request->getPost('sizes', []);
            $model->attributes=Yii::app()->request->getPost($this->modelClass);
            $model->code = $model->getUniqCode($model->name);

            if($model->save()) {

                foreach ($postSizes as $key=>$val) {
                    $comp = Complectations::model()->findByAttributes([
                        'size'=>$key,
                        'manufacturer'=>Yii::app()->request->getParam('manufacturer'),
                        'material'=>Yii::app()->request->getParam('material'),
                        'collection'=>Yii::app()->request->getParam('collection'),
                    ]);
                    if($comp!==null) {
                        $icomp = Icomplectations::model()->findByAttributes([
                            'complectation'=>$comp->id,
                            'item'=>$model->id,
                        ]);
                        if($icomp===null){
                            $icomp = new Icomplectations;
                            $icomp->complectation = $comp->id;
                            $icomp->item = $model->id;
                        }

                        $icomp->active = $val['ch'] == 1? 1: 0;
                        $icomp->articul = $val['articul'];
                        $icomp->code = $val['code'];
                        $icomp->discount = $val['discount'];

                        if(!$icomp->save())
                            $model->errors = $icomp->errors;
                    }
                }

                if(count($deleteArr = array_keys(json_decode(Yii::app()->request->getPost('imagesForDelete', []), true)))>0)
                    Itemimages::removeLinks($model->id, $deleteArr);

                if(is_array($_FILES['file']))
                    Itemimages::addNewLinks($model->id, $_FILES['file']);

                $this->redirect(['edit',
                    'manufacturer'=>Yii::app()->request->getParam('manufacturer'),
                    'material'=>Yii::app()->request->getParam('material'),
                    'category'=>Yii::app()->request->getParam('category'),
                    'collection'=>Yii::app()->request->getParam('collection'),
                ]);
            }
        }

        $this->render('_form', $params);
    }


}