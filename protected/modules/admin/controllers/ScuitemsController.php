<?php
/**
 * Created by PhpStorm.
 * User: saint
 * Date: 25.08.14
 * Time: 13:02
 */

class ScuitemsController extends AController {
    public $modelClass = 'Items';
    public function actionIndex($params = []) {
        $this->modelClass = 'Categories';
        $dataProvider=new CActiveDataProvider($this->modelClass, [
            'criteria'=>[
                'condition'=>'id > 1'
            ],
            'pagination'=>[
                'pageSize'=>65535,
            ],
        ]);
        $params = array_merge($params, ['dataProvider'=>$dataProvider]);
        $this->render('index', $params);
    }

    public function actionEdit($category) {

        $criteria = new CDbCriteria;
        $criteria->condition = 't.category = :cat';
        $criteria->params = [':cat'=>$category];
        if($q = Yii::app()->request->getParam('q', false)) {
            $match = addcslashes($q, '%_');
            $criteria->addCondition("t.name like :name");
            $criteria->params[':name']="%$match%";
        }

        $item_model = new Items;

        $sort = new CSort();
        $sort->defaultOrder='creation_time DESC';
        $sort->attributes = array(
            'name'=>array('asc'=>'name', 'desc'=>'name desc'),
            'update_time'=>array('asc'=>'update_time', 'desc'=>'update_time desc'),
            'creation_time'=>array('asc'=>'creation_time', 'desc'=>'creation_time desc'),
        );
        $sort->applyOrder($criteria);
        $items = Items::model()->with('comps')->findAll($criteria);

        $dataProvider=new CActiveDataProvider('Items', [
            'sort'=>$sort
        ]);
        $dataProvider->setData($items);

        $this->render('edit', ['dataProvider'=>$dataProvider,  'category'=>$category, 'item_model'=>$item_model]);
    }

    public function actionCreate($params =[]) {
        $class = new $this->modelClass();
        $model = new $class();
        if(!$model->category = Yii::app()->request->getParam('category', false))
            throw new CHttpException(400,Yii::t('yii','Your request is invalid.'));
        $this->CU($model);
    }

    private function checkOpt($catalogs, $optList) {
        $error = false;
        foreach($catalogs as $c) {
            if($c->filter==0) continue;
                if(!$optList[$c->id])
                    $error = true;
        }
        return $error;
    }

    public function CU($model, $params = []) {
        $model->scenario = 'skuitems';
        $catalogsItems = Catalogs::model()->findAllByAttributes(['category'=>$model->category, 'type'=>0]);
        $catalogs = Catalogs::model()->findAllByAttributes(['category'=>$model->category, 'type'=>1]);

        if(Yii::app()->request->getPost($this->modelClass, false)){
            $model->attributes = Yii::app()->request->getPost($this->modelClass);
            $model->code = $model->getUniqCode($model->name);
            $scuImagesForDelete = json_decode(Yii::app()->request->getPost('scuImagesForDelete', '{}'), true);

            $optList = Yii::app()->request->getPost('PRODUCT', [])['product_options'];
            if($this->checkOpt($catalogsItems, $optList))
                    $model->addError('id', 'Заполните все обязательные поля');

            if(count($model->errors)==0 && $model->save()) {

                // удаление торгового предложения
                $scudel = json_decode(Yii::app()->request->getPost('SCU_DEL','{}'), true);
                foreach($scudel as $did)
                    Scu::model()->deleteByPk($did);

                if(is_array($optList))
                foreach($optList as $optId=>$optVal) {
                    if(intval($optVal)==0) continue;
                    $itemOpt = ItemOptions::model()->findByAttributes([
                        'itemid'=>$model->id,
                        'catalogitem'=>$optId
                    ]);
                    if(!$itemOpt) {
                        $itemOpt = new ItemOptions;
                        $itemOpt->itemid = $model->id;
                        $itemOpt->catalogitem = $optId;
                    }
                    $itemOpt->value = intval($optVal);
                    if(!$itemOpt->save())
                        $model->addError('id', 'Ошибка при сохранении опций товара');

                }

                foreach(Yii::app()->request->getPost('SCU', []) as $scuId=>$scu) {
                    if(in_array($scuId, $scudel)) continue;

                    // создание торгового предложения

                    $item = Scu::model()->findByPk($scuId);
                    if(!$item) {
                        $item = new Scu;
                        $item->itemid = $model->id;
                    }

                    $item->price = $scu['price'];
                    $item->discount = $scu['discount'];
                    $item->articul = $scu['articul'];
                    $item->code = $scu['code'];
                    $item->descr = $scu['descr'];
                    $item->price = $scu['price'];
                    $item->active = is_null($scu['active'])? 0 : 1;

                    if(!$item->save())
                        $model->addError('id', 'Ошибка при сохранении торговоговых предложений');
                    else {
                        // Создание элементов торгового предложения
                        foreach($scu['options'] as $scuOptID=>$scuOptVal) {
                            $scuOpt = Scuitems::model()->findByAttributes([
                                'scu'=>$item->id,
                                'catalogitem'=>$scuOptID
                            ]);

                            if(!$scuOpt) {
                                $scuOpt = new Scuitems;
                                $scuOpt->catalogitem = $scuOptID;
                                $scuOpt->scu = $item->id;
                            }

                            $scuOpt->value=$scuOptVal;
                            if(!$scuOpt->save()) {
                                $model->addError('id', 'Ошибка при сохранении торговоговых предложений');
                            }
                        }

                        if(is_array($scuImagesForDelete[$item->id])){
                            foreach($scuImagesForDelete[$item->id] as $siid)
                                Scuimages::model()->deleteAllByAttributes(['image'=>$siid, 'scu'=>$item->id]);
                        }

                        if(is_array($_FILES['scuimagefile_'.$scuId]))
                            Scuimages::addNewLinks($item->id, $_FILES['scuimagefile_'.$scuId]);

                    }
                }

                if(count($deleteArr = array_keys(json_decode(Yii::app()->request->getPost('imagesForDelete', []), true)))>0)
                    Itemimages::removeLinks($model->id, $deleteArr);

                if(is_array($_FILES['file']))
                    Itemimages::addNewLinks($model->id, $_FILES['file']);

                if(count($model->errors)==0)
                    $this->redirect($this->createURL('edit', ['category'=>$model->category]));

            }
        }

        $SCU = Scu::model()->with('scuitems','images')->findAllByAttributes(['itemid'=>$model->id]);
        $OPTIONS = ItemOptions::model()->findAllByAttributes(['itemid'=>$model->id]);

        $this->render('_form', [
            'model'=>$model,
            'catalogsItems'=>$catalogsItems,
            'catalogs'=>$catalogs,
            'SCU'=>$SCU,
            'OPTIONS'=>$OPTIONS,
        ]);
    }
}