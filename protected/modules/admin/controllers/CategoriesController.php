<?php

class CategoriesController extends AController
{
    public $modelClass = 'Categories';

    public function CU($model, $params = []) {
        if(Yii::app()->request->getPost($this->modelClass, false)) {
            $model->code = $model->getUniqCode($model->name);

            if($_FILES['fileimage']['error'] == 0)
                $model->image = Images::uploadImages($_FILES['fileimage'])[0];
        }

        parent::CU($model, $params);
    }

    public function actionDelete($id)
    {
        $model = $this->loadModel($id);
        try {
            $model->delete();
        } catch (Exception $e){
            throw new CHttpException(500, 'Нельзя удалить используемый объект');
        }
        if(!isset($_GET['ajax']))
            $this->redirect(Yii::app()->request->getPost('returnUrl', false) ? Yii::app()->request->getPost('returnUrl') : ['admin']);
    }
}
