window.onload = function(){
    styleSelectBoxes();
}
function styleSelectBoxes(){
    var boxes = document.getElementsByTagName('select');
    if (boxes.length>0){
        var count = boxes.length;
        for (var i = 0;i<count;i++){
            if ((boxes[i].getAttribute('class')!='styled')&&(boxes[i].getAttribute('class')!='no-style')){
            var options = boxes[i].getElementsByTagName('option'),
                styledOptions = [],
                optionsCount = boxes[i].getElementsByTagName('option').length,
                elOption = document.createElement('li'),
                styledSelect = document.createElement('ul');
                styledSelect.setAttribute('class','styled-select');
                for (var j = 0; j<optionsCount; j++){
                    styledOptions[j] = elOption.cloneNode(false);
                    styledOptions[j].setAttribute('value',options[j].getAttribute('value'));
                    styledOptions[j].setAttribute('onclick','check('+i+','+j+');');
                    if(options[j].innerHTML.length>0){
                    styledOptions[j].innerHTML = options[j].innerHTML;}else{
                        styledOptions[j].innerHTML = 'Не выбрано'
                    }
                    if (options[j].hasAttribute('selected')){
                        styledOptions[j].setAttribute('class','selected');
                        styledOptions[j].setAttribute('onclick','selectOpen('+i+')');
                    }
                    styledSelect.appendChild(styledOptions[j]);
                }
                if (styledSelect.getElementsByClassName('selected').length==0){
                    styledSelect.getElementsByTagName('li')[0].setAttribute('class','selected');
                    styledSelect.getElementsByTagName('li')[0].setAttribute('onclick','selectOpen('+i+')');
                    boxes[i].getElementsByTagName('option')[0].setAttribute('selected','selected');
                }
            boxes[i].parentNode.appendChild(styledSelect);
            boxes[i].setAttribute('class','styled');
        }}
    }
}
function check(i,j){
   var selectBox = document.getElementsByClassName('styled-select')[i];
    selectBox.getElementsByClassName('selected')[0].setAttribute('onclick','check('+i+','+Array.prototype.indexOf.call(selectBox.getElementsByTagName('li'),selectBox.getElementsByClassName('selected')[0])+')');
    selectBox.getElementsByClassName('selected')[0].setAttribute('class','');
    selectBox.getElementsByTagName('li')[j].setAttribute('class','selected');
    selectBox.getElementsByTagName('li')[j].setAttribute('onclick','selectOpen('+i+');');
    getElementsByAttribute(document.getElementsByTagName('select')[i].getElementsByTagName('option'),'selected')[0].removeAttribute('selected');
    document.getElementsByTagName('select')[i].options[j].setAttribute('selected','selected');
    document.getElementsByTagName('select')[i].selectedIndex = j;
    selectBox.setAttribute('class','styled-select');
}
function selectOpen(i){
    if(document.getElementsByClassName('styled-select opened').length>0){document.getElementsByClassName('styled-select opened')[0].setAttribute('class','styled-select');}
    document.getElementsByClassName('styled-select')[i].setAttribute('class','styled-select opened');
    document.getElementsByClassName('styled-select')[i].getElementsByClassName('selected')[0].setAttribute('onclick','check('+i+','+Array.prototype.indexOf.call(document.getElementsByClassName('styled-select')[i].getElementsByTagName('li'),document.getElementsByClassName('styled-select')[i].getElementsByClassName('selected')[0])+')');
}





function getElementsByAttribute(where,what){
var count = where.length,
    retArr = [];
    for (i=0;i<count;i++){
        if(where[i].hasAttribute(what)){
             retArr.push(where[i])
        }
    }
return retArr;
}

