'use strict';

app.controller('ItemsCtrl', ['$scope', function(scope) {
    scope.delImg = {};
    scope.newImages = [];

    scope.deleteImage = function(id) {
        scope.delImg[id] = true;
    }
    scope.getDeleteJSON = function() {
        return JSON.stringify(scope.delImg);
    }
    scope.deleteNewImage = function(id) {
        scope.newImages.splice(id, 1);
    }

    scope.changefile = function(e) {
        var index =e.srcElement.getAttribute('id').replace('file', ''),
            file =  e.srcElement.files[0];
        if (file.type.match(/image.*/)) {
            var reader = new FileReader();
            reader.onload = function (event){
                scope.newImages[index].file = event.target.result;
                scope.$apply();
            }
            reader.readAsDataURL(file);
        }
    }

    scope.openNewFile = function() {
        scope.newImages.push({file:''});
        setTimeout(function(){
            document.querySelector('#file'+(scope.newImages.length-1)).onchange = scope.changefile;
            document.querySelector('#file'+(scope.newImages.length-1)).click();
        }, 10);
    }

}]);
