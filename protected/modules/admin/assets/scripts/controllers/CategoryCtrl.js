'use strict';

app.controller('CategoryCtrl', ['$scope', function(scope) {
    scope.getImage = function(){
        return scope.fileimage;
    }
    scope.changefile = function(e) {
        var file =  e.srcElement.files[0];
        if (file.type.match(/image.*/)) {
            var reader = new FileReader();
            reader.onload = function (event){
                scope.fileimage = event.target.result;
                scope.$apply();
            }
            reader.readAsDataURL(file);
        } else alert('Файл не является картинкой');
    }

    scope.openNewFile = function() {
        document.querySelector('#fileimage').click();
    }

    document.querySelector('#fileimage').onchange = scope.changefile;
}]);
