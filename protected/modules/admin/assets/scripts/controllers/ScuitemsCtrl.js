'use strict';

app.controller('ScuitemsCtrl', ['$scope', function(scope) {
    scope.delImg = {};
    scope.newImages = [];
    scope.SCU = SCU;
    scope.PRODUCT = PRODUCT;
    scope.SCU.toDelete = [];

    scope.delImg = {};
    scope.newImages = [];


    scope.deleteImage = function(id) {
        scope.delImg[id] = true;
    }
    scope.getDeleteJSON = function() {
        return JSON.stringify(scope.delImg);
    }
    scope.getScuDeleteJSON = function() {
        var res = {};
        angular.forEach(scope.SCU, function(val, key){
            angular.forEach(val, function(v, k) {
                angular.forEach(v.images, function(iv, ik) {
                    if(iv.delete) {
                        if(res[v.id] == undefined)
                            res[v.id] = [];
                        res[v.id].push(iv.id);
                    }
                });
            });
        });
        return JSON.stringify(res);
    }
    scope.deleteNewImage = function(id) {
        scope.newImages.splice(id, 1);
    }

    scope.changefile = function(e) {
        var index =e.srcElement.getAttribute('id').replace('file', ''),
            file =  e.srcElement.files[0];
        if (file.type.match(/image.*/)) {
            var reader = new FileReader();
            reader.onload = function (event){
                scope.newImages[index].file = event.target.result;
                scope.$apply();
            }
            reader.readAsDataURL(file);
        }
    }

    scope.openNewFile = function() {
        scope.newImages.push({file:''});
        setTimeout(function(){
            document.getElementById('file'+(scope.newImages.length-1)).onchange = scope.changefile;
            document.getElementById('file'+(scope.newImages.length-1)).click();
        }, 10);
    }

    scope.addScuImage = function(id) {
        if(scope.SCU.items[id].images == undefined)
            scope.SCU.items[id].images = [];
        scope.SCU.items[id].images.push({file:''});
        setTimeout(function(){
            document.querySelector('#scuimagefile_'+id+'_'+(scope.SCU.items[id].images.length-1)).onchange = function(e) {
                var file =  e.srcElement.files[0];
                if (file.type.match(/image.*/)) {
                    var reader = new FileReader();
                    reader.onload = function (event){
                        scope.SCU.items[id].images[(scope.SCU.items[id].images.length-1)].file = event.target.result;
                        scope.$apply();
                    }
                    reader.readAsDataURL(file);
                }
            }
            document.querySelector('#scuimagefile_'+id+'_'+(scope.SCU.items[id].images.length-1)).click();
        }, 10);
    }

    scope.deleteScuImage = function(id, idn) {
        if(scope.SCU.items[id].images[idn].id == undefined)
            scope.SCU.items[id].images.splice(idn, 1);
        else {
            scope.SCU.items[id].images[idn].file = '';
            scope.SCU.items[id].images[idn].delete = true;
        }
    }

    scope.getSCU = function() {
        return scope.SCU.items;
    }
    scope.addSCU = function() {
        var obj = {options:{}, active: 1};
        angular.forEach(SCU.options, function(val, key){
            angular.forEach(val, function(v, k){
                obj.options[k]=0;
            });
        });

        scope.SCU.items.push(obj);
    }

    scope.deleteSCU = function(i){
        if(parseInt(scope.SCU.items[i].id)  > 0)
            scope.SCU.toDelete.push(scope.SCU.items[i].id);
        scope.SCU.items.splice(i, 1);
    }

}]);
