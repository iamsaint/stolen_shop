'use strict';

app.controller('CatalogCtrl', ['$scope', function(scope) {
    scope.catalogItems = [];
    scope.Catalogitems_new = [];
    scope.Catalogitems_delete = [];

    scope.addCategoryItem = function() {
        scope.Catalogitems_new.push({name:''});
    }

    scope.deleteNewCategoryItem = function(i) {
        scope.Catalogitems_new.splice(i, 1);
    }

    scope.deleteCategoryItem = function(i) {
        scope.catalogItems[i] = false;
        scope.Catalogitems_delete.push({id:i});
    }

    scope.getCatalogItemsVisible = function(i){
        return scope.catalogItems[i];
    }
}]);
