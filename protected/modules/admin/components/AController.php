<?php

class AController extends Controller {
    public $modelClass = '';
    public $layout='admin';


    protected function beforeAction($action){
        if(defined('YII_DEBUG') && YII_DEBUG){
            Yii::app()->assetManager->forceCopy = true;
        }
        return parent::beforeAction($action);
    }

    public function filters()
    {
        return [
            'accessControl',
            'postOnly + delete',
        ];
    }

    public function accessRules()
    {
        return [
            ['allow',
                'users'=>['admin'],
            ],
            ['deny',  // deny all users
                'users'=>['*'],
            ],
        ];
    }


    public function loadModel($id)
    {
        $class = new $this->modelClass();
        $model=$class::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }


    protected function performAjaxValidation($model)
    {
        if(Yii::app()->request->getPost('ajax', false) && Yii::app()->request->getPost('ajax', '')===strtolower($this->modelClass).'-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionCreate($params = [])
    {
        $class = new $this->modelClass();
        $this->CU(new $class, $params);
    }

    public function actionUpdate($id, $params = [])
    {
        $this->CU($this->loadModel($id), $params);
    }

    public function actionDelete($id)
    {
        $model = $this->loadModel($id);

        try {
            $model->delete();
        } catch (Exception $e){
            throw new CHttpException(500, $e->getMessage());
        }
        if(!isset($_GET['ajax']))
            $this->redirect(Yii::app()->request->getPost('returnUrl', false) ? Yii::app()->request->getPost('returnUrl') : ['admin']);
    }


    public function actionIndex( $params = [])
    {
        $dataProvider=new CActiveDataProvider($this->modelClass, [
            'pagination'=>[
                'pageSize'=>65535,
            ],
            'sort'=>array(
                'defaultOrder'=>'name ASC',
            )
        ]);
        $params = array_merge($params, ['dataProvider'=>$dataProvider]);
        $this->render('index', $params);
    }

    public function CU($model, $params = []) {
        if(Yii::app()->request->getPost($this->modelClass, false))
        {
            $model->attributes=Yii::app()->request->getPost($this->modelClass);
            if($model->save()) {
                if(method_exists($model, 'getCategoryLinkClassName')) {
                    if($classData = $model->getCategoryLinkClassName()) {
                        $class = new $classData['class']();
                        $list = Yii::app()->request->getPost('categoryConnect', []);
                        $insertList = [];
                        foreach(Categories::selectList() as $k=>$v) {
                            if($list[$k]) $insertList[]=$k;
                            if(!$model->isNewRecord)
                                $class::model()->deleteAllByAttributes([$classData['to']=>$k, $classData['from']=>$model->id]);
                        }

                        foreach($insertList as $category) {
                            $link = new $class;
                            $link->{$classData['from']} = $model->id;
                            $link->{$classData['to']} = $category;
                            $link->save();
                        }
                    }
                }
                $this->redirect(['index']);
            }
        }

        $params = array_merge($params, [
            'model'=>$model,
        ]);

        if(method_exists($model, 'getCatCon'))
            $params['catCon'] = $model->getCatCon();

        $this->render('_form', $params);
    }
}