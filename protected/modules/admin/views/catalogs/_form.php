
<h1>Редактирование справочника <?=$model->name?></h1>

<div class="form" ng-controller="CatalogCtrl">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'categories-form',
	'enableAjaxValidation'=>false,
    'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>

	<?php echo $form->errorSummary($model); ?>
<table>
	<tr class="row">
		<td><?php echo $form->labelEx($model,'name'); ?></td>
		<td><?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?><?php echo $form->error($model,'name'); ?></td>

	</tr>
	<tr class="row">
        <td><?php echo $form->labelEx($model,'type'); ?></td>
        <td><?php echo $form->dropDownList($model,'type',array('Свойство товара','Свойство торгового предложения')); ?><?php echo $form->error($model,'type'); ?></td>

	</tr>

	<tr class="row">
        <td><?php echo $form->labelEx($model,'filter'); ?></td>
        <td><?php echo $form->dropDownList($model,'filter',array('Нет','Да')); ?>
            <?php echo $form->error($model,'filter'); ?></td>
	</tr>
	<tr class="row">
        <td><?php echo $form->labelEx($model,'basket'); ?></td>
        <td><?php echo $form->dropDownList($model,'basket',array('Нет','Да')); ?>
            <?php echo $form->error($model,'basket'); ?></td>
	</tr>
	<tr class="row">
        <td><?php echo $form->labelEx($model,'search'); ?></td>
        <td><?php echo $form->dropDownList($model,'search',array('Нет','Да')); ?>
            <?php echo $form->error($model,'search'); ?></td>
	</tr>
</table>
    <h1>Элементы справочника</h1>
    <table>

        <?foreach($model->catalogitems as $ci):?>
            <tr ng-init="catalogItems[<?=$ci->id?>]=true" ng-show="getCatalogItemsVisible(<?=$ci->id?>)">
                <td>Название</td>
                <td><input type="text" name="Catalogitems[<?=$ci->id?>][name]" value="<?=$ci->name?>"></td>
                <td><a class="delete" href="javascript:void(0)" ng-click="deleteCategoryItem(<?=$ci->id?>)"></a></td>
            </tr>
        <?endforeach?>

        <tr ng-repeat="i in Catalogitems_new">
            <td>Название</td>
            <td><input type="text" name="Catalogitems_new[{{$index}}][name]" ng-model="Catalogitems_new[$index].name"></td>
            <td><a class="delete" href="javascript:void(0)" ng-click="deleteNewCategoryItem($index)"></a></td>
        </tr>

        <tr ng-repeat="i in Catalogitems_delete" style="display: none">
            <td colspan="3"><input name="Catalogitems_delete[{{$index}}]" value="{{i.id}}"></td>
        </tr>
    </table>

    <br>
    <a href="javascript:void(0)" ng-click="addCategoryItem()">Добавить элемент справочника</a>
    <br><br>

    <div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script src="<?php echo $this->module->assetsUrl?>/scripts/select.js"></script>
<script src="<?= $this->module->assetsUrl.'/scripts/controllers/CatalogCtrl.js' ?>"></script>