<h1>Категории справочников</h1>

<?
    $this->widget('zii.widgets.grid.CGridView', array(
        'dataProvider'=>$dataProvider,
        'cssFile'=>false,
        'selectableRows'=>1,
        'selectionChanged'=>'function(id){ window.location = "'.$this->createUrl('edit').'/category/"+$.fn.yiiGridView.getSelection(id) }',
        'columns'=>array(
            'name',
        ),
    ));
?>
