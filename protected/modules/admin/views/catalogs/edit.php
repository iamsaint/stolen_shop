<h1>Справочники</h1>
<a class="add-big-button" href="<?=$this->createUrl('create', ['category'=>$category])?>"><span class="plused-text">Новый справочник</span></a>
<?
    $this->widget('zii.widgets.grid.CGridView', array(
        'dataProvider'=>$dataProvider,
        'selectableRows'=>1,
        'cssFile'=>false,
        'selectionChanged'=>'function(id){ window.location = "'.$this->createUrl('update').'/id/"+$.fn.yiiGridView.getSelection(id) }',
        'columns'=>array(
            'name',
            array('class'=>'CButtonColumn',
                'template'=>'{delete}',
                'buttons'=>[
                    'delete'=>['imageUrl'=>$this->module->assetsUrl.'/images/delete.png']
                ]
            ),
        ),
    ));
?>
