
<h1>Редактирование товара <?=$model->name?></h1>

<div class="form" ng-controller="ItemsCtrl">
    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'materials-form',
        'enableAjaxValidation'=>false,
        'htmlOptions'=>array('enctype'=>'multipart/form-data'),
    )); ?>

    <input name="imagesForDelete" value="{{getDeleteJSON()}}" type="hidden">

    <?php echo $form->errorSummary($model); ?>

    <div class="image-list">
        <?foreach($model->images as $img):?>
            <div class="image-box" ng-hide="delImg[<?=$img->id?>]">
                <img src="<?=$img->file?>">
                <div class="delete" ng-click="deleteImage(<?=$img->id?>)"></div>
            </div>
        <?endforeach?>
        <div class="image-box" ng-repeat="n in newImages" ng-show="n.file!=''">
            <img ng-src="{{n.file}}">
            <div class="delete" ng-click="deleteNewImage($index)"></div>
            <input type="file" id="file{{$index}}" name="file[{{$index}}]">
        </div>
        <div class="image-box add" ng-click="openNewFile()"></div>
        <div class="name-block-after-img">
            <?php echo $form->labelEx($model,'name'); ?><br>
            <?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
            <?php echo $form->error($model,'name'); ?>
        </div>
        <div class="row">
            <?php echo $form->labelEx($model,'sticker'); ?>
            <?php echo $form->dropDownList($model,'sticker', Stickers::selectList([], true, 'Не выбран')); ?>
            <?php echo $form->error($model,'sticker'); ?>
        </div>
    </div>
    <table class="table-white">
        <thead>
            <tr>
                <th>Размер</th>
                <th>Артикул</th>
                <th>Код</th>
                <th>Скидка</th>
            </tr>
        </thead>
    <?foreach($sizes as $k=>$v):?>
        <tr>
            <td>
                <input class="checkbox" type="checkbox" id="size<?=$k?>"
                       ng-true-value="1"
                       ng-false-value="0"
                       ng-model="size<?=$k?>"
                       ng-init="size<?=$k?>=<?=$v['ch']==1?1:0?>"
                       ng-checked="size<?=$k?>==1"
                    >
                <label class="checkbox-label" for="size<?=$k?>"><?=$v['name']?></label>
                <input name="sizes[<?=$k?>][ch]" value="{{size<?=$k?>}}" type="hidden">

            </td>
            <td>
                <input type="text" name="sizes[<?=$k?>][articul]" value="<?=$v['articul']?>">
            </td>
            <td>
                <input type="text" name="sizes[<?=$k?>][code]" value="<?=$v['code']?>">
            </td>
            <td>
                <input type="text" name="sizes[<?=$k?>][discount]" value="<?=$v['discount']?>">
            </td>
        </tr>
    <?endforeach?>
    </table>
    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->

<script src="<?= $this->module->assetsUrl.'/scripts/controllers/ItemsCtrl.js' ?>"></script>