<?
$controller = $this;
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider'=>$dataProvider,
    'cssFile'=>false,
    'columns' => array(
        array(
            'name' => 'manufacturer',
            'type' => 'html',
            'value' => '$data->bmanufacturer->name',
        ),
        array(
            'name' => 'material',
            'type' => 'html',
            'value' => '$data->bmaterial->name',
        ),
        array(
            'name' => 'collection',
            'type' => 'html',
            'value' => '$data->bcollection->name',
        ),
        array('class' => 'CButtonColumn',
            'template' => '{update}',
            'buttons' => array
            (
                'update' => array
                (
                    'imageUrl'=>$this->module->assetsUrl.'/images/edit.png',
                    'url' => function ($data) use ($controller, $category) {
                            return $this->createUrl("edit", array("manufacturer" => $data->manufacturer, "material" => $data->material, 'category'=>$category, 'collection'=>$data->bcollection->id));
                        },
                ),
            ),
        ),
    )
));

?>