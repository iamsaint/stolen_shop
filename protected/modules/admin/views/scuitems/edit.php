<h1>Товары</h1>
<a class="add-big-button" href="<?=$this->createUrl('create', ['category'=>$category])?>"><span class="plused-text">Новый товар</span></a>
Поиск<br>
<input type="text" id="q" name="q" >
<?
$this->widget('zii.widgets.CListView', array(
    'dataProvider'=>$dataProvider,
    'itemView'=>'_edit_list',
    'itemsTagName'=>'ul',
    'itemsCssClass'=>'block-list-floated',
    'template'=>"{sorter}{items}",
    'sortableAttributes'=>array(
        'name',
        'update_time',
        'creation_time',
    )
));
?>

<?
Yii::app()->clientScript->registerScript('search', "
    var timer;
    $('input#q').keyup(function(){
        var q = this;
        window.clearTimeout(timer);

        timer = setTimeout(function () {
            $.fn.yiiListView.update('yw0', {
                data: $(q).serialize()
            });
        }, 1000);

        return false;
    });
");
?>
