<li class="item-block" id="item<?=$data->id?>">
    <div class="block-item fll">
        <?=Yii::app()->easyImage->thumbOf('.'.$data->images[0]->file, array('resize' => array('width' => 70, 'height' => 70)))?>
    </div>
    <div class="block-item fll"><?=$data->name?></div>
    <div class="block-item flr">
        <a href="<?=$this->createUrl('update', [
            'id'=>$data->id,
            'category'=>$_GET['category'],

        ])?>" class="update fll"><img src="<?=$this->module->assetsUrl?>/images/edit.png"></a>
        <?  echo CHtml::ajaxLink("", $this->createUrl('delete', ['id'=>$data->id]),
                [
                    'type'=>'POST',
                    'data'=>[
                        'returnUrl'=>$this->createUrl('edit', [
                                'category'=>$_GET['category'],
                            ])
                    ],
                    'success'=>'function(){
                        $("#item'.$data->id.'").remove()
                    }',
                    'error'=>'function(data){
                        alert(data.responseText)
                    }',
                ],

                [
                    'confirm'=>'Вы действительно хотите удалить товар?',
                    'class'=>'delete fll'
                ]);

        ?>
    </div>
</li>