
<h1>Редактирование товара <?=$model->name?></h1>

<div class="form" ng-controller="ScuitemsCtrl">

    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'categories-form',
        'enableAjaxValidation'=>false,
        'htmlOptions'=>array('enctype'=>'multipart/form-data'),
    )); ?>

    <input name="imagesForDelete" value="{{getDeleteJSON()}}" type="hidden">
    <input name="scuImagesForDelete" value="{{getScuDeleteJSON()}}" type="hidden">

    <?php echo $form->errorSummary($model); ?>


    <div class="image-list">
        <?foreach($model->images as $img):?>
            <div class="image-box" ng-hide="delImg[<?=$img->id?>]">
                <img src="<?=$img->file?>">
                <div class="delete" ng-click="deleteImage(<?=$img->id?>)"></div>
            </div>
        <?endforeach?>
        <div class="image-box" ng-repeat="n in newImages" ng-show="n.file!=''">
            <img ng-src="{{n.file}}">
            <div class="delete" ng-click="deleteNewImage($index)"></div>
            <input type="file" id="file{{$index}}" name="file[{{$index}}]">
        </div>
        <div class="image-box add" ng-click="openNewFile()"></div>
        <div class="name-block-after-img">
            <?php echo $form->labelEx($model,'name'); ?><br>
            <?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
            <?php echo $form->error($model,'name'); ?>
        </div>

    </div>

    <div class="row">

    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'price'); ?><br>
        <?php echo $form->textField($model,'price',array('size'=>60,'maxlength'=>255)); ?>
        <?php echo $form->error($model,'price'); ?>
    </div>
    <div class="row">

        <?php echo $form->checkBox($model,'active', array('checked'=>$model->active,'class'=>'checkbox')); ?>
        <?php echo $form->labelEx($model,'active',array('class'=>'checkbox-label')); ?>
        <?php echo $form->error($model,'active'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'descr'); ?>
        <?php echo $form->textArea($model,'descr',array('size'=>60,'maxlength'=>255)); ?>
        <?php echo $form->error($model,'descr'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'sticker'); ?>
        <?php echo $form->dropDownList($model,'sticker', Stickers::selectList([], true, 'Не выбран')); ?>
        <?php echo $form->error($model,'sticker'); ?>
    </div>

    <table>
    <?for($i=0;$i<count($catalogsItems);$i++):?>
        <tr>
            <td><?=$catalogsItems[$i]->name?> <?=$catalogsItems[$i]->filter==1? '*' : ''?></td>
            <td><input type="hidden" name="PRODUCT[product_options][<?=$catalogsItems[$i]->id?>]" value="{{PRODUCT.product_items[<?=$catalogsItems[$i]->id?>]}}">
                <select class="styled"
                    ng-model="PRODUCT.product_items[<?=$catalogsItems[$i]->id?>]"
                    ng-options="opt.key as opt.name for opt in PRODUCT.product_options[<?=$i?>][<?=$catalogsItems[$i]->id?>] | orderBy : 'name'">
                </select>
                    <div class="selected-wrapper"><!--p class="select-selected">{{ (PRODUCT.product_options[<?=$i?>][<?=$catalogsItems[$i]->id?>] | filter : {key: PRODUCT.product_items[<?=$catalogsItems[$i]->id?>]})[0].name }}</p-->
                    <ul class="styled-select" ng-class="{opened:dd_list<?=$catalogsItems[$i]->id?>}" ng-click="dd_list<?=$catalogsItems[$i]->id?>=!dd_list<?=$catalogsItems[$i]->id?>">
                        <li class="selected" ng-hide="PRODUCT.product_items[<?=$catalogsItems[$i]->id?>]!=undefined">-не выбрано-</li>
                        <li ng-class = "{selected:PRODUCT.product_items[<?=$catalogsItems[$i]->id?>]==i.key}" ng-click="PRODUCT.product_items[<?=$catalogsItems[$i]->id?>]=i.key" ng-repeat="i in PRODUCT.product_options[<?=$i?>][<?=$catalogsItems[$i]->id?>] | orderBy : 'name'">{{i.name}}</li>
                    </ul>
                    </div>
            </td>

        </tr>
    <?endfor?>
    </table>

    <? $rowspan = count($catalogs)+1?>

    <h1>Торговые предложения</h1>
    <table class="table-white" ng-repeat="i in getSCU()" style="margin-bottom: 10px">
        <thead>        <tr>
            <th colspan="2">Опции</th>
            <th>Цена</th>
            <th>Скидка</th>
            <th>Артикул</th>
            <th>Код</th>
            <th>Описание</th>
            <th>Активность</th>
            <th></th>
        </tr></thead>

        <tr>
            <td></td>
            <td></td>

            <td rowspan="<?=$rowspan?>"><input type="text" name="SCU[{{i.id || $index}}][price]" ng-model="SCU.items[$index].price"></td>
            <td rowspan="<?=$rowspan?>"><input type="text" name="SCU[{{i.id || $index}}][discount]" ng-model="SCU.items[$index].discount"></td>
            <td rowspan="<?=$rowspan?>"><input type="text" name="SCU[{{i.id || $index}}][articul]" ng-model="SCU.items[$index].articul"></td>
            <td rowspan="<?=$rowspan?>"><input type="text" name="SCU[{{i.id || $index}}][code]" ng-model="SCU.items[$index].code"></td>
            <td rowspan="<?=$rowspan?>"><textarea name="SCU[{{i.id || $index}}][descr]" ng-model="SCU.items[$index].descr"></textarea></td>
            <td rowspan="<?=$rowspan?>"><input class="checkbox" type="checkbox" id="SCU[{{i.id || $index}}][active]" name="SCU[{{i.id || $index}}][active]" ng-model="SCU.items[$index].active" ng-checked="SCU.items[$index].active==1"><label for="SCU[{{i.id || $index}}][active]" class="checkbox-label"></label></td>
            <td rowspan="<?=$rowspan?>">
                <a href="javascript:void(0)" class="delete" ng-click="deleteSCU($index)"></a>
            </td>
        </tr>
        <?for($i=0;$i<count($catalogs);$i++):?>
            <tr>
                <td><?=$catalogs[$i]->name?></td>
                <td>
                    <input type="hidden" name="SCU[{{i.id || $index}}][options][<?=$catalogs[$i]->id?>]" value="{{SCU.items[$index].options[<?=$catalogs[$i]->id?>]}}">
                    <select class="styled"
                        ng-model="SCU.items[$index].options[<?=$catalogs[$i]->id?>]"
                        ng-options="opt.key as opt.name for opt in SCU.options[<?=$i?>][<?=$catalogs[$i]->id?>]  | orderBy : 'name'"></select>
                    <div class="selected-wrapper">
                        <ul class="styled-select" ng-class="{opened:dd_list<?=$catalogs[$i]->id?>}" ng-click="dd_list<?=$catalogs[$i]->id?>=!dd_list<?=$catalogs[$i]->id?>">
                            <li ng-show="SCU.items[$index].options[<?=$catalogs[$i]->id?>]==0" ng-class="{selected:SCU.items[$index].options[<?=$catalogs[$i]->id?>]==0}" ng-click="SCU.items[$index].options[<?=$catalogs[$i]->id?>]=0">не выбрано</li>
                            <li ng-class = "{selected:SCU.items[$parent.$index].options[<?=$catalogs[$i]->id?>]==opt.key}" ng-click="SCU.items[$parent.$index].options[<?=$catalogs[$i]->id?>]=opt.key" ng-repeat="opt in SCU.options[<?=$i?>][<?=$catalogs[$i]->id?>]  | orderBy : 'name'">{{opt.name}}</li>
                        </ul>
                    </div>
                </td>
            </tr>
        <?endfor?>
        <tr>
            <td colspan="9">
                <div class="image-list">

                    <div class="image-box" ng-repeat="n in SCU.items[$index].images" ng-show="n.file!=''">
                        <img ng-src="{{n.file}}">
                        <div class="delete" ng-click="deleteScuImage($parent.$index, $index)"></div>
                        <input type="file" id="scuimagefile_{{$parent.$index}}_{{$index}}" name="scuimagefile_{{i.id || $parent.$index}}[{{$index}}]">
                    </div>

                    <div class="image-box add" ng-click="addScuImage($index)"></div>
                </div>
            </td>
        </tr>
    </table>
    <input name="SCU_DEL" value="{{SCU.toDelete|json}}" type="hidden">
    <br>
    <a id="add-select" href="javascript:void(0)" ng-click="addSCU()">Добавить торговое предложение</a>
    <br>

    <br><div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    var SCU = {options:[], items : []};
    var PRODUCT = {product_options: [], product_items: []};

    <?foreach($catalogsItems as $cat):?>
        <?
            $items = Catalogitems::selectList(['catalog'=>$cat->id]);
            $data = [];
            foreach($items as $k=>$v)
                $data[$cat->id][]=['key'=>$k, 'name'=>$v]
        ?>
        PRODUCT.product_options.push(<?=CJavaScript::encode($data)?>);
    <?endforeach?>

    <?foreach($catalogs as $cat):?>
        <?
            $items = Catalogitems::selectList(['catalog'=>$cat->id]);
            $data = [];
            foreach($items as $k=>$v)
                $data[$cat->id][]=['key'=>$k, 'name'=>$v]
        ?>
        SCU.options.push(<?=CJavaScript::encode($data)?>);
    <?endforeach?>

    <?
        foreach($SCU as $scu) {
            $s = [];

            foreach($scu->scuitems as $itm)
                $s['options'][$itm->catalogitem] = (int)$itm->value;

            foreach($scu->images as $img)
                $s['images'][] = [
                    'id'=>$img->id,
                    'file'=>$img->file,
                ];

            $s['id']=$scu->id;
            $s['price']=$scu->price;
            $s['active']=$scu->active;
            $s['discount']=$scu->discount;
            $s['articul']=$scu->articul;
            $s['code']=$scu->code;
            $s['descr']=$scu->descr;
    ?>
        SCU.items.push(<?=CJavaScript::encode($s)?>)
    <? } ?>

    <?
         $s = [];
         foreach($OPTIONS as $opt) {
             $s[$opt->catalogitem] = (int)$opt->value;
         }
    ?>
    PRODUCT.product_items = <?=CJavaScript::encode($s)?>

</script>
<script src="<?= $this->module->assetsUrl.'/scripts/controllers/ScuitemsCtrl.js' ?>"></script>
