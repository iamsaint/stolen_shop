<h1>Производители</h1>
<a class="add-big-button" href="<?=$this->createUrl('create')?>"><span class="plused-text">Новый производитель</span></a>


<?
    $this->widget('zii.widgets.grid.CGridView', array(
        'dataProvider'=>$dataProvider,
        'cssFile'=>false,
        'selectableRows'=>1,
        'selectionChanged'=>'function(id){ window.location = "'.$this->createUrl('update').'/id/"+$.fn.yiiGridView.getSelection(id) }',
        'columns'=>array(
            'name',
            array('class'=>'CButtonColumn',
                'template'=>'{delete}',
                'buttons'=>[
                    'delete'=>['imageUrl'=>$this->module->assetsUrl.'/images/delete.png']
                ]
            ),
        ),
    ));
?>
