<h1>Комплектации</h1>
<a class="add-big-button" href="<?=$this->createUrl('create', ['category'=>$category])?>"><span class="plused-text">Новая комплектация</span></a>
<?=CHtml::link('Очистить фильтр', ['clear', 'category'=>$category])?><br><br>
<?
    $this->widget('zii.widgets.grid.CGridView', array(
        'dataProvider'=>$model->search(),
        'pager' => array('cssFile' => false),
        'cssFile'=>false,
        'enableHistory'=>true,
        'filter'=>$model,
        'selectableRows'=>1,
        'selectionChanged'=>'function(id){ window.location = "'.$this->createUrl('update').'/id/"+$.fn.yiiGridView.getSelection(id) }',
        'columns'=>array(
            array(
                'name'=>'manufacturer',
                'type'=>'html',
                'value'=>'$data->bmanufacturer->name',
                'filter'=>CHtml::dropDownList('Complectations[manufacturer]', $model->manufacturer, Manufacturers::selectList(['mmcategory.id'=>$model->category], true),array('class'=>'no-style')),
            ),
            array(
                'name'=>'material',
                'type'=>'html',
                'value'=>'$data->bmaterial->name',
                'filter'=>CHtml::dropDownList('Complectations[material]', $model->material, Materials::selectList(['mmcategory.id'=>$model->category], true),array('class'=>'no-style')),
            ),
            array(
                'name'=>'size',
                'type'=>'html',
                'value'=>'$data->bsize->name',
                'filter'=>CHtml::dropDownList('Complectations[size]', $model->size, Sizes::selectList(['mmcategory.id'=>$model->category], true),array('class'=>'no-style')),
            ),
            array(
                'name'=>'collection',
                'type'=>'html',
                'value'=>'$data->bcollection->name',
                'filter'=>false,
            ),
            [
                'name'=>'descr',
                'filter'=>false,
                'value'=>'$data->descr'
            ],
            [
                'name'=>'price',
                'filter'=>false,
                'value'=>'$data->price'
            ],
            array('class'=>'CButtonColumn',
                'template'=>'{delete}',
                'buttons'=>[
                    'delete'=>['imageUrl'=>$this->module->assetsUrl.'/images/delete.png']
                ]
            ),
        ),

    ));
?>
