
<h1>Редактирование комплектации</h1>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'categories-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

    <table>
	<tr class="row">
		<td><?php echo $form->labelEx($model,'descr'); ?></td>
        <td><?php echo $form->textArea($model,'descr',array('size'=>60,'maxlength'=>255)); ?></td>
        <td><?php echo $form->error($model,'descr'); ?></td>
	</tr>

	<tr class="row">
        <td><?php echo $form->labelEx($model,'price'); ?></td>
        <td><?php echo $form->textField($model,'price',array('size'=>60,'maxlength'=>255)); ?></td>
        <td><?php echo $form->error($model,'price'); ?></td>
	</tr>

	<tr class="row">
        <td><?php echo $form->labelEx($model,'manufacturer'); ?></td>
        <td><?php echo $form->dropDownList($model,'manufacturer', Manufacturers::selectList($criteria, true, 'Не выбрано')); ?></td>
        <td><?php echo $form->error($model,'manufacturer'); ?></td>
	</tr>

	<tr class="row">
        <td><?php echo $form->labelEx($model,'material'); ?></td>
        <td><?php echo $form->dropDownList($model,'material', Materials::selectList($criteria, true, 'Не выбрано')); ?></td>
        <td><?php echo $form->error($model,'material'); ?></td>
	</tr>

	<tr class="row">
        <td><?php echo $form->labelEx($model,'size'); ?></td>
        <td><?php echo $form->dropDownList($model,'size', Sizes::selectList($criteria, true, 'Не выбрано')); ?></td>
        <td><?php echo $form->error($model,'size'); ?></td>
	</tr>

	<tr class="row">
        <td><?php echo $form->labelEx($model,'collection'); ?></td>
        <td><?php echo $form->dropDownList($model,'collection', Collections::selectList($criteria, true, 'По умолчанию')); ?></td>
        <td><?php echo $form->error($model,'collection'); ?></td>
	</tr>

    <tr class="row">
        <td><?php echo $form->labelEx($model,'articul'); ?></td>
        <td><?php echo $form->textField($model,'articul',array('size'=>60,'maxlength'=>255)); ?></td>
        <td><?php echo $form->error($model,'articul'); ?></td>
    </tr>

    <tr class="row">
        <td><?php echo $form->labelEx($model,'note'); ?></td>
        <td><?php echo $form->textArea($model,'note',array('size'=>60,'maxlength'=>255)); ?></td>
        <td><?php echo $form->error($model,'note'); ?></td>
    </tr>
    </table>
    <div class="row buttons">
        <br><?php echo CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>
    <script src="<?php echo $this->module->assetsUrl?>/scripts/select.js"></script>
</div><!-- form -->