<?
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider'=>$dataProvider,
    'columns'=>array(
        'name',
        array('class'=>'CButtonColumn',
            'template'=>'{update}',
            'buttons' => array
            (
                'update' => array
                (
                    'url' => function ($data) use ($controller) {
                            return $this->createUrl("edit", ['category'=>$data->id] );
                        },
                    'imageUrl'=>$this->module->assetsUrl.'/images/update.png'
                ),
            ),
        ),
    ),
));
?>
