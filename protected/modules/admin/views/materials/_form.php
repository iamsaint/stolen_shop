
<h1>Редактирование материала <?=$model->name?></h1>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'materials-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?><br>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'package'); ?><br>
		<?php echo $form->dropDownList($model,'package', Packages::selectList()); ?>
		<?php echo $form->error($model,'package'); ?>
	</div><br>

    <br><input type="checkbox" id="cat[1]" name="categoryConnect[1]" checked  style="display: none"/>

    <div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>
    <script src="<?php echo $this->module->assetsUrl?>/scripts/select.js"></script>
</div><!-- form -->