<h1>Категории</h1>
<a class="add-big-button" href="<?=$this->createUrl('create')?>"><span class="plused-text">Новая категория</span></a>
<?
    $this->widget('zii.widgets.grid.CGridView', array(
        'dataProvider'=>$dataProvider,
        'cssFile'=>false,
        'selectableRows'=>1,
        'selectionChanged'=>'function(id){ window.location = "'.$this->createUrl('update').'/id/"+$.fn.yiiGridView.getSelection(id) }',
        'columns'=>array(
            array(
                'name'=>'image',
                'type'=>'html',
                'value'=>'"<img src=\'".$data->bimage->file."\'>"',
            ),
            'name',
            'descr',
            array('class'=>'CButtonColumn',
                'template'=>'{delete}',
                'buttons'=>[
                    'delete'=>['imageUrl'=>$this->module->assetsUrl.'/images/delete.png']
                ]
            ),
        ),
    ));
?>

