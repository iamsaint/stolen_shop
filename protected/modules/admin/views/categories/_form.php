
<h1>Редактирование категории <?=$model->name?></h1>

<div class="form" ng-controller="CategoryCtrl">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'categories-form',
	'enableAjaxValidation'=>false,
    'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>

	<?php echo $form->errorSummary($model); ?>
    <table>
	<tr class="row">
		<td><?php echo $form->labelEx($model,'name'); ?></td>
        <td><?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?></td>
        <td><?php echo $form->error($model,'name'); ?></td>
	</tr>

	<tr class="row">
        <td><?php echo $form->labelEx($model,'code'); ?></td>
        <td><?php echo $form->textField($model,'code',array('size'=>60,'maxlength'=>255)); ?></td>
        <td><?php echo $form->error($model,'code'); ?></td>
	</tr>

	<tr class="row">
        <td><?php echo $form->labelEx($model,'descr'); ?></td>
        <td><?php echo $form->textField($model,'descr',array('size'=>60,'maxlength'=>255)); ?></td>
        <td><?php echo $form->error($model,'descr'); ?></td>
	</tr>
	<tr class="row">
        <td colspan="2">
        <?php echo $form->checkBox($model,'imagetype',  array('checked'=>$model->imagetype==1?'checked':'','class'=>'checkbox')); ?><?php echo $form->labelEx($model,'imagetype',array('class'=>'checkbox-label')); ?></td>
        <td><?php echo $form->error($model,'imagetype'); ?></td>
	</tr>

	<tr class="row">
        <td><?php echo $form->labelEx($model,'image'); ?></td>

        <td><a href="#fileimage" ng-click="openNewFile()">
                <img src="{{getImage()}}" ng-init="fileimage='<?=$model->bimage->file?:$this->module->assetsUrl.'/images/default.png'?>'" style="width: 70px">
            </a>
            <input type="file" name="fileimage" id="fileimage" style="display: none"/></td>

        <td><?php echo $form->error($model,'image'); ?></td>
	</tr>

    </table>
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить'); ?>


<?php $this->endWidget(); ?>

</div><!-- form -->

<script src="<?= $this->module->assetsUrl.'/scripts/controllers/CategoryCtrl.js' ?>"></script>