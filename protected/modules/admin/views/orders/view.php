<h3>Даные заказчика</h3>
<table class="items">
    <tr>
        <td>ФИО</td>
        <td><?=$model->morderdata[0]->fio?></td>
    </tr>
    <tr>
        <td>Email</td>
        <td><?=$model->morderdata[0]->email?></td>
    </tr>
    <tr>
        <td>Телефон</td>
        <td><?=$model->morderdata[0]->phone?></td>
    </tr>
    <tr>
        <td>Почтовый индекс</td>
        <td><?=$model->morderdata[0]->zip?></td>
    </tr>
    <tr>
        <td>Город</td>
        <td><?=$model->morderdata[0]->city?></td>
    </tr>
    <tr>
        <td>Адрес</td>
        <td><?=$model->morderdata[0]->address?></td>
    </tr>
    <tr>
        <td>Транспортная компания</td>
        <td><?=$model->morderdata[0]->bdelivery->name?></td>
    </tr>
    <tr>
        <td>Комментарий</td>
        <td><?=$model->morderdata[0]->comment?></td>
    </tr>
</table>

<h3>Товары в заказе</h3>
<table class="items">
    <thead>
        <th></th>
        <th>Название</th>
        <th>Параметры</th>
        <th>Количество</th>
        <th>Цена</th>
        <th>Стоимость</th>
    </thead>
    <tbody>
    <?foreach($model->morderitems as $item):?>
        <tr>
            <td>
                <?if(is_null($item->scu)):?>
                    <img src="<?=$item->icomp->bitem->images[0]->file?>">
                <?else:?>
                    <?
                        $scuImageFile = isset($item->bscu->images[0]->file)? $item->bscu->images[0]->file : $item->bscu->item->images[0]->file;
                    ?>
                    <img src="<?=Yii::app()->easyImage->thumbSrcOf('.'. $scuImageFile, array('resize' => array('width' => 64, 'height' => 64)))?>">
                <?endif?>
            </td>
            <td><?=$item->icomp->bitem->name?></td>
            <td>
                <?if(is_null($item->scu)):?>
                    <strong>Размер</strong> <?=$item->icomp->bcomplectation->bsize->name?><br>
                    <strong>Материал</strong> <?=$item->icomp->bcomplectation->bmaterial->name?><br>
                    <strong>Производитель</strong> <?=$item->icomp->bcomplectation->bmanufacturer->name?><br>
                <?else:?>
                    <?foreach($item->bscu->scuitems as $s):?>
                        <strong><?=$s->bcatalog->name?></strong> <?=$s->bcatalogitem->name?><br>
                    <?endforeach?>
                <?endif?>
            </td>
            <td><?=$item->count?></td>
            <td>
                <?=is_null($item->scu)?$item->icomp->bcomplectation->price:$item->bscu->price?>
            </td>
            <td><?=$price = (is_null($item->scu)?$item->icomp->bcomplectation->price:$item->bscu->price)*$item->count ?> руб.</td>
        </tr>
        <? $total += $price?>
    <?endforeach?>
    </tbody>
    <tfoot>
    <tr>
        <td colspan="6"></td>
        <td>Итого:</td>
        <td><?=$total?> руб.</td>
    </tr>
    </tfoot>
</table>

<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'materials-form',
        'enableAjaxValidation'=>false,
    )); ?>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model,'status'); ?>
        <?php echo $form->dropDownList($model,'status',array(
            1=>'Подтвержден',
        )); ?>
        <?php echo $form->error($model,'status'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->