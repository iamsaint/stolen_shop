<h1>Заказы</h1>


<?

$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $dataProvider,
    'cssFile'=>false,
    'ajaxUpdate'=>false,
    'selectableRows'=>1,
    'selectionChanged'=>'function(id){ window.location = "'.$this->createUrl('view').'/id/"+$.fn.yiiGridView.getSelection(id) }',
    'columns' => array(
        ['name' => 'Номер заказа', 'value' => '$data->order_num'],
        ['name' => 'Дата', 'value' => '$data->creation_date'],
        ['name' => 'ФИО', 'value' => '$data->morderdata[0]->fio'],
        ['name' => 'Email', 'value' => '$data->morderdata[0]->email'],
        ['name' => 'Телефон', 'value' => '$data->morderdata[0]->phone'],
        ['name' => 'Почтовый индекс', 'value' => '$data->morderdata[0]->zip'],
        ['name' => 'Город', 'value' => '$data->morderdata[0]->city'],
        ['name' => 'Адрес', 'value' => '$data->morderdata[0]->address'],
        ['name' => 'Транспортная компания', 'value' => '$data->morderdata[0]->bdelivery->name'],
        ['name' => 'Комментарий', 'value' => '$data->morderdata[0]->comment'],
    ),
));
?>
