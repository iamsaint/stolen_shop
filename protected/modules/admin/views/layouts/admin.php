<!doctype html>
<html ng-app="admin">
<head></head>
<body>
<link rel="stylesheet" type="text/css" href="<?php echo $this->module->assetsUrl?>/css/admin.css" />
<script src="/bower_components/angular/angular.min.js"></script>
<script src="<?php echo $this->module->assetsUrl?>/scripts/app.js"></script>

<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800,300&subset=latin,cyrillic' rel='stylesheet' type='text/css'>

<div class="leftPanel fll">
    <div class="sidebar-header">
        <img class="volta-cms" src="<?php echo $this->module->assetsUrl?>/images/volta-cms.png" />
        <div class="sidebar-header-logo"><div class="sidebar-header-logo-img"><img src="/styles/img/favicon.png"></div></div>
    </div>
    <div class="sidebar-body">
        <p class="sidebar-body-name tcenter">Текстиль Профи — магазин</p>
        <p class="tcenter"><a class="sidebar-body-web-link" href="http://<?=$_SERVER['HTTP_HOST']?>">http://<?=$_SERVER['HTTP_HOST']?>/</a></p>
    </div>
    <div class="menu">
    <div class="sidebar-drop-btn"><h3>КПБ</h3>
    <ul class="drop-down-sidebar">
        <li class="sidebar-menu-item"><a href="/admin/manufacturers">Производители</a></li>
        <li class="sidebar-menu-item"><a href="/admin/packages">Упаковки</a></li>
        <li class="sidebar-menu-item"><a href="/admin/collections">Коллекции</a></li>
        <li class="sidebar-menu-item"><a href="/admin/materials">Материалы</a></li>
        <li class="sidebar-menu-item"><a href="/admin/sizes">Размеры</a></li>
        <li class="sidebar-menu-item"><a href="/admin/complectations">Комплектации</a></li>
        <li class="sidebar-menu-item"><a href="/admin/items">Товары</a></li>
    </ul>
    </div>
    <div class="sidebar-drop-btn"><h3>Другое</h3>
    <ul class="drop-down-sidebar">
        <li class="sidebar-menu-item"><a href="/admin/categories">Категории</a></li>
        <li class="sidebar-menu-item"><a href="/admin/catalogs">Справочники</a></li>
        <li class="sidebar-menu-item"><a href="/admin/scuitems">Товары</a></li>
    </ul>
    </div>
    <div class="sidebar-drop-btn"><h3>Заказы</h3>
    <ul class="drop-down-sidebar">
        <li class="sidebar-menu-item"><a href="/admin/orders">Заказы</a></li>
    </ul>
    </div>
    <a href="/user/logout" target="_self" class="sidebar-drop-btn" style="position: fixed; bottom: 0px; width: 270px"><h3>Выход</h3></a>
    <div class="sidebar-bottom"></div>
    </div>
</div>

<div class="mainPanel">
<?=$content?>
</div>
</body>
</html>