<?php


/**
 * This is the model class for table "orders".
 *
 * The followings are the available columns in table 'orders':
 * @property integer $id
 * @property integer $user
 * @property string $cookie
 * @property string $status
 * @property string $tmp
 *
 * The followings are the available model relations:
 * @property Orderitems[] $orderitems
 */
class Orders extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'orders';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user', 'numerical', 'integerOnly'=>true),
			array('cookie, status, tmp', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user, cookie, status, tmp', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'morderitems' => array(self::HAS_MANY, 'Orderitems', 'order'),
			'morderdata' => array(self::HAS_MANY, 'Orderdata', 'order'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user' => 'User',
			'cookie' => 'Cookie',
			'status' => 'Status',
			'tmp' => 'Tmp',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user',$this->user);
		$criteria->compare('cookie',$this->cookie,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('tmp',$this->tmp,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Orders the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public static function getBasket($id=null) {
        if($id===null) {
            $cookie = Yii::app()->request->cookies['basket_id']->value;
            return self::model()->findByAttributes([
                'cookie'=>$cookie
            ]);
        } else {
            return self::model()->findByPk($id);
        }
    }

    public static function getBasketItems(){
        $basket =  self::getBasket();

        if(is_array($basket->morderitems))
        foreach($basket->morderitems as $item){
            if($item->scu > 0) {
                $scuImageFile = isset($item->bscu->images[0]->file)? $item->bscu->images[0]->file : $item->bscu->item->images[0]->file;
                $tItem = [
                    'id'=>$item->bscu->id,
                    'name'=>$item->bscu->item->name,
                    'image'=>Yii::app()->easyImage->thumbSrcOf('.'. $scuImageFile, array('resize' => array('width' => 64, 'height' => 64))),
                    'price'=>$item->bscu->price > 0 ?$item->bscu->price:$item->bscu->item->price,
                    'count'=>$item->count,
                    'category'=>$item->bscu->item->bcategory->name,
                    'link'=>'/catalog/'.$item->bscu->item->bcategory->code.'/'.$item->bscu->item->code,
                    'scu'=>[]
                ];

                foreach($item->bscu->scuitems as $i) {
                    if($i->bcatalogitem->bcatalog->basket == 0) continue;
                    $tItem['scu'][]=[
                        'title'=>$i->bcatalogitem->bcatalog->name,
                        'name' => $i->bcatalogitem->name
                    ] ;
                }
                foreach($item->bscu->item->popt as $i) {
                    if($i->bcatalogitem->bcatalog->basket == 0) continue;
                    $tItem['scu'][]=[
                        'title'=>$i->bcatalogitem->bcatalog->name,
                        'name' => $i->bcatalogitem->name
                    ] ;
                }

                $res[]=$tItem;

            } else {
                $item = [
                    'id'=>$item->icomp->id,
                    'name'=>$item->icomp->bitem->name,
                    'image'=>Yii::app()->easyImage->thumbSrcOf('.'.$item->icomp->bitem->images[0]->file, array('resize' => array('width' => 64, 'height' => 64))),
                    'material'=>$item->icomp->bcomplectation->bmaterial->name,
                    'manufacturer'=>$item->icomp->bcomplectation->bmanufacturer->name,
                    'size'=>$item->icomp->bcomplectation->bsize->name,
                    'package'=>$item->icomp->bcomplectation->bmaterial->bpackage->name,
                    'category'=>$item->icomp->bitem->bcategory->name,
                    'price'=>$item->icomp->bcomplectation->price,
                    'link'=>'/catalog/'.$item->icomp->bitem->bcategory->code.'/'.$item->icomp->bitem->code,
                    'count'=>$item->count,
                ];

                $res[]=$item;
            }
        }
        return $res;
    }

    public static function removeAllItems($id, $basketId=null){
        $basket =  self::getBasket($basketId);
        return Orderitems::model()->deleteAllByAttributes([
            'order'=>$basket->id,
            'icomplectation'=>$id,
        ]);
    }

    public static function removeAllItemsscu($id, $basketId=null){
        $basket =  self::getBasket($basketId);
        return Orderitems::model()->deleteAllByAttributes([
            'order'=>$basket->id,
            'scu'=>$id,
        ]);
    }

    public static function removeItemscu($id, $count=1, $basketId=null){
        $basket =  self::getBasket($basketId);
        $model = Orderitems::model()->findByAttributes([
            'order'=>$basket->id,
            'scu'=>$id,
        ]);
        if($model->count > 1) {
            $model->count = $model->count - 1;
            return $model->save();
        } else {
            return self::removeAllItemsscu($id, $basketId);
        }
    }

    public static function removeItem($id, $count=1, $basketId=null){
        $basket =  self::getBasket($basketId);
        $model = Orderitems::model()->findByAttributes([
            'order'=>$basket->id,
            'icomplectation'=>$id,
        ]);
        if($model->count > 1) {
            $model->count = $model->count - 1;
            return $model->save();
        } else {
            return self::removeAllItems($id, $basketId);
        }
    }

    public static function setStatus($status, $basketId=null) {
        $basket =  self::getBasket($basketId);
        $basket->status = $status;
        return $basket->save();
    }

    public static function saveOrder($data, $basketId=null) {
        $basket =  self::getBasket($basketId);
        $basket->status = 1;
        if($basket->save()) {
            $odata = new Orderdata;
            $odata->attributes = $data;
            $odata->order = $basket->id;
            if($odata->save()) {

                Yii::app()->request->cookies['basket_id'] = new CHttpCookie('basket_id', uniqid());
                $mail = new YiiMailer('orderEmail', ['orderdata'=>$data, 'model'=>$basket]);
                $mail->setLayout('email');
                $mail->setFrom(Yii::app()->params['mailerEmail'], Yii::app()->params['mailerName']);
                $mail->setSubject('Новый заказ');

                $mail->setTo($odata->email);
                $mail->send();

                $mail->setTo(Yii::app()->params['orderEmail']);
                $mail->send();

                return true;
            }
            else self::setStatus(0, $basket);
        } else return false;
    }
}
