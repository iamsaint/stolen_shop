<?php

/**
 * This is the model class for table "complectations".
 *
 * The followings are the available columns in table 'complectations':
 * @property integer $id
 * @property integer $manufacturer
 * @property integer $material
 * @property integer $size
 * @property integer $collection
 * @property integer $price
 * @property integer $package
 *
 * The followings are the available model relations:
 * @property Manufacturers $manufacturer0
 * @property Materials $material0
 * @property Sizes $size0
 * @property Icomplectations[] $icomplectations
 */
class Complectations extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'complectations';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('manufacturer, material, size, collection, price', 'numerical', 'integerOnly'=>true),
			array('manufacturer, material, size, collection, price, descr', 'required'),
            array('manufacturer, material, size, collection, category', 'ECompositeUniqueValidator', 'message'=>'Указанная комбинация производителя, материала и размера уже существует'),
			array('id, manufacturer, material, size, price, descr, note', 'safe', 'on'=>'search'),
			array('note, articul', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'bmanufacturer' => array(self::BELONGS_TO, 'Manufacturers', 'manufacturer'),
			'bmaterial' => array(self::BELONGS_TO, 'Materials', 'material'),
			'bcollection' => array(self::BELONGS_TO, 'Collections', 'collection'),
			'bsize' => array(self::BELONGS_TO, 'Sizes', 'size'),
			'micat' => array(self::HAS_MANY, 'Icomplectations', 'complectation'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'manufacturer' => 'Производитель',
			'material' => 'Материал',
			'size' => 'Размер',
			'price' => 'Цена',
			'descr' => 'Описание',
			'note' => 'Примечание',
			'collection' => 'Коллекция',
			'articul' => 'Артикул',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('manufacturer',$this->manufacturer);
		$criteria->compare('material',$this->material);
		$criteria->compare('size',$this->size);
		$criteria->compare('price',$this->price);
		$criteria->compare('category',$this->category);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort'=>[
                'defaultOrder'=>'manufacturer ASC, material ASC, size ASC'
            ],
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Complectations the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
