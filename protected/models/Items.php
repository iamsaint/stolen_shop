<?php

/**
 * This is the model class for table "items".
 *
 * The followings are the available columns in table 'items':
 * @property integer $id
 * @property string $name
 * @property string $code
 * @property timestamp $update_time
 * @property datetime $creation_time
 *
 * The followings are the available model relations:
 * @property Icomplectations[] $icomplectations
 * @property Itemimages[] $itemimages
 */
class Items extends CActiveRecord
{
    use Translit;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'items';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('price, active','numerical', 'integerOnly'=>true),
			array('name, code', 'length', 'max'=>255),
            array('name', 'uniqueName'),
            array('name', 'required'),
            array('name', 'required', 'on'=>'skuitems'),
            array('code', 'unique'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('descr, sticker', 'safe'),
			array('id, name, code, descr', 'safe', 'on'=>'search'),
		);
	}

    public function uniqueName($attribute,$params)
    {
        if($this->isNewRecord)
        {
            $condition = ['and','items.id!=:id', 'items.name=:name','complectations.manufacturer=:manufacturer', 'complectations.material=:material'];
            $params = [':id'=>$this->id,':name'=>$this->name, ':manufacturer'=>$comp->manufacturer, ':material'=>$comp->material];

            $rows = Categoryitems::findAll($condition, $params);

            if(count($rows)>0)
                $this->addError($attribute, 'Товар с таким названием уже существует');
        } else {
            foreach($this->comps as $comp) {
                $condition = ['and','items.id!=:id', 'items.name=:name','complectations.manufacturer=:manufacturer', 'complectations.material=:material'];
                $params = [':id'=>$this->id,':name'=>$this->name, ':manufacturer'=>$comp->manufacturer, ':material'=>$comp->material];

                $rows = Categoryitems::findAll($condition, $params);

                if(count($rows)>0) {
                    $this->addError($attribute, 'Товар с таким названием уже существует');
                    return false;
                } else return true;
            }
        }
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'comps' => array(self::MANY_MANY, 'Complectations', 'icomplectations(complectation, item)'),
			'icomps' => array(self::HAS_MANY, 'Icomplectations', 'item'),
			'images' => array(self::MANY_MANY, 'Images', 'itemimages(image, item)'),
			'scu' => array(self::HAS_MANY, 'Scu', 'itemid'),
			'popt' => array(self::HAS_MANY, 'ItemOptions', 'itemid'),
			'bcategory' => array(self::BELONGS_TO, 'Categories', 'category'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Название',
			'code' => 'Code',
			'update_time' => 'Дата изменения',
			'creation_time' => 'Дата создания',
			'descr' => 'Описание',
			'price' => 'Цена',
			'active' => 'Активность',
			'sticker' => 'Стикер',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('code',$this->code,true);
		$criteria->compare('update_time',$this->update_time,true);
		$criteria->compare('creation_time',$this->creation_time,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Items the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public static function getItemCard($code) {
        $item = self::model()->findByAttributes(['code'=>$code, 'active'=>1]);
        if($item === null) return false;

        $result = [];
        $result['name'] = $item->name;
        $result['id'] = $item->id;
        $result['category'] = $item->category;
        $result['category_data'] = [
            'code'=>$item->bcategory->code,
            'imagetype'=>$item->bcategory->code,
        ];

        foreach($item->images as $img)
            $result['images'][]= [
                'file'=>Yii::app()->easyImage->thumbSrcOf('.'.$img->file, array('resize' => array('width' => 570, 'height' => 570))),
                'thumb'=>Yii::app()->easyImage->thumbSrcOf('.'.$img->file, array('resize' => array('width' => 94, 'height' => 94)))
        ];

        if($item->category == 1) {

            $result['manufacturer'] = $item->comps[0]->bmanufacturer->name;
            $result['material'] = $item->comps[0]->bmaterial->name;
            $result['package'] = $item->comps[0]->bmaterial->bpackage->name;
            if($item->comps[0]->bcollection->id > 1)
                $result['collection'] = $item->comps[0]->bcollection->name;

            $sz = [];

            foreach($item->icomps as $icomp) {
                if($icomp->active!=1) continue;
                $comp = $icomp->bcomplectation;
                $discount = ($icomp->discount > 0)?$icomp->discount:false;
                $result['sku'][$comp->bsize->id] = [
                    'price'=>ceil($discount?$comp->price*$discount:$comp->price),
                    'descr'=>str_replace("\n", '<br>',$comp->descr),
                    'articul' => $icomp->articul?:$comp->articul,
                    'code' => $icomp->code,
                    'comp' => $comp->id,
                ];
                if(!$sz[$comp->bsize->id])
                    $sz[$comp->bsize->id] = ['id'=>$comp->bsize->id, 'name' => $comp->bsize->name];
            }

            foreach($sz as $k=>$v)
                $result['sizes'][]=$v;
        } else {
            $result['descr'] = $item->descr;
            $options = Catalogs::model()->with('catalogitems')->findAllByAttributes(['category'=>$item->category]);
            foreach($options as $o) {
                $opt = ['key'=>$o->id,'value'=>$o->name];
                foreach($o->catalogitems as $ci)
                    $opt['items'][]=['key'=>$ci->id, 'name'=>$ci->name];
                if($o->type==0)
                    $result['product_options'][]=$opt;
                else
                    $result['options'][]=$opt;
            }

            // опции товара
            $itemOpt = [];
            foreach($item->popt as $opt) {
                $itemOpt[$opt->bcatalog->id] = $opt->bcatalogitem->name;
            }
            $result['popt']=$itemOpt;

            // торговые предложения
            $itemScu = [];
            foreach($item->scu as $scu) {
                if($scu->active != 1) continue;
                $scuPrice = $scu->price?$scu->price:$item->price;
                $itemScu[$scu->id] = [
                    'price'=>ceil(($scu->discount > 0)?$scu->discount*$scuPrice:$scuPrice),
                    'articul'=>$scu->articul,
                    'code'=>$scu->code,
                    'descr'=>$scu->descr,
                ];
                foreach($scu->scuitems as $sitm) {
                    $itemScu[$scu->id]['options'][$sitm->catalogitem]=$sitm->value;
                }
                foreach($scu->images as $img) {
                    $itemScu[$scu->id]['images'][]=[
                        'file'=>$img->file,
                        'thumb'=>Yii::app()->easyImage->thumbSrcOf('.'.$img->file, array('resize' => array('width' => 94, 'height' => 94)))
                        ];
                }

            }
            $result['scu']=$itemScu;
        }
        return $result;
    }

    public function beforeSave(){
        if($this->isNewRecord)
            $this->creation_time=date('Y-m-d H:i:s');
        return parent::beforeSave();
    }

    public function afterSave() {
        $client = Yii::app()->elastica->getClient();
        $elasticaIndex = $client->getIndex('profi_shop');
        $elasticaType = $elasticaIndex->getType('items');

        if($this->active == 1) {
            $result = [];
            $result['name'] = $this->name;
            $result['code'] = $this->code;
            $result['category_data'] = [
                'code'=>$this->bcategory->code,
                'imagetype'=>$this->bcategory->imagetype,
            ];

            $search_field = [
                $this->name,
                $this->bcategory->name,
            ];

            $scuPrice = 0;
            $discount = 1;
            $cnt = 0;

            if($this->category == 1) {
                foreach($this->icomps as $icomp){
                    if($icomp->active == 0 ) continue;
                    $cnt += 1;
                    $comp = $icomp->bcomplectation;
                    if(intval($scuPrice)==0 && intval($comp->price) > 0) {
                        $scuPrice = $comp->price;
                        if($icomp->discount > 0)
                            $discount = $icomp->discount;
                    }
                }
                $result['price'] = $scuPrice * $discount;
                $result['file'] = $this->images[0]->file;
            } else {
                $result['file'] = $this->images[0]->file? : $this->scu[0]->images[0]->file;

                foreach($this->scu as $scu){
                    if($scu->active == 0) continue;
                    $cnt += 1;
                    if(intval($scuPrice)==0 && intval($scu->price) > 0) {
                        $scuPrice = $scu->price;
                        if($scu->discount > 0)
                            $discount = $scu->discount;
                    }

                    foreach ($scu->scuitems as $si) {
                        if($si->bcatalog->search == 1 && !in_array($si->bcatalogitem->name, $search_field))
                            $search_field[] = $si->bcatalogitem->name;
                    }
                }

                $scuPrice = $scuPrice > 0 ? $scuPrice: $this->price;
                $result['price'] = ceil($discount*$scuPrice);

                if(count($this->scu)==0 || $cnt == 0) {
                    try { $elasticaType->deleteById($this->id); } catch (Exception $e) { Yii::log($e->getMessage(), CLogger::LEVEL_ERROR); }
                    $elasticaType->getIndex()->refresh();
                    return true;
                }

                foreach($this->popt as $si) {
                    if($si->bcatalog->search == 1 && !in_array($si->bcatalogitem->name, $search_field))
                        $search_field[] = $si->bcatalogitem->name;
                }
            }

            $result['search_field'] = implode(' ', $search_field);

            if($this->bcategory->imagetype)
                $result['file'] = Yii::app()->easyImage->thumbSrcOf('.'.$result['file'], array('resize' => array('width' => 214, 'height' => 160)));
            else
                $result['file'] = Yii::app()->easyImage->thumbSrcOf('.'.$result['file'], array('resize' => array('width' => 160, 'height' => 214)));

            $tweetDocument = new \Elastica\Document($this->id, $result);
            $elasticaType->addDocument($tweetDocument);
        } else {
            try { $elasticaType->deleteById($this->id); } catch (Exception $e) { Yii::log($e->getMessage(), CLogger::LEVEL_ERROR); }
        }
        $elasticaType->getIndex()->refresh();
    }
}
