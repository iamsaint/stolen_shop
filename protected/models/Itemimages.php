<?php
/**
 * This is the model class for table "itemimages".
 *
 * The followings are the available columns in table 'itemimages':
 * @property integer $item
 * @property integer $image
 *
 * The followings are the available model relations:
 * @property Items $item0
 * @property Images $image0
 */
class Itemimages extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'itemimages';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('item, image', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('item, image', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'bitem' => array(self::BELONGS_TO, 'Items', 'item'),
            'bimage' => array(self::BELONGS_TO, 'Images', 'image'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'item' => 'Item',
			'image' => 'Image',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('item',$this->item);
		$criteria->compare('image',$this->image);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Itemimages the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public static function addNewLinks($itemId, $files) {
        $imageIds = Images::uploadImages($files);
        foreach($imageIds as $imageId) {
            $className = __CLASS__;
            $class = new $className();
            $model = new $class;
            $model->item = $itemId;
            $model->image = $imageId;
            $model->save();
        }
    }

    public static function removeLinks($itemId, $array) {
        if(is_array($array)) {
            $className = __CLASS__;
            $class = new $className();
            $class::model()->deleteAllByAttributes(['item'=>$itemId, 'image'=>$array]);
        }
    }
}
