<?php

/**
 * This is the model class for table "icomplectations".
 *
 * The followings are the available columns in table 'icomplectations':
 * @property integer $id
 * @property integer $item
 * @property integer $complectation
 *
 * The followings are the available model relations:
 * @property Items $item0
 * @property Complectations $complectation0
 * @property Orderitems[] $orderitems
 */
class Icomplectations extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'icomplectations';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('item, complectation', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, item, complectation', 'safe', 'on'=>'search'),
			array('articul, code, discount', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'bitem' => array(self::BELONGS_TO, 'Items', 'item'),
			'bcomplectation' => array(self::BELONGS_TO, 'Complectations', 'complectation'),
			'orderitems' => array(self::HAS_MANY, 'Orderitems', 'icomplectation'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'item' => 'Item',
			'complectation' => 'Complectation',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('item',$this->item);
		$criteria->compare('complectation',$this->complectation);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Icomplectations the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
