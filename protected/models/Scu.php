<?php

/**
 * This is the model class for table "scu".
 *
 * The followings are the available columns in table 'scu':
 * @property integer $id
 * @property integer $itemid
 * @property integer $active
 * @property string $price
 * @property string $discount
 * @property string $articul
 * @property string $code
 *
 * The followings are the available model relations:
 * @property Items $item
 * @property Scuitems[] $scuitems
 */
class Scu extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'scu';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('itemid, active', 'numerical', 'integerOnly'=>true),
			array('price, discount', 'length', 'max'=>10),
			array('articul, code', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('descr', 'safe'),
			array('id, itemid, active, price, discount, articul, code', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'item' => array(self::BELONGS_TO, 'Items', 'itemid'),
			'scuitems' => array(self::HAS_MANY, 'Scuitems', 'scu'),
			'images' => array(self::MANY_MANY, 'Images', 'scuimages(scu, image)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'itemid' => 'Itemid',
			'active' => 'Active',
			'price' => 'Price',
			'discount' => 'Discount',
			'articul' => 'Articul',
			'code' => 'Code',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('itemid',$this->itemid);
		$criteria->compare('active',$this->active);
		$criteria->compare('price',$this->price,true);
		$criteria->compare('discount',$this->discount,true);
		$criteria->compare('articul',$this->articul,true);
		$criteria->compare('code',$this->code,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Scu the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
