<?php

class Categoryitems extends CModel
{
    public $id;
    public $category;
    public $name;
    public $code;
    public $price;
    public $manufacturer;
    public $material;
    public $size;
    public $file;

    public static function findAll($conditions='', $params = []) {
        return self::getQuery()->where($conditions, $params)->queryAll();
    }

    private static function getQuery(){

        return Yii::app()->db
            ->cache(60)
            ->createCommand()
            ->select(
                'items.id,'.
                'items.category,'.
                'items.name,'.
                'items.code,'.
                'min(case when icomplectations.discount > 0 and icomplectations.discount <1 then icomplectations.discount*complectations.price else complectations.price end) as price,'.
                'complectations.manufacturer,'.
                'complectations.material,'.
                'complectations.size,'.
                'images.file'
            )
            ->from('items')
            ->join('icomplectations', 'icomplectations.item = items.id')
            ->join('complectations', 'icomplectations.complectation = complectations.id')
            ->join('itemimages', 'itemimages.item = items.id')
            ->join('images', 'itemimages.image=images.id')
            ->group('items.id');
    }

    public function attributeNames(){

    }
}
