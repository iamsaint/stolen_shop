<?php

class m140812_081023_create_table_collections extends CDbMigration
{
	public function up()
	{
        $this->createTable('collections', [
            'id'=>'pk',
            'name'=>'string'
        ]);
	}

	public function down()
	{
		$this->dropTable('collections');
	}
}