<?php

class m140812_081631_complectations_add_column_collection extends CDbMigration
{
	public function up()
	{
        $this->addColumn('complectations', 'collection', 'int(11) AFTER size');
	}

	public function down()
	{
		echo "m140812_081631_complectations_add_column_collection does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}