<?php

trait Translit {
    public function encodestring($st)
    {
        $translit = array(
            'а' => 'a',   'б' => 'b',   'в' => 'v',
            'г' => 'g',   'д' => 'd',   'е' => 'e',
            'ё' => 'yo',   'ж' => 'zh',  'з' => 'z',
            'и' => 'i',   'й' => 'j',   'к' => 'k',
            'л' => 'l',   'м' => 'm',   'н' => 'n',
            'о' => 'o',   'п' => 'p',   'р' => 'r',
            'с' => 's',   'т' => 't',   'у' => 'u',
            'ф' => 'f',   'х' => 'x',   'ц' => 'c',
            'ч' => 'ch',  'ш' => 'sh',  'щ' => 'shh',
            'ь' => '_',  'ы' => 'y',   'ъ' => '_',
            'э' => 'e',   'ю' => 'yu',  'я' => 'ya',
            'А' => 'A',   'Б' => 'B',   'В' => 'V',
            'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
            'Ё' => 'YO',   'Ж' => 'Zh',  'З' => 'Z',
            'И' => 'I',   'Й' => 'J',   'К' => 'K',
            'Л' => 'L',   'М' => 'M',   'Н' => 'N',
            'О' => 'O',   'П' => 'P',   'Р' => 'R',
            'С' => 'S',   'Т' => 'T',   'У' => 'U',
            'Ф' => 'F',   'Х' => 'X',   'Ц' => 'C',
            'Ч' => 'CH',  'Ш' => 'SH',  'Щ' => 'SHH',
            'Ь' => '_',  'Ы' => '_',   'Ъ' => '_',
            'Э' => 'E',   'Ю' => 'YU',  'Я' => 'YA',
            ' '=>'_'
        );

        $str = strtr($st, $translit);
        return preg_replace ("/[^a-zA-Z0-9\_]/","",$str);
    }

    public function getUniqCode($name, $i = '') {
        $className = __CLASS__;
        $class = new $className();
        $code = $this->encodestring($name.$i);
        $count = $class::model()->count('code=:code', [':code' => $code]);

        if($count == 0 || ($count==1 && $this->code==$code))
            return $code;
        else return $this->getUniqCode($name, $i= $i+1);
    }
}