<?php

class Controller extends CController {

    protected $request = null;
    public $title;

    public function init() {
        parent::init();
        $cookie = Yii::app()->request->cookies['basket_id']->value;
        if($cookie === null)
            Yii::app()->request->cookies['basket_id'] = new CHttpCookie('basket_id', uniqid());
    }

    public $layout='//layouts/main';

    public $menu=array();

    public $jsParams=array();

    public $breadcrumbs=array();

    public function __construct ($id, $module = null) {
        parent::__construct($id, $module);
        if (is_null($this->title))
            $this->title = Yii::app()->name;
        $this->request = Yii::app()->getRequest();
    }

    public function getNgPost() {
        return json_decode(file_get_contents("php://input"), true);
    }

    protected function renderJSON($data)
    {
        header('Content-type: application/json');
        echo CJSON::encode($data);

        Yii::app()->end();
    }
}