<?php

trait MyActiveRecord  {
    public static function selectList($conf=[], $addDefault = false, $defauktVal = 'Все') {
        if(is_array($conf)) {
            $criteria = new CDbCriteria;
            $criteria->together = true;
            foreach($conf as $k=>$v){
                $key = preg_replace('~[^0-9A-Za-z]+~','_',$k);
                $criteria->addCondition($k.'=:'.$key);
                $criteria->params[':'.$key] = $v;
            }
        } else $criteria = $conf;
        if(isset(self::model()->mmcategory))
            $data = self::model()->with('mmcategory')->findAll($criteria);
        else
            $data = self::model()->findAll($criteria);

        $result = [];
        if($addDefault)
            $result=[''=>$defauktVal];
        foreach($data as $d) {
            $result[$d->id] = $d->name;
        }
        return $result;
    }

    public function getCatCon() {
        if(isset($this->mmcategory)) {
            $result = [];
            foreach($this->mmcategory as $cat) {
                $result[$cat->id] = true;
            }
            return $result;
        } else return [];
    }

    public function getCategoryLinkClassName() {
        if(!isset($this->mmcategory)) return false;
        if(preg_match('|([a-zA-Z\_0-9]*)\(([a-zA-Z\_0-9]*)\,([a-zA-Z\_0-9]*)\)|U', str_replace(' ', '', $this->relations()['mmcategory'][2]), $matches)) {
            return [
               'class'=> ucfirst(strtolower($matches[1])),
               'to'=> $matches[2],
               'from'=> $matches[3],
            ];
        } else return false;
    }
}